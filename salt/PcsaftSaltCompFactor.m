function Z_S = PcsaftSaltCompFactor(P, T,issalt)        

% Compute the compressibility factor for the salt component
%
% SYNOPSIS:
%   function Z_S = PcsaftSaltCompFactor(P, T,issalt)
%
% DESCRIPTION:
%
% PARAMETERS:
%   P      - fluid pressure
%   T      - fluid temperature
%   issalt - salty cells
%
% RETURNS:
%   Z_S - compressibility factor for the salt (Nacl) component
%
% EXAMPLE:
%
% SEE ALSO:
% PcsaftSaltFugacity

    if isempty(issalt)
        Z_S = P./T./(2.16/58.44*1e6)./8.3144598;
    else                
        Z_S = P(issalt)./T(issalt)./(2.16/58.44*1e6)./8.3144598;
    end
end