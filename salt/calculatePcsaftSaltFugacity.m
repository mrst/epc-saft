function f_S = calculatePcsaftSaltFugacity(model,P,comp,T)  

%
%
% SYNOPSIS:
%    function f_S = calculatePcsaftSaltFugacity(model,P,comp,T)
%
% DESCRIPTION:
%
% PARAMETERS:
%   model - pcsaft  model for the salt
%   P     -  pressure
%   comp  - fluid composition
%   T     - temperature
%
% RETURNS:
%   f_s - salt info:  (1) solid and liquid fugacites (2) fugacity
%   coefficient and compressibility factor of salt in water 
%   (3) salt existence
%     
    
    % pcsaft model      
    pcsaftmodel = model.CompositionalMixture; 
    % state 
    state.components = comp; 
    state.pressure   = P; 
    state.T = T; 
    state.reducedDensity = nan(numel(P),1);             
    % FluidType 
    state.FluidType = 'Liquid';             
    pcstate = model.densitymodel.getProp(state, 'initializePcsaftState'); 
    f_S     = pcsaftmodel.getProp(pcstate, 'PcsaftSaltFugacity');  
    
end