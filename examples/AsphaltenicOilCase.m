%% Example demonstrating  ePC-SAFT EoS for modeling asphaltene precipitation
% This example sets up and runs phase equilibria with ePC-SAFT EoS for
% modeling asphaltenic crude oil.
%
% For details on the fluid and the corresponding theromdynamics
% assumptions; see
%   "{Saeed Parvin et al. Continuum scale modelling of salt precipitation
%   in the context of CO2 storage in saline aquifers with MRST
%   compositional."  International Journal of Greenhouse Gas Control 99
%   (2020)}"
% See also
%   "{Gonzales Mohebinia, D.L. Gonzalez, F.M. Vargas, G.J. Hirasaki, W.G.
%   Chapman, Modeling study of CO2-induced asphaltene precipitation, Energy
%   Fuels 22 (2008)}"
%
% We consider the same thermodynamic assumptions as used in these two
% references:
% 
% * Asphaltene precipitation is a thermodynamically reversible process.
% * Van der Waals dispersive forces and the molecular size can sufficiently
%   to describe the molecular interactions in crude oil, so the
%   association term is  not considered.
% * Three phases are modeled:
%      -- (V)  - the vapor
%      -- (L)  - hydrocarbon-rich phase
%      -- (HL) - pseudo-liquid asphaltene-rich phase.

mrstModule add ad-core epc-saft compositional

%% Set an asphaltenic fluid with the following composition
% We have two different examples. Setting casename to 'OilC' gives a fluid
% with the components
%   [N2; CO2; C1; Light-gas; Saturates; Aro+Resin; Asphaltene]
% whereas 'OilC_v2' gives a system with components:
%   [H2S; N2 ; CO2; C1; Heavy-gas; Saturates; Aro+Resin; Asphaltene]
pcsaftmodel = PCSaftModel('casename', 'OilC');

%% Set the temperature & pressure range
pMin = 5.5*mega*Pascal;    % Minimum pressure
pMax = 55*mega*Pascal;     % Maximum pressure
TMin = 330;      % Minimum temperature
TMax = 530;      % Maximum temperature
n    = 50;

% Get pressure/temperature grid
[p,T] = ndgrid(linspace(pMin, pMax, n), linspace(TMin, TMax, n));
state = struct('T', T(:), 'pressure', p(:));

% Initiate composition from fluid
[~, Properties] = AsphaltenicCrudeOil();
comp = Properties(:, 2)';
state.components = repmat(comp, n^2, 1);

%% Get the model
eosmodel = PCSaftAsphEosModel(pcsaftmodel);
eosmodel.verbose = false;

%% Show state functions from the implementation
% For a complete overview of the ePC-SAFT package, we plot the dependency
% graph showing all functions and their connections to compute pressure
figure;
plotStateFunctionGroupings(eosmodel.densitymodel.pcsaftmodel.PCSaftPropertyFunctions, 'Stop', 'Pressure');
title('The ePC-SAFT package');

% To show how the ePC-saft package delivers the fugacities for the phase
% equilibrium calculations, we plot the dependency graph  showing
% connections between state functions of the PcSaftModel
groups =eosmodel.densitymodel.pcsaftmodel.getStateFunctionGroupings();
figure;
plotStateFunctionGroupings(groups{1},  'label', 'label');
title('All dependency to evaluate fugacity for liquid or gaseous phases as wll as solid fugacity');

% Plot all dependencies for the Associative Helmoltz Energy
figure;
plotStateFunctionGroupings(eosmodel.densitymodel.pcsaftmodel.PCSaftPropertyFunctions, 'Stop', 'AssociativeHelmoltzEnergy')
title('All dependencies required to evaluate the Associative Helmoltz Energy');
% One can plot other energies (Dissipative, Hard-chain,....) by changing
% the input
pause(5);

%% Perform a standalone flash
state = genericStandaloneFlash(state, eosmodel);

%% Diagram of Asphaletene precipitation envelope
% The diagram and the results are in good  agreement with those presented
% in Massoudi et al. (2020), Mohebbinia et al. (2017),  and Gonzalez et al.
% (2008).
flag = (state.s(:,1) > 0) + (state.s(:,2) > 0) + (state.s(:,3) > 0);
contourf(T, convertTo(p, mega*Pascal), reshape(flag, [], n),n);
fig = gcf;
annotatePhaseDiagram(fig);
title('Diagram of Asphaltene preci. envelope','fontsize',14);
xlabel('Temperature (K)','fontsize',14);
ylabel('Pressure (MPa)','fontsize',14);

%%  Plot precipated Asphaltene
index = strcmp(eosmodel.densitymodel.pcsaftmodel.names, 'Asphaltene');
figure;
contourf(T, convertTo(p, mega*Pascal), reshape(state.X_HL(:,index), [], n),n);
fig = gcf;
annotatePhaseDiagram(fig);
title('Solid Asphaltene','fontsize',14);
xlabel('Temperature (K)','fontsize',14);
ylabel('Pressure (MPa)','fontsize',14);

%%  Plot CO2 in Liquid phase
index = strcmp(eosmodel.densitymodel.pcsaftmodel.names, 'CO2');
figure;
contourf(T, convertTo(p, mega*Pascal), reshape(state.X_L(:,index), [], n),10);
title('CO2 in Liquid phase','fontsize',14);
xlabel('Temperature (K)','fontsize',14);
ylabel('Pressure (MPa)','fontsize',14);


%% Plot reduced density
phases = eosmodel.eosVLLmodel.getPhaseNames;
WhichPhase = 'L';
index = strcmp(phases, 'L');
figure;
contourf(T, convertTo(p, mega*Pascal), reshape(state.reducedDensity(:,index), [], n),10);
title(strcat('Reduced Density'," ", '(',WhichPhase,')'),'fontsize',14);
xlabel('Temperature (K)','fontsize',14);
ylabel('Pressure (MPa)','fontsize',14);


%% Plot compressibility factor
figure;
WhichPhase = 'L';
index = find(strcmp(phases, 'L'));
contourf(T, convertTo(p, mega*Pascal), reshape(state.Z(:,index), [], n),10);
title(strcat('Compressibility Factor'," ", '(',WhichPhase,')'),'fontsize',14);
xlabel('Temperature (K)','fontsize',14);
ylabel('Pressure (MPa)','fontsize',14);

%% Plot cumulated flash iterations
figure;
[CS, H] = contourf(T, convertTo(p, mega*Pascal), ...
                   reshape(state.eos.itCount, [], n), n, 'ShowText', 'off');
clabel(CS,H);
H.LevelList = round(H.LevelList);
fig = gcf;
annotatePhaseDiagram(fig);
title('Cumulated Flash Iterations','fontsize',14);
xlabel('Temperature (K)','fontsize',14);
ylabel('Pressure (MPa)','fontsize',14);

%%  Plot cumulated density solves
figure;
[CS, H] = contourf(T, convertTo(p, mega*Pascal), reshape(sum(state.eos.itDens,2), [], n),n,'ShowText','off');
clabel(CS,H,'FontSize',8,'Color','red');
H.LevelList = round(H.LevelList);
fig = gcf;
annotatePhaseDiagram(fig);
title('Cumulated Density Iterations','fontsize',14);
xlabel('Temperature (K)','fontsize',14);
ylabel('Pressure (MPa)','fontsize',14);

%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
