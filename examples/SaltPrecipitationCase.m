%% Example demonstrating phase ePC-SAFT for modeling Salt precipitation
% This example sets up and runs phase equilibria with ePC-SAFT EoS for
% modeling Salt precipitation. 
%
% For details on the fluid and the corresponding theromdynamics
% assumptions; see
% "{ Saeed Parvin et al. Continuum scale modelling of salt precipitation in the
%  context of CO2 storage in saline aquifers with MRST compositional."  International Journal of Greenhouse
%  Gas Control 99 (2020)}"
% We use epc-saft EoS for  the H20-CO2-NaCl system  from the above reference, where the 
% following thermodynamic assumptions are made:
% - The association term can capture the polarity contribution of the molecules.
% - The ePC-SAFT considers the hydrogen bonding through cross association between unlike sites (i.e., O–H).
% - The model for water molecules is based on the two-site single segment model proposed by Cameretti et al. (2005), 
%   in which two associating sites of type (H) and type (O) represent the proton-donor sites and electronlone pairs, respectively.
% - Two sites of the same type(i.e., O–O or H–H) do not associate.
% - CO2 is not considered as an associative molecule.
% - Dispersion interaction is not considered between ions (Cameretti et al.,2005)
% - The Liquid phase contains H2O, CO2, Na+, and Cl-
% - The Vapor phase contains H2O and CO2; the salt only dissolves in the Liquid phase
% - The Solid phase only contains NaCl.
%%  The equilibrium system of this case: 
% (1) f^{Liquid}_{i}= f^{Gas}_{i}, i = H2O, CO2
% (2) f^{Liquid}_{salt}= f^{Solid}_{salt}.
% Note: the fugacity of the pure Liquid and Vapor components are computed with ePC-SAFT EoS. 
% However, the fugacity for the  pure solid Salt component are obtained explicitely with a (P,T) 
% equation (see PCSaftSaltEosModel fo details).

mrstModule add ad-core epcsaft compositional

%% Get the pcsaft model for the H20-CO2-NaCl system 
pcsaftmodel = PCSaftModel('casename', 'Co2H2oNaCl');

% Get pressure/temperature grid
pMin = 5.5*mega*Pascal;    % Minimum pressure
pMax = 30*mega*Pascal;     % Maximum pressure
TMin = 310;      % Minimum temperature 
TMax = 380;      % Maximum temperature 
n    = 20;      % Grid size

[p,T] = ndgrid(linspace(pMin, pMax, n), linspace(TMin, TMax, n));
state = struct('T', T(:), 'pressure', p(:));

% Set the initial composition
[~,Properties] = NaClH20Co2Fluid();
comp = Properties(:, 2)';

ppm = 4.5*58.44e3; % mg salt / kg water 4.5 Molal
Mwsalt = 58.44; 
molsalt_molwater = ppm/1e6/Mwsalt*18.01; 
compion = molsalt_molwater/(2*molsalt_molwater + 1); 
compwater = 1/(2*molsalt_molwater + 1); 

H2O = find(strcmp(pcsaftmodel.names, 'H2O'));
CO2 = find(strcmp(pcsaftmodel.names, 'CO2'));
Na  = find(strcmp(pcsaftmodel.names, 'Na'));
Cl  = find(strcmp(pcsaftmodel.names, 'Cl'));

comp(H2O) = compwater; 
comp(CO2) = 0; 
comp(Na)  = compion; 
comp(Cl)  = compion; 

%% We initiate the system by adding 10 persent of CO2 
comp(CO2) = 0.1; 
comp = comp./sum(comp); 
state.components = repmat(comp, n^2, 1);

%% Simulation
eosmodel = PCSaftSaltEosModel(pcsaftmodel);
eosmodel.verbose = false;
state = genericStandaloneFlash(state, eosmodel);

%% Results
% The results shows the capability of ePC-saft to phase equilibriun of CO2-Brine  system in a wide range of pressure 
% and temperature relevant to the CO2 storage in saline aquifers. (A) The first figure shows the solubility of CO2 
% in brine: it  increases with pressure and decreases with temperature. (B) In the second figue, we plot the H2O 
% solubility in CO2. We can see a sharp discontinuity below the critical temperature. That's the point where the phase change from a gaseous 
% to a liquid CO2-rich phase. When increasing the pressure, the water evaporation into the gaseous CO2-rich 
% phase decreases, however the opposite  is happening for the water solubility in liquid CO2. 

% We plot the amount of CO2 in Water
 index = find(strcmp(eosmodel.densitymodel.pcsaftmodel.names, 'CO2'));
 figure;
 contourf(T, convertTo(p, mega*Pascal), reshape(state.X_L(:,2), [], n),n);
 title('CO2 in H2O','fontsize',14);
 xlabel('Temperature (K)','fontsize',14);
 ylabel('Pressure (MPa)','fontsize',14);
% We plot the amount of H2O in CO2
 figure;
 contourf(T, convertTo(p, mega*Pascal), reshape(state.X_V(:,1), [], n),n);
 title('H2O in CO2','fontsize',14);
 xlabel('Temperature (K)','fontsize',14);
 ylabel('Pressure (MPa)','fontsize',14); 

% We plot for a fixed temperature the amount of CO2 in brine as a function of the pressure 
 figure;
 hold on;
 kk =  1:int16((n-1)/4):n;
 T1 = [num2str(int16(state.T(1+kk(1)*n))) ' K'];
 plot(convertTo(state.pressure(1:n), mega*Pascal),state.X_L(1+kk(1)*n:(kk(1)+1)*n,2));
 T2 = [num2str(int16(state.T(1+kk(1)*n))) ' K'];
 plot(convertTo(state.pressure(1:n), mega*Pascal),state.X_L(1+kk(2)*n:(kk(2)+1)*n,2),'Color', 'red');
 T3 = [num2str(int16(state.T(1+kk(2)*n))) ' K'];
 plot(convertTo(state.pressure(1:n), mega*Pascal),state.X_L(1+kk(3)*n:(kk(3)+1)*n,2), 'Color', 'green');
 T4 = [num2str(int16(state.T(1+kk(3)*n))) ' K'];
 plot(convertTo(state.pressure(1:n), mega*Pascal),state.X_L(1+kk(4)*n:(kk(4)+1)*n,2), 'Color', 'magenta');
 xlabel('Pressure (MPa)','fontsize',14);
 ylabel('Liquid CO2','fontsize',14);
 legend(T1,T2,T3,T4,'FontSize',14);

% We plot for a fixed temperature the amount of H2O in CO2 as a function of the pressure
 figure;
 hold on;
 T1 = [num2str(int16(state.T(1+kk(1)*n))) ' K'];
 plot(convertTo(state.pressure(1:n), mega*Pascal),state.X_V(1+kk(1)*n:(kk(1)+1)*n,1));
 T2 = [num2str(int16(state.T(1+kk(2)*n))) ' K'];
 plot(convertTo(state.pressure(1:n), mega*Pascal),state.X_V(1+kk(2)*n:(kk(2)+1)*n,1),'Color', 'red');
 T3 = [num2str(int16(state.T(1+kk(3)*n))) ' K'];
 plot(convertTo(state.pressure(1:n), mega*Pascal),state.X_V(1+kk(3)*n:(kk(3)+1)*n,1), 'Color', 'green');
 T4 = [num2str(int16(state.T(1+kk(4)*n))) ' K'];
 plot(convertTo(state.pressure(1:n), mega*Pascal),state.X_V(1+kk(4)*n:(kk(4)+1)*n,1), 'Color', 'magenta');
 xlabel('Pressure (MPa)','fontsize',14);
 ylabel('Vaporized H2O','fontsize',14);
 legend(T1,T2,T3,T4,'FontSize',14); 
% We plot for fixed temperature the amount of CO2 in brine as a function of the pressure
 figure;
 hold on;
 P1 = [num2str(int16(convertTo(state.pressure(kk(1)), mega*Pascal))) ' Mpa'];
 plot(state.T(1+kk(2):n:n*n),state.X_L(1+kk(1):n:n*n,2));
 P2 = [num2str(int16(convertTo(state.pressure(kk(2)), mega*Pascal))) ' Mpa'];
 plot(state.T(1+kk(2):n:n*n),state.X_L(1+kk(2):n:n*n,2),'Color', 'red');
 P3 = [num2str(int16(convertTo(state.pressure(kk(3)), mega*Pascal))) ' Mpa'];
 plot(state.T(1+kk(3):n:n*n),state.X_L(1+kk(3):n:n*n,2), 'Color', 'green');
 P4 = [num2str(int16(convertTo(state.pressure(kk(4)), mega*Pascal))) ' Mpa'];
 plot(state.T(1+kk(4):n:n*n),state.X_L(1+kk(4):n:n*n,2), 'Color', 'magenta');
 xlabel('Temperature (K)','fontsize',14);
 ylabel('Liquid CO2','fontsize',14);
 legend(P1,P2,P3,P4,'FontSize',14);
 pause(5);
% We plot for fixed pressure the amount of H2O in CO2  as a function of the temperature
 figure;
 hold on;
 P1 = [num2str(int16(convertTo(state.pressure(kk(1)), mega*Pascal))) ' Mpa'];
 plot(state.T(1+kk(1):n:n*n),state.X_L(1+kk(1):n:n*n,2));
 P2 = [num2str(int16(convertTo(state.pressure(kk(2)), mega*Pascal))) ' Mpa'];
 plot(state.T(1+kk(2):n:n*n),state.X_L(1+kk(2):n:n*n,2),'Color', 'red');
 P3 = [num2str(int16(convertTo(state.pressure(kk(3)), mega*Pascal))) ' Mpa'];
 plot(state.T(1+kk(3):n:n*n),state.X_L(1+kk(3):n:n*n,2), 'Color', 'green');
 P4 = [num2str(int16(convertTo(state.pressure(kk(4)), mega*Pascal))) ' Mpa'];
 plot(state.T(1+kk(4):n:n*n),state.X_L(1+kk(4):n:n*n,2), 'Color', 'magenta');
 xlabel('Temperature (K)','fontsize',14);
 ylabel('Vaporized H2O','fontsize',14);
 legend(P1,P2,P3,P4,'FontSize',14);
% Some extra plots to show flow properties computed with the ePC-SAFT package (1) An extra plot showing the computed compressibility factors 
% for the three phases (V), (L), (HL) at fixed temperature T
 figure;
 hold on;
 T1 = [num2str(int16(state.T(1+kk(3)*n))) ' K'];
 plot(convertTo(state.pressure(1:n), mega*Pascal),state.Z(1+kk(3)*n:(kk(3)+1)*n,1));
 plot(convertTo(state.pressure(1:n), mega*Pascal),state.Z(1+kk(3)*n:(kk(3)+1)*n,2),'Color', 'red');
 plot(convertTo(state.pressure(1:n), mega*Pascal),state.Z(1+kk(3)*n:(kk(3)+1)*n,3), 'Color', 'green');
 xlabel('Pressure (MPa)','fontsize',14);
 ylabel('Compressibility factor','fontsize',14);
 legend('Liquid','Vapor','Heavy-Liquid','FontSize',14);
 title(strcat('Compressibility factors at T=', T1),'fontsize',14);
 
% An extra plot showing the computed reduced density 
% for the three phases (V), (L), (HL) at fixed temperature T
 figure;
 hold on;
 T1 = [num2str(int16(state.T(1+kk(3)*n))) ' K'];
 plot(convertTo(state.pressure(1:n), mega*Pascal),state.reducedDensity(1+kk(3)*n:(kk(3)+1)*n,1));
 plot(convertTo(state.pressure(1:n), mega*Pascal),state.reducedDensity(1+kk(3)*n:(kk(3)+1)*n,2),'Color', 'red');
 plot(convertTo(state.pressure(1:n), mega*Pascal),state.reducedDensity(1+kk(3)*n:(kk(3)+1)*n,3), 'Color', 'green');
 xlabel('Pressure (MPa)','fontsize',14);
 ylabel('Reduced density','fontsize',14);
 legend('Liquid','Vapor','Heavy-Liquid','FontSize',14);
 title(strcat('Compressibility factors at T=', T1),'fontsize',14);
 
 %{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
