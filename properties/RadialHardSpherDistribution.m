classdef RadialHardSpherDistribution < StateFunction
%% "{ Joachim Gross and Gabriele Sadowski. "Perturbed-Chain SAFT: An
%% Equation of State Based on a Perturbation Theory for Chain Molecules".
%% In: Industrial & Engineering Chemistry Research 40.4 (Feb. 2001), pages 1244–
%% 1260. url: https://doi.org/10.1021/ie0003887}"
%
% SYNOPSIS:
%   RadialHardSpherDistribution(model, varargin)
%
% DESCRIPTION: 
%    Computes the radial distribution function $g_{ij}^{hs}$, see A.7 of Ref, and its
%    derivative with respect to density, $\frac{\partial
%     g_{ij}^{hs}}{\partial\rho}$. The derivative is obtained by first
%     computing the derivative multiplied by the density, see (A.27) in
%     Ref. and we also return this value
%
% PARAMETERS:
%   model    - a density model built with epc-saft EoS
%   varargin - state with a given pressure, temperature and composition
%
% RETURNS:
%   class instance
%
% EXAMPLE:
%
% SEE ALSO:
%   HardChainHelmoltzEnergy
%


    properties
    end
    
    methods
        function gp = RadialHardSpherDistribution(model, varargin)
            gp@StateFunction(model, varargin{:});
            gp = gp.dependsOn({'AuxillaryVariables', 'Density', 'Diameters'});
            
            gp.label = 'Radial Hard Sphere Distribution';
        end

        function ghs_all = evaluateOnDomain(prop, model, state)            
            rho     = model.getProps(state, 'Density');
            AuxVars = model.getProps(state, 'AuxillaryVariables');
            diams   = model.getProps(state, 'Diameters');
            
            AuxVars = AuxVars.values;
            
            numC    = model.numC;
            segNum  = model.segNum;

            % Start computation
            ghs         = cell(numC, numC);
            dghs_rho    = cell(numC, numC);
            dghs_xi     = cell(numC, numC, numC);
            
            % shortcut
            d     = diams.d; 
            dij   = diams.dij; % di.*dj / (di + dj)

            avs = AuxVars;
            m   = segNum;

            term11 = 1./(1 - avs{4});
            coefterm12 = 3.*avs{3}./((1 - avs{4}).^2);
            coefterm13 = 2.*(avs{3}.^2)./((1 - avs{4}).^3);
            
            term21 = avs{4}./((1 - avs{4}).^2)./(rho); 
            coefterm22 = (coefterm12 + 6.*avs{3}.*avs{4}./(1 - avs{4}).^3)./(rho);
            coefterm23 = (2.*coefterm13 + 2/3.*avs{4}.*coefterm12.^2)./(rho);
            
            for i = 1 : numC
                for j = 1 : numC

                    term12 =  dij{i,j}.*coefterm12;
                    term13 =  dij{i,j}.^2.*coefterm13;
                    
                    ghs{i,j} = term11 + term12 + term13; 
                    
                    term22 =  dij{i,j}.*coefterm22; 
                    term23 =  dij{i,j}.^2.*coefterm23;                    
                    dghs_rho{i, j} = term21 + term22 + term23;

                end
            end
            
            co_term32 = term11.*coefterm12;
            c = pi./6.*rho;
            for k = 1 :numC
                coe = c.*m(k);
                term31 = coe.*d{k}.^3.*(term11.^2); 
                coefterm32 = coe.*(3.*(d{k}.*term11).^2 + 2.*d{k}.^3.*co_term32);
                coefterm33 = coe.*(4.*d{k}.^2.*avs{3}./(1 - avs{4}).^3 + 6.*avs{3}.^2.*d{k}.^3./(1 - avs{4}).^4);
                for i = 1 : numC
                    for j = 1 : numC
                        term32 =  dij{i,j}.*coefterm32; 
                        term33 =  dij{i,j}.^2.*coefterm33; 
                        dghs_xi{i, j, k} = term31 + term32 + term33;                           
                    end
                end
            end
            
            ghs_all.values           = ghs;
            ghs_all.derivatives_rho  = dghs_rho;
            ghs_all.derivatives_xi   = dghs_xi;
            
        end
    end
end

%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
