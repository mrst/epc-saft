classdef CompressibilityFactor < StateFunction
%% see "{Joachim Gross and Gabriele Sadowski. "Perturbed-Chain SAFT: An
%% Equation of State Based on a Perturbation Theory for Chain Molecules".
%% In: Industrial & Engineering Chemistry Research 40.4 (Feb. 2001), pages 1244–
%% 1260. url: https://doi.org/10.1021/ie0003887}"
%
% SYNOPSIS:
%   CompressibilityFactor(model, varargin)
%
% DESCRIPTION:
%   Compute the compressibility factor Z from the total helmoltz free
%   energy and the density
% PARAMETERS:
%   model - density model built with epc-saft EoS
%   varagin - state with a given pressure, temperature and composition
%
% RETURNS:
%   Z - compressibility factor
%
% EXAMPLE:
%   see also Eq. 21 of the report
% SEE ALSO: 
%   ChemicalPotentials
%



    properties
    end
    
    methods
        function gp = CompressibilityFactor(model, varargin)
            gp@StateFunction(model, varargin{:});
            gp = gp.dependsOn({'TotalHelmoltzEnergy'});
            gp = gp.dependsOn({'DipolHelmoltzEnergy'});
            gp = gp.dependsOn({'Density'});
            
            gp.label = 'Compressibility Factors';
        end

        function Z = evaluateOnDomain(prop, model, state)

            totA = model.getProp(state, 'TotalHelmoltzEnergy');
            dens = model.getProp(state, 'Density');
            
            %Apolar = model.getProp(state, 'DipolHelmoltzEnergy');

            %% safe-guard against nan for degenerate rho

          if isa(totA.derivatives_rho,'ADI')
              totA.derivatives_rho.val(isnan( totA.derivatives_rho.val)) = 0;           
              jac_totA =  totA.derivatives_rho.jac;
              jac_totA{:}.diagonal(isnan(jac_totA{:}.diagonal)) = 1;           
              totA.derivatives_rho.jac = jac_totA;
          else
               totA.derivatives_rho(isnan( totA.derivatives_rho)) = 0;
          end          
          Z = dens.*(totA.derivatives_rho)+1;
        %  Z = Z + Apolar.Z;
                
          % safe-guard for debugging
          if isa(   Z,'ADI') && ~all(isfinite([   Z.val' sum(   Z.jac{:})]))                               
              warning('compressibility factor  must have finite entries.');
          elseif ~isa(   Z,'ADI')&& ~all(isfinite(Z))            
              warning('compressibility factor must have finite entries.');            
          end

        end
    end
end

%{
  Copyright 2009-2021 SINTEF ICT, Applied Mathematics.

  This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

  MRST is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  MRST is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}