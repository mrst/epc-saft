classdef HardChainHelmoltzEnergy < StateFunction
%% "{ Joachim Gross and Gabriele Sadowski. "Perturbed-Chain SAFT: An
%% Equation of State Based on a Perturbation Theory for Chain Molecules".
%% In: Industrial & Engineering Chemistry Research 40.4 (Feb. 2001), pages 1244–
%% 1260. url: https://doi.org/10.1021/ie0003887}"
%
% SYNOPSIS:
%   HardChainHelmoltzEnergy(model, varargin)
%
% DESCRIPTION:
%   Compute the hard-chain Helmoltz free energy
% PARAMETERS:
%   model    - density model built with epc-saft EoS
%   varargin - state with a given pressure, temperature and composition
%
% RETURNS:
%   Ahc - the hc Helmoltz free energy and its derivatives wrt the
%   composition x and density
%
% EXAMPLE:
%
% SEE ALSO:
%     HardSphereHelmoltzEnergy, TotalHelmoltzEnergy


    properties
    end
    
    methods
        function gp = HardChainHelmoltzEnergy(model, varargin)
            gp@StateFunction(model, varargin{:});
            gp = gp.dependsOn({'RadialHardSpherDistribution', ...
                               'HardSphereHelmoltzEnergy', ...
                               'AveragedSegmentNumber'});
            gp = gp.dependsOn({'x'}, 'state');
            gp.label = 'Hard-Chain Helmotz Energy';
            
        end

        function Ahc = evaluateOnDomain(prop, model, state)
        % Compute Ahc for association bindings.  It is obtained from
        % sum_i^numAss X(x(i))
        % $Z = \rho\frac{\partial a^{assoc}}{\partial\rho}$, we use 21 in ref2
            
        % Fetch or compute argument variables
            
            x            = model.getProp(state, 'x');
            ghs_all      = model.getProp(state, 'RadialHardSpherDistribution');
            Ahs_all      = model.getProp(state, 'HardSphereHelmoltzEnergy');
            AvsegNum_all = model.getProp(state, 'AveragedSegmentNumber');
            
            ghs       = ghs_all.values;
            dghs_drho = ghs_all.derivatives_rho;
            dghs_xi   = ghs_all.derivatives_xi;

            AvsegNum   = AvsegNum_all.values;
            daverm_xi  = AvsegNum_all.derivatives_xi;
            Ahs        = Ahs_all.values;
            dAhs_rho   = Ahs_all.derivatives_rho;
            dAhs_xi    = Ahs_all.derivatives_xi;

            numC   = model.numC;
            segNum = model.segNum; 
            
            % Start computation
            
            % First derivative in function of rho 
            
            term1  = 0;
            term2  = 0;
            for i = 1 : numC
                ghs{i,i}(x{i}==0)=1;
                term2  = term2 + x{i}.*(1 - segNum(i)).*log(ghs{i,i}); 
                dghs_drho{i, i}(x{i}==0)=1;
                term1  = term1 + x{i}.*(segNum(i) - 1).*ghs{i,i}.^(-1).*dghs_drho{i, i};
            end
            a_hc     = AvsegNum.*Ahs +term2;
            dAhc_rho =  dAhs_rho.*AvsegNum - term1 ;
            
            % Second derivative in function of rho_i 
            dAhc_xi = cell(1,numC);
            for i = 1 :numC
                sum1 =0;
                for k = 1 :numC
                    ghs{k,k}(x{k}==0)=1;
                    dghs_xi{k,k, i}(x{k}==0) =1;
                    sum1 = sum1 + x{k}.*(segNum(k) - 1).*ghs{k,k}.^(-1).*dghs_xi{k,k, i};
                                    
                end                
                ghs{i,i}(x{i}==0)=1;                                
                dAhc_xi{i} = daverm_xi{i}.*Ahs + AvsegNum.*dAhs_xi{i} - sum1 +(1 - segNum(i)).*log(ghs{i,i});
            end
            
            Ahc.values           = a_hc;
            Ahc.derivatives_rho  = dAhc_rho;
            Ahc.derivatives_xi   = dAhc_xi;



        end
    end
end

%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
