classdef AssociativeHelmoltzEnergy < StateFunction
%
%
% SYNOPSIS:
%   AssociativeHelmoltzEnergy(model, varargin)
%
% DESCRIPTION:
%   Compute the Association Helmoltz free energy
% PARAMETERS:
%   model    - density model built with epc-saft EoS
%   varargin - state with a given pressure, temperature and composition 
%
% RETURNS:
%   Aion - the Association Helmoltz free energy and its derivatives wrt the
%   composition x and density
%
% EXAMPLE:
%
% SEE ALSO:
%     HardSphereHelmoltzEnergy, TotalHelmoltzEnergy


    properties
    end
    
    methods
        function gp = AssociativeHelmoltzEnergy(model, varargin)
            gp@StateFunction(model, varargin{:});
            gp = gp.dependsOn({'FreeSiteMoleFraction'});
            gp = gp.dependsOn({'x'}, 'state');
            
            gp.label = 'Associative Helmoltz Energy';
        end

        function Aassoc = evaluateOnDomain(prop, model, state)
        % Compute Aassoc for association bindings.  It is obtained from
        % sum_i^numAss X(x(i))
        % $Z = \rho\frac{\partial a^{assoc}}{\partial\rho}$, we use 21 in ref2
        % Fetch or compute argument variables
            
            Xa_all = model.getProp(state, 'FreeSiteMoleFraction'); 
            x      = model.getProp(state, 'x');
            
            Xa        = Xa_all.values;
            dXa_rho   = Xa_all.derivatives_rho; 
            dXa_xi    = Xa_all.derivatives_xi; 
            
            numAss    = model.numAss;
            numC      = model.numC;
            sites     = model.sites;
            num_site_i= model.num_site_i;
           
 
            if  any(sum([x{:}]'.*num_site_i,2)~=0)   
            % Start computation
            A_assoc = 0;
            for j = 1 : numAss
                xj = sites.components(j);
                A_assoc  =  A_assoc + x{xj}.*(log(Xa{j}) -0.5.*Xa{j}); %+sum_i M_i (to be verified)
            end
            for i = 1 : numC
                A_assoc = A_assoc + 0.5.*x{i}.*num_site_i(i);
            end
            
            % First derivative in function of rho
            dAassoc_rho = 0;
            for i = 1 : numAss
                xi = sites.components(i);
                dAassoc_rho = dAassoc_rho + x{xi}.*(dXa_rho{i}.*(1./Xa{i} - 0.5));
            end
            
            % Second derivative in function of xi (dAassoc_xi=muAss of ref2)
            dAassoc_xi  = cell(numC, 1);
            for k = 1 : numC
                
                s1 = 0;
                for j = 1 : numAss
                    indlin = model.getInd(j, k);
                    xj = sites.components(j);
                    s1 = s1 + x{xj}.*dXa_xi{indlin}.*(1./Xa{j} - 0.5);
                end
                
                s2 = 0;
                for sd = 1 : num_site_i(k)
                    s2 = s2 + (log(Xa{sd}) -0.5.*Xa{sd});
                end
                
                dAassoc_xi{k} = s1 + s2 +0.5.*num_site_i(k);
            end
            else
                
                A_assoc = 0;
                dAassoc_rho = 0;
                for i = 1 : numC                
                        dAassoc_xi{i} = 0;
                end
            end
            Aassoc.values          = A_assoc;
            Aassoc.derivatives_rho = dAassoc_rho; 
            Aassoc.derivatives_xi  = dAassoc_xi; 
            
        end
    end
end

%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
