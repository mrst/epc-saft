classdef HardSphereHelmoltzEnergy < StateFunction
%
%
% SYNOPSIS:
%   HardSphereHelmoltzEnergy(model, varargin)
%
% DESCRIPTION:
%   Compute the hard-sphere Helmoltz free energy
% PARAMETERS:
%   model    - density model built with epc-saft EoS
%   varargin - state with a given pressure, temperature and composition
%
% RETURNS:
%   Ahs - the hs Helmoltz free energy and its derivatives wrt the
%   composition x and density
%
% EXAMPLE:
%
% SEE ALSO: 
%   HardChainHelmoltzEnergy, TotalHelmoltzEnergy
%


    properties
    end
    
    methods
        function gp = HardSphereHelmoltzEnergy(model, varargin)
            gp@StateFunction(model, varargin{:});
            gp = gp.dependsOn({'AuxillaryVariables'});
            
            gp.label = 'Hard Sphere Helmoltz Energy';
        end

        function Ahs = evaluateOnDomain(prop, model, state)
        % Compute Ahs  and its derivatives from Eq A.6 ref1
            AuxVars   = model.getProp(state, 'AuxillaryVariables');
            aux       = AuxVars.values;
            daux_rho  = AuxVars.derivatives_rho;
            daux_xi   = AuxVars.derivatives_xi;
            
            numC      = model.numC;
            % Start computation
            
            term1 = 3*aux{2}.*aux{3}./(1-aux{4});
            term2 = aux{3}.*aux{3}.*aux{3}./(aux{4}.*(1-aux{4}).^2);
            term3 = (aux{3}.*aux{3}.*aux{3}./aux{4}.^2-aux{1}).*log(1-aux{4});
            a_hs  = (1./aux{1}).*(term1 + term2 + term3); %Eq. A6 of reference
            
            term1 = -daux_rho{1}./aux{1}.*a_hs;
            term2 = 3.*(daux_rho{2}.*aux{3} + aux{2}.*daux_rho{3})./(1-aux{4});
            term3 = 3.*aux{2}.*aux{3}.*daux_rho{4}./(1-aux{4}).^2;
            term4 = 3.*aux{3}.^2.*daux_rho{3}./(aux{4}.*(1-aux{4}).^2);
            term5 = aux{3}.^3.*daux_rho{4}.*(3.*aux{4}-1)./(aux{4}.^2.*(1-aux{4}).^3);
            term6 = ((3.*aux{3}.^2.*daux_rho{3}.*aux{4}-2.*aux{3}.^3.*daux_rho{4})./aux{4}.^3-daux_rho{1}).*log(1-aux{4});
            term7 = (aux{1}-aux{3}.^3./aux{4}.^2).*daux_rho{4}./(1-aux{4});
            
            dAhs_rho = term1 + 1./aux{1}.*(term2 + term3 + term4 + term5 + term6 +term7); %Eq. A36 of reference
            
            dAhs_xi = cell(1,numC);
             
            for i = 1:numC
                term1 = -daux_xi{1,i}./aux{1}.*a_hs;
                term2 = 3.*(daux_xi{2,i}.*aux{3} + aux{2}.*daux_xi{3,i})./(1-aux{4});
                term3 = 3.*aux{2}.*aux{3}.*daux_xi{4,i}./(1-aux{4}).^2;
                term4 = 3.*aux{3}.^2.*daux_xi{3,i}./(aux{4}.*(1-aux{4}).^2);
                term5 = aux{3}.^3.*daux_xi{4,i}.*(3*aux{4}-1)./(aux{4}.^2.*(1-aux{4}).^3);
                term6 = ((3*aux{3}.^2.*daux_xi{3,i}.*aux{4}-2*aux{3}.^3.*daux_xi{4,i})./aux{4}.^3-daux_xi{1,i}).*log(1-aux{4});
                term7 = (aux{1}-aux{3}.^3./aux{4}.^2).*daux_xi{4,i}./(1-aux{4});
                dAhs_xi{i} = term1 + 1./aux{1}.*(term2 + term3 + term4 + term5 + term6 +term7); %Eq. A36 of reference
            end
            
            Ahs.values          = a_hs;
            Ahs.derivatives_rho = dAhs_rho; 
            Ahs.derivatives_xi  = dAhs_xi; %= daass_xi     
            
        end
    end
end

%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
