classdef AdispChainConstants < StateFunction
%
%
% SYNOPSIS:
%   AdispChainConstants(model, varargin)
%
% DESCRIPTION:
%   prepare some functions for the Helmholtz free energy for the
%   hard-sphere contribution
% PARAMETERS:
%   model  - epc-saft model
%
% RETURNS:
%   abcoefs - Hard-chain constants with their derivatives with repect to
%   composition and density
%
% EXAMPLE:
%   see details in the repeort
% SEE ALSO: AdispCompressibilityConstants
%


    properties
    end
    
    methods
        function gp = AdispChainConstants(model, varargin)
            gp@StateFunction(model, varargin{:});
            gp = gp.dependsOn({'AveragedSegmentNumber'});
            
            gp.label = 'Hard-Chain Constants';
            
        end

        function abcoefs = evaluateOnDomain(prop, model, state)
        % Compute coefficients obtained from universal model constants, see
        % eqs 18 and 18 in ref1 of the report
            
        % Fetch or compute argument variables
            AvsegNum_all = model.getProp(state, 'AveragedSegmentNumber');
            AvsegNum     = AvsegNum_all.values;
            dAvsegNum_xi = AvsegNum_all.derivatives_xi;
            
            amodconsts = model.amodconsts;
            bmodconsts = model.bmodconsts;
            numC       = model.numC;
            
            % Start computation
            a = cell(7, 1);
            b = cell(7, 1);
            ac = amodconsts; % shortcut
            bc = bmodconsts; % shortcut
            for j = 1 : 7
                coef1 = (AvsegNum - 1)./AvsegNum;
                coef2 = (AvsegNum - 2)./AvsegNum;                
                
                a{j} = ac(1, j) + coef1.*ac(2, j) + coef1.*coef2.*ac(3, j); 
                b{j} = bc(1, j) + coef1.*bc(2, j) + coef1.*coef2.*bc(3, j);
            end
            
            ab_coefs = cell(2, 1);
            ab_coefs{1} = a;
            ab_coefs{2} = b;
            
            der_a = cell(7,numC);
            der_b = cell(7,numC);
            for i = 1:7
                for j = 1:numC
                    der_a{i,j} = dAvsegNum_xi{j}./AvsegNum.^2.*ac(2, i) + dAvsegNum_xi{j}./AvsegNum.^3.*(3.*AvsegNum-4).*ac(3, i); %Eq. A44 of reference
                    der_b{i,j} = dAvsegNum_xi{j}./AvsegNum.^2.*bc(2, i) + dAvsegNum_xi{j}./AvsegNum.^3.*(3.*AvsegNum-4).*bc(3, i); %Eq. A45 of reference
                end
            end
            abcoefs.values           = ab_coefs;
            abcoefs.derivatives_axi = der_a;
            abcoefs.derivatives_bxi = der_b;

        end
    end
end


%{
  Copyright 2009-2021 SINTEF ICT, Applied Mathematics.

  This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

  MRST is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  MRST is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
