classdef AuxillaryVariables < StateFunction
%
%
% SYNOPSIS:
%   AuxillaryVariables(model, varargin)
%
% DESCRIPTION:
%
% PARAMETERS:
%   model    - 
%   varargin - 
%
% RETURNS:
%   class instance
%
% EXAMPLE:
%
% SEE ALSO:
%


    properties
    end
    
    methods
        function gp = AuxillaryVariables(model, varargin)
            gp@StateFunction(model, varargin{:});
            gp = gp.dependsOn({'Density', 'Diameters'});
            gp = gp.dependsOn({'x'}, 'state');
            
            gp.label = 'Auxillary Variables';
        end

        function AuxVars = evaluateOnDomain(prop, model, state)
        % Computes the auxillary variables (denoted zeta in reference), see A.8 in ref1            
            
            x     = model.getProps(state, 'x');
            diams = model.getProps(state, 'Diameters');
            rho   = model.getProps(state, 'Density');
            
            segNum    = model.segNum;            
            numC      = model.numC;
            d         = diams.d;
            
            % Start computation
            Auxvar        = cell(4, 1);
            dauxvar_drho  = cell(4, 1);
            dauxvar_dxi   = cell(4, numC);

            for j = 1 : 4
                Auxvar{j} = 0;
                if iscell(x)                               
                    for i = 1 : numC                   
                        Auxvar{j} = Auxvar{j} + x{i}.*segNum(i).*d{i}.^(j - 1);                    
                        dauxvar_dxi{j,i} = pi./6.*segNum(i).*rho.*d{i}.^(j - 1);
                    end
                else                    
                    for i = 1 : numC                    
                        Auxvar{j} = Auxvar{j} + x(i).*segNum(i).*d{i}.^(j - 1);                   
                        dauxvar_dxi{j,i} = pi./6.*segNum(i).*rho.*d{i}.^(j - 1);
                    end
                end
                    
                dauxvar_drho{j} = pi/6.*Auxvar{j};
                Auxvar{j}       = pi/6.*rho.*Auxvar{j}; 
            end
            AuxVars.values          = Auxvar;
            AuxVars.derivatives_rho = dauxvar_drho;
            AuxVars.derivatives_xi  = dauxvar_dxi;
            
        end
    end
end

