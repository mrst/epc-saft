classdef AdispCompressibilityConstants < StateFunction
%
%
% SYNOPSIS:
%   AdispCompressibilityConstants(model, varargin)
%
% DESCRIPTION:
%   Compute  compressibility constants C1 and C2 for the dispersion contribution
%   See Eq. 8 and 9 of the report
% PARAMETERS:
%   model - epc-saft model
%   state -  state with computed reduced density
%
% RETURNS:
%    C - Constants functions C1 and C2 to compute the dispersion Helmoltz energy
%
% EXAMPLE:
%    see details in the report
% SEE ALSO: AdispChainConstants
%


    properties
    end
    
    methods
        function gp = AdispCompressibilityConstants(model, varargin)
            gp@StateFunction(model, varargin{:});
            gp = gp.dependsOn({'AveragedSegmentNumber', 'AuxillaryVariables'});
            gp = gp.dependsOn({'ReducedDensity'}, 'state');
            
            gp.label = 'Compressibility Constants';
        end

        function C = evaluateOnDomain(prop, model, state)
        % Computes constants C1 and C2, see eqs A.11 and A.31 of ref1
            
            AvsegNum = model.getProp(state, 'AveragedSegmentNumber');
            Auxvars  = model.getProp(state, 'AuxillaryVariables');
            rho_red  = model.getProp(state, 'ReducedDensity');
            
            daux_dxi   = Auxvars.derivatives_xi;
            AvsegNum   = AvsegNum.values;
            segNum     = model.segNum;
            numC       = model.numC;
            
            % Start computation
            term1 = AvsegNum.*(8.*rho_red - 2.*rho_red.^2)./(1 - rho_red).^4;
            term2 = (1 - AvsegNum).*(20.*rho_red - 27.*rho_red.^2 + 12.*rho_red.^3 ...
                                     - 2.*rho_red.^4);
            term3 = ((1 - rho_red).*(2 - rho_red)).^2;
            C1    = (1 + term1 + term2./term3).^(-1); % Eq. 9 im the report
            term1 = AvsegNum.*(-4.*rho_red.^2+20.*rho_red + 8)./(1 - rho_red).^5;
            term2 = (1 - AvsegNum).*(2.*rho_red.^3 + 12.*rho_red.^2 - 48.*rho_red + 40);
            term3 = ((1 - rho_red).*(2 - rho_red)).^3;
            
            Q  = term1 + term2./term3;
            
            C2 = -(C1.^2).*Q; % Eq. A.31 of ref1
            
            CC = cell(1, 2);
            
            CC{1}    = C1;
            CC{2}    = C2;
            dC1_xi   = cell(1, numC);
            
            for i = 1: numC
                term1       = segNum(i).*(8.*rho_red - 2.*rho_red.^2)./(1-rho_red).^4;
                term2       = segNum(i).*(20.*rho_red - 27.*rho_red.^2 + 12.*rho_red.^3 - 2.*rho_red.^4)./((1-rho_red).*(2-rho_red)).^2;  
                dC1_xi{i}   = C2.*daux_dxi{4,i} - C1.^2.*(term1 - term2); % Eq. A41 of Chapman reference
            end
            
            C.values             = CC;
            C.derivatives_C1xi   = dC1_xi;

        end
    end
end

%{
  Copyright 2009-2021 SINTEF ICT, Applied Mathematics.

  This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

  MRST is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  MRST is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}