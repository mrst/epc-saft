classdef PCSaftFugacity < StateFunction
%
%
% SYNOPSIS:
%   PCSaftFugacity(model, varargin)
%
% DESCRIPTION:
%   Translates the fugacity coefficient from the fugacity value
% PARAMETERS:
%   model    - pcsaft model
%   varargin - fugacity coefficient (phi)
%            - state: pressure (P) 
%            - state: composition (x)
% RETURNS:
%   f - fugacity 
%
% EXAMPLE:
%   f = phi*P*x;
% SEE ALSO:
%   FugacityCoefficient
%   ChemicalPotentials

    properties
    end
    
    methods
        function gp = PCSaftFugacity(model, varargin)
            gp@StateFunction(model, varargin{:});
            gp = gp.dependsOn({'FugacityCoefficient'});
            gp = gp.dependsOn({'pressure'}, 'state');
            gp = gp.dependsOn({'x'}, 'state');
            
            gp.label = 'Fugacity';
        end

        function f = evaluateOnDomain(prop, model, state)

            phi = model.getProp(state, 'FugacityCoefficient');
            x   = model.getProp(state, 'x');
            p   = model.getProp(state, 'Pressure');
            
            numC  = model.numC;


            if iscell(x) && iscell(phi)               
                f = cell(1,numC);                
                for i = 1:numC
                    f{i} = phi{i}.*p.*x{i};
                end
            else               
                f = phi.*bsxfun(@times, p, x);
            end
            

        end
    end
end

%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}