classdef PcsaftSaltFugacity < StateFunction
%
% SYNOPSIS:
%   PcsaftSaltFugacity(model, varargin)
%
% DESCRIPTION:
%   Calculate the Salt fugacity in the solid and liquid phases  and  check Salt
%   existence
% PARAMETERS:
%   model    - epc-saft model
%   varargin - state with a given pressure, temperature and composition
%
% RETURNS:
%   class instance
%
% EXAMPLE:
%
% SEE ALSO: 
%   PCSaftFugacity, PCSaftFugacityCoefficient



    properties
    end
    
    methods
        function gp = PcsaftSaltFugacity(model, varargin)
            gp@StateFunction(model, varargin{:});
            gp = gp.dependsOn('FugacityCoefficient');
            gp = gp.dependsOn('CompressibilityFactor');
            gp = gp.dependsOn({'P'}, 'state');
            gp = gp.dependsOn({'T'}, 'state');
            gp = gp.dependsOn({'x'}, 'state');

            
            gp.label = 'SaltFugacity';
        end

        function fS = evaluateOnDomain(prop, model, state,opt)

            CN  = model.CN;
            P   = model.getProp(state, 'pressure');
            T   = model.getProp(state, 'T');
            x   = model.getProp(state, 'components');

            phiw   = model.getProp(state, 'FugacityCoefficient');
            Zw     = model.getProp(state, 'CompressibilityFactor');


            %% Calculate the Salt Fugacity in solid phase     
            f_S = -3.998e+06 + 8975.*T - 5642.*(P./1e5) + 46.9 .*T.^2 + 28.74.*T.*(P./1e5);
            
            %% Calculate the Salt Fugacity  in aqueous phase fS = phi*x*P   
            Na = find(strcmp(model.names, 'Na'));
            Cl = find(strcmp(model.names, 'Cl'));            
            if iscell(x) 
                fS_W = (phiw{Na}.^CN(Cl).*phiw{Cl}.^CN(Na)).^(1./(CN(Na) + CN(Cl))).*(x{Na} + x{Na}).*P;
            else                                
                fS_W = (phiw(:,Na).^CN(Cl).*phiw(:,Cl).^CN(Na)).^(1./(CN(Na) + CN(Cl))).*(x(:, Na) + x(:, Cl)).*P;
            end
            %% Criteria for  Salt Existence check
            isSalt = log(fS_W) > log(f_S);

            %% Output
            fS.SolidSalt   = f_S;
            fS.AqueousSalt = fS_W;
            fS.phiw        = phiw;
            fS.Zw          = Zw;
            fS.isSalt      = isSalt;
            
        end
    end
end

%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}

