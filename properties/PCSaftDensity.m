classdef PCSaftDensity < StateFunction
%
% SYNOPSIS:
%   PCSaftDensity(model, varargin)
%
% DESCRIPTION:
%   Compute the density from the reduced density and composition
% PARAMETERS:
%   model    - pcsaft model : 
%                             segment dimater (diams)
%                             segment number  (m)   
%   varargin - state: 
%                     reduced density (rho)  
%                     composition (x)
%
% RETURNS:
%   redrho - reduced density
%
% EXAMPLE:
%   redrho = 6/pi*rho*sum(x*m*diams.^3)^(-1)
% SEE ALSO:
%   CompressibilityFactor
%   ChemicalPotentials

    
    properties
    
    end
    
    methods
        function gp = PCSaftDensity(model, varargin)
            gp@StateFunction(model, varargin{:});
            gp = gp.dependsOn({'Diameters'});
            gp = gp.dependsOn({'x', 'ReducedDensity'}, 'state');
            
            gp.label = 'Density';
        end

        function rho = evaluateOnDomain(prop, model, state)
           
            x        = model.getProp(state, 'x');
            dens_red = model.getProp(state, 'ReducedDensity');
            diams    = model.getProp(state, 'Diameters');
            
            m    = model.segNum;
            numC = model.numC;
            d    = diams.d;
            % Start computation
            if iscell(x)            
                sum1 = 0;            
                for i = 1 : numC
               
                    sum1 = sum1 + x{i}.*m(i).*d{i}.^3;
                end
            else                
                sum1 = 0;
                for i = 1 : numC                
                    sum1 = sum1 + x(i).*m(i).*d{i}.^3;
                end            
            end
            rho = 6/pi.*dens_red.*sum1.^(-1);             
        end
    end
end

%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
