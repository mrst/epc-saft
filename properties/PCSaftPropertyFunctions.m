classdef PCSaftPropertyFunctions < StateFunctionGrouping
%
%
% SYNOPSIS:
%   PCSaftPropertys(model)
%
% DESCRIPTION:
%   adding state functions to the epc-saft model
% PARAMETERS:
%   model - epc-saft EoS
%
% RETURNS:
%   class instance
%
% EXAMPLE:
%
% SEE ALSO:
%    DensityModel, PcSaftDensityPropertyFunctions


    % Grouping for PC Saft
    properties
        Density
        Diameters
        AuxillaryVariables
        DielectricConstants
        AveragedSegmentNumber
        RadialHardSpherDistribution
        SiteInteractionStrength
        AdispChainConstants
        FreeSiteMoleFraction
        AdispCompressibilityConstants
        AdispIntegralApproximations
        AdispMixingVals
        DebyeConstant
        AssociativeHelmoltzEnergy
        DissipativeHelmoltzEnergy
        HardChainHelmoltzEnergy
        IonicHelmoltzEnergy
        HardSphereHelmoltzEnergy
        Pressure
        CompressibilityFactor
        ChemicalPotentials
        Fugacity
        FugacityCoefficient
        PcsaftSaltFugacity
        DipolHelmoltzEnergy
    end

    methods
        function props = PCSaftPropertyFunctions(model)
            props@StateFunctionGrouping('PCSaftProps');
            
            props = props.setStateFunction('Density'                      , PCSaftDensity(model));
            props = props.setStateFunction('Diameters'                    , Diameters(model));
            props = props.setStateFunction('AuxillaryVariables'           , AuxillaryVariables(model));
            props = props.setStateFunction('DielectricConstants'          , DielectricConstants(model));
            props = props.setStateFunction('AveragedSegmentNumber'        , AveragedSegmentNumber(model));
            props = props.setStateFunction('RadialHardSpherDistribution'  , RadialHardSpherDistribution(model));
            props = props.setStateFunction('SiteInteractionStrength'      , SiteInteractionStrength(model));
            props = props.setStateFunction('AdispChainConstants'          , AdispChainConstants(model));
            props = props.setStateFunction('FreeSiteMoleFraction'         , FreeSiteMoleFraction(model));
            props = props.setStateFunction('AdispCompressibilityConstants', AdispCompressibilityConstants(model));
            props = props.setStateFunction('AdispIntegralApproximations'  , AdispIntegralApproximations(model));
            props = props.setStateFunction('AdispMixingVals'              , AdispMixingVals(model));
            props = props.setStateFunction('DebyeConstant'                , DebyeConstant(model));
            props = props.setStateFunction('AssociativeHelmoltzEnergy'    , AssociativeHelmoltzEnergy(model));
            props = props.setStateFunction('DissipativeHelmoltzEnergy'    , DissipativeHelmoltzEnergy(model));
            props = props.setStateFunction('HardChainHelmoltzEnergy'      , HardChainHelmoltzEnergy(model));
            props = props.setStateFunction('IonicHelmoltzEnergy'          , IonicHelmoltzEnergy(model));
            props = props.setStateFunction('HardSphereHelmoltzEnergy'     , HardSphereHelmoltzEnergy(model));
            props = props.setStateFunction('Pressure'                     , PCSaftPressure(model));            
            props = props.setStateFunction('TotalHelmoltzEnergy'          , TotalHelmoltzEnergy(model));
            props = props.setStateFunction('CompressibilityFactor'        , CompressibilityFactor(model));
            props = props.setStateFunction('ChemicalPotentials'           , ChemicalPotentials(model));
            props = props.setStateFunction('Fugacity'                     , PCSaftFugacity(model));
            props = props.setStateFunction('FugacityCoefficient'          , PCSaftFugacityCoefficient(model)); 
            props = props.setStateFunction('PcsaftSaltFugacity'           , PcsaftSaltFugacity(model));
            props = props.setStateFunction('DipolHelmoltzEnergy'          , DipolHelmoltzEnergy(model));  


        end
    end
end

%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
