classdef AdispMixingVals < StateFunction
%
%
% SYNOPSIS:
%   AdispMixingVals(model, varargin)
%
% DESCRIPTION:
%   compute the mixing rules for the dispersion interactions
% PARAMETERS:
%   model - epc-saft model
%   state - state with a given composition and temperature  
%
% RETURNS:
%   mixingvals - mixing values for the dispersion interactions with their
%   derivatives with respect to composition & density
%
% EXAMPLE:
%
% SEE ALSO: AdispIntegralApproximations, AdispChainConstants
%


    properties
    end
    
    methods
        function gp = AdispMixingVals(model, varargin)
            gp@StateFunction(model, varargin{:});
            gp = gp.dependsOn({'x', 'T'}, 'state');
            
            gp.label = 'Mixing Values';
        end

        function mixingvals = evaluateOnDomain(prop, model, state)
        % Compute average values obtained from mixingrules, see A.12 and A.13
        % in ref1.
            
            x = model.getProp(state, 'x');
            T = model.getProp(state, 'T');
            
            epsilon = model.epsilon;
            sigmaij   = model.sigmaij;            
            segNum    = model.segNum;
            CN    = model.CN; 
            numC      = model.numC;

            binar = 3.211.*10.^(-8).*T.^3-4.515.*(10.^(-5)).*T.^2+0.02082.*T-2.996;
            %binar =-2.51.*10^(-5).*T.^(2) +2.24 .*10.^(-2).*T -4.44;
            for i = 1 : numC            
                for j = 1 : numC
                    if (CN(i) == 0) || (CN(j) == 0) % The ions do not have dispersion with each other.
                        epsilonij(i, j) = sqrt(epsilon(i)*epsilon(j)); % see A.15 of ref1     	
                    end
                end
            end
            
            % Intermediate coefficient
            sigmaIJ =cell(numC,numC);
            
            % Start computation
            mv1 = 0;
            mv2 = 0;
            

            % Compute derivatives in function of xi
            der_prom1 = cell(1,numC);
            der_prom2 = cell(1,numC);
            
            for i = 1 : numC                   
                sum1 = 0;
                sum2 = 0;
                for j = 1 : numC                    
                    sigmaIJ{i,j} = epsilonij(i, j)./T.*(1-binar.*(i~=j)).*sigmaij(i, j).^3;
                    mv1 = mv1 + x{i}.*x{j}.*segNum(i).*segNum(j).*sigmaIJ{i,j}; % Eq. A12 of reference 1
                    mv2 = mv2 + x{i}.*x{j}.*segNum(i).*segNum(j).*(epsilonij(i, j)./T.*(1-binar.*(i~=j))).*sigmaIJ{i,j}; % Eq. A13 of reference 2
                    
                    sum1 = sum1 + x{j}.*segNum(j).*sigmaIJ{i,j};
                    sum2 = sum2 + x{j}.*segNum(j).*(epsilonij(i, j)./T.*(1-binar.*(i~=j))).*sigmaIJ{i,j};
                                
                end                               
                der_prom1{i} = 2.*segNum(i).*sum1; %Eq. A39 of reference
                der_prom2{i} = 2.*segNum(i).*sum2; %Eq. A40 of reference
            end
                       
            mixing_vals    = cell(2, 1);
            mixing_vals{1} = mv1;
            mixing_vals{2} = mv2;
            
            mixingvals.values          = mixing_vals;
            mixingvals.derivatives_1xi = der_prom1;
            mixingvals.derivatives_2xi = der_prom2;

        end
    end
end

%{
  Copyright 2009-2021 SINTEF ICT, Applied Mathematics.

  This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

  MRST is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  MRST is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
