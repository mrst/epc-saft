classdef Diameters < StateFunction
%
% SYNOPSIS:
%   Diameters(model, state.Temperature)
%
% DESCRIPTION:
%   Computes the segment diameter for PC-SAFT model
% PARAMETERS:
%   model    - pcsaftmodel segNum
%                        segment energy parameter  (epsilon)
%                          sigma 
%   varargin - state 
%                    Temperature (T)
%
% RETURNS:
%   diams  - segment diameter
%
% EXAMPLE:
%       d{i} = sigma(i)*(1 - 0.12.*exp(-3*epsilon(i)./T))
%diams    = model.getProp(state, 'Diameters');
% SEE ALSO:
%    ChemicalPotentials

    properties
    end
    
    methods
        function gp = Diameters(model, varargin)
            gp@StateFunction(model, varargin{:});
            gp = gp.dependsOn({'T'}, 'state');
        end

        function diams = evaluateOnDomain(prop, model, state)
            
            T = model.getProp(state, 'T');
            
            epsilon = model.epsilon;
            sigma   = model.sigma;
            numC    = model.numC;
            n   = numel(sigma);
            d   = cell(numC,1);
            dij = cell(numC,numC);
             
            % d            
            for i = 1 : n            
                d{i} = sigma(i)*(1 - 0.12.*exp(-3*epsilon(i)./T));                
            end
                        
            % dij
              
            for i = 1 : numC                            
                for j = 1 : numC                    
                    sumij = d{i} + d{j};
                    dij{i,j}  = (d{i}.*d{j})./sumij;                    
                end
            end                                
            
            diams.d = d;
            diams.dij = dij;
            
            
        end
    end
end

%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
