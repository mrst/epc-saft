classdef IonicHelmoltzEnergy < StateFunction
%
%
% SYNOPSIS:
%   IonicHelmoltzEnergy(model, varargin)
%
% DESCRIPTION:
%   Compute the electrolyte/ionic Helmoltz free energy
% PARAMETERS:
%   model    - density model built with epc-saft EoS
%   varargin - state with a given pressure, temperature and composition 
%
% RETURNS:
%   Aion - the ionic Helmoltz free energy and its derivatives wrt the
%   composition x and density
%
% EXAMPLE:
%
% SEE ALSO:
%     HardSphereHelmoltzEnergy, TotalHelmoltzEnergy
    
    properties
    end
    
    methods
        function gp = IonicHelmoltzEnergy(model, varargin)
            gp@StateFunction(model, varargin{:});
            gp = gp.dependsOn({'DielectricConstants', 'DebyeConstant', 'Density'});
            gp = gp.dependsOn({'x', 'T'}, 'state');
            
            gp.label = 'Ionic Helmoltz Energy';
        end

        function Aion = evaluateOnDomain(prop, model, state)
        % Compute Aion for ionic model. 
            
   
        sigma = model.sigma;
        CN = model.CN;        
        e  = model.e;        
        numC = model.numC;        
        kb   = model.kb;                  
        x    = model.getProp(state, 'x');
        if any(sum(CN'.*[x{:}],2)~=0)
                
                T          = model.getProp(state, 'T');
                dielectric = model.getProp(state, 'DielectricConstants');
                kdebye     = model.getProp(state, 'DebyeConstant');
                rho        = model.getProp(state, 'Density');                
                
                % Start computation
                termx  = cell(numC, 1);
                sigmak = cell(numC, 1);
                sum1 = 0;
                sum2 = 0;
                k   = kdebye.values;
                dkx = kdebye.derivatives_xi; 

                for i = 1 : numC
                    
                    termx{i} = 3./((k.*sigma(i)).^3).*((3/2)+log(1+k.*sigma(i))-(2.*(1+k.*sigma(i)))+(0.5.*((1+k.*sigma(i)).^2)));
                    sigmak{i} = -2.*termx{i} + (3./(1 + k.*sigma(i)));
                    
                    sum1 = sum1 + x{i}.*(CN(i).*e).^2.*termx{i};  % used for Aion
                    sum2 = sum2 + x{i}.*(CN(i).*e).^2.*sigmak{i}; % Used for dAion_drho
                end
                A_ion = - k./12./pi./kb./T./dielectric.*sum1*(10.^10);
                
                % First derivative with respect to rho
                dAion_rho = - k/24./pi./kb./T./dielectric./rho.*sum2.*(10.^10);
                
                % Second derivative with respect to xi
                dAion_xi = cell(numC, 1);
                for i = 1 : numC                
                    if ~all(model.CN == 0)
                        dAion_xi{i} = - 1/12/pi/kb./T./dielectric*((CN(i)*e)^2).*k.*termx{i}*10^10- 1/12/pi/kb./T./dielectric.*sum2.*dkx{i}.*10^10;
                    else
                        % we get right dimension
                        dAion_xi{i} = - 1/12/pi/kb./T./dielectric*0;
                    end
                end
           
                
        else            
            % In case there is no ionic effect. We set A_ion and its
            % derivative to zero
            A_ion = 0;
            dAion_rho = 0;               
            for i = 1 : numC                                        
                dAion_xi{i} = 0;
            end
                
                
        end        
        Aion.values  = A_ion;           
        Aion.derivatives_rho = dAion_rho; 
        Aion.derivatives_xi  = dAion_xi;
        end
    end
end

%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
