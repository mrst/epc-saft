classdef DipolHelmoltzEnergy < StateFunction
%
%
% SYNOPSIS:
%   DipolHelmoltzEnergy(model, varargin)
%
% DESCRIPTION:
%   Compute the d-polar Helmoltz free energy
% PARAMETERS:
%   model    - density model built with epc-saft EoS
%   varargin - state with a given pressure, temperature and composition 
%
% RETURNS:
%   Aion - the polar Helmoltz free energy and its derivatives wrt the
%   composition x and density
%
% EXAMPLE:
%
% SEE ALSO:
%     HardSphereHelmoltzEnergy, TotalHelmoltzEnergy
    
    properties
    end
    
    methods
        function gp = DipolHelmoltzEnergy(model, varargin)
            gp@StateFunction(model, varargin{:});
            gp = gp.dependsOn({'Diameters', 'Density'});
            gp = gp.dependsOn({'ReducedDensity'}, 'state');
            gp = gp.dependsOn({'x', 'T'}, 'state');
            
            gp.label = 'Ionic Helmoltz Energy';
        end

        function Apolar = evaluateOnDomain(prop, model, state)
        % Compute Aion for ionic model.
            x = model.getProp(state, 'x');
            T = model.getProp(state, 'T');
            rho_red     = model.getProp(state, 'ReducedDensity');
            rho_red = rho_red.*6.02214076.*1e23./1e30;
            eta = model.getProp(state, 'Density');
            diams    = model.getProp(state, 'Diameters');
            

            
            epsilon = model.epsilon;
            epsilonij = model.epsilonij;
            sigmaij   = model.sigmaij;

            dipMo = model.dipMo;
            dipNum = model.dipNum;
            
            segNum    = model.segNum;
            CN    = model.CN; 
            numC      = model.numC;
            mu_polar = zeros(1, numC);

            d    = diams.d;
            if ~isempty(dipMo)                   
               A2 = 0;
               A3 = 0;
               dA2_det = 0;
               dA3_det = 0;
               dA2_dx = cell(numC, 1);
               dA2_dx{1}=0;
               dA2_dx{numC}=0;
               dA3_dx = cell(numC,1);
               dA3_dx{1}=0;
               dA3_dx{numC}=0;

               a0dip = [0.3043504, -0.1358588, 1.4493329, 0.3556977, -2.0653308];
               a1dip = [0.9534641, -1.8396383, 2.0131180, -7.3724958, 8.2374135];
               a2dip = [-1.1610080, 4.5258607, 0.9751222, -12.281038, 5.9397575];
               b0dip = [0.2187939, -1.1896431, 1.1626889, 0, 0];
               b1dip = [-0.5873164, 1.2489132, -0.5085280, 0, 0];
               b2dip = [3.4869576, -14.915974, 15.372022, 0, 0];
               c0dip = [-0.0646774, 0.1975882, -0.8087562, 0.6902849, 0];
               c1dip = [-0.9520876, 2.9924258, -2.3802636, -0.2701261, 0];
               c2dip = [-0.6260979, 1.2924686, 1.6542783, -3.4396744, 0];

           
               for i = 1:numC                   
                  % for j = 1:numC                    
                  %     if (CN(i) == 0) || (CN(j) == 0) % The ions do not have dispersion with each other.
                 %       epsilonij(i, j) = sqrt(epsilon(i)*epsilon(j)); % see A.15 of ref1     	
                 %      end
                 %  end
                                  
                   conv = 7242.702976750923; % conversion factor
                   dipmSQ(i) = dipMo(i).^2./(segNum(i).*model.epsilonij(i,i).*model.sigmaij(i,i).^3).*conv;
               end
               adip = zeros(1, 5);
               bdip = zeros(1, 5);
               cdip = zeros(1, 5);
              for i = 1:numC           
                  for j = 1:numC               
                      m_ij = sqrt(segNum(i)*segNum(j));               
                     % epsilonij(i, j) = sqrt(epsilon(i)*epsilon(j)); % see A.15 of ref1                
                      if m_ij > 2
                          m_ij = 2;
                      end               
                      J2 = 0;
                      dJ2_det = 0;
                      detJ2_det = 0;              
                      for l = 1:5                  
                          adip(l) = a0dip(l) + (m_ij-1)./m_ij*a1dip(l) + (m_ij-1)./m_ij*(m_ij-2)./m_ij*a2dip(l);
                          bdip(l) = b0dip(l) + (m_ij-1)./m_ij*b1dip(l) + (m_ij-1)./m_ij*(m_ij-2)./m_ij*b2dip(l);
                          J2 = J2 + (adip(l) + bdip(l).*epsilonij(i, j)./T).*eta.^(l-1);
                          dJ2_det = dJ2_det  + (adip(l) + bdip(l).*epsilonij(i, j)./T).*(l-1).*(eta.^(l-2));
                          detJ2_det = detJ2_det+ (adip(l) + bdip(l)*epsilonij(i, j)./T).*(l).*eta.^(l-1);
                      end
                      A2 = A2 + x{i}.*x{j}.*epsilonij(i, i)./T.*epsilonij(j, j)./T.*(sigmaij(i,i).^3).*(sigmaij(j,j).^3)./(sigmaij(i,j).^3).*dipNum(i).*dipNum(j).*dipmSQ(i).*dipmSQ(j).*J2;
                      dA2_det = dA2_det + x{i}.*x{j}*epsilonij(i, i)./T.*epsilonij(j, j)./T*(sigmaij(i,i).^3).*(sigmaij(j,j).^3)./(sigmaij(i,j).^3).*dipNum(i).*dipNum(j).*dipmSQ(i).*dipmSQ(j).*detJ2_det;

                      if (i == j)                          
                          dA2_dx{i} = dA2_dx{i} + epsilonij(i, i)./T.*epsilonij(j, j)./T.*(sigmaij(i,i).^3).*(sigmaij(j,j).^3)./(sigmaij(i,j).^3).*dipNum(i).*dipNum(j).*dipmSQ(i).*dipmSQ(j).*(x{i}.*x{j}.*dJ2_det.*pi./6.*rho_red.*segNum(i).*(d{i}.^3) + 2.*x{j}.*J2);
                      else                 
                          dA2_dx{i} = dA2_dx{i} + epsilonij(i, i)./T.*epsilonij(j, j)./T.*(sigmaij(i,i).^3).*(sigmaij(j,j).^3)./(sigmaij(i,j).^3).*dipNum(i).*dipNum(j).*dipmSQ(i).*dipmSQ(j).*(x{i}.*x{j}.*dJ2_det.*pi./6.*rho_red.*segNum(i).*(d{i}.^3) + x{j}.*J2);
                      end
                               
                      for k = 1:numC                
                          m_ijk = (segNum(i).*segNum(j).*segNum(k)).^(1/3);
                          if (m_ijk > 2)
                              m_ijk = 2;
                          end
                          J3 = 0.;                   
                          dJ3_det = 0.; 
                          detJ3_det = 0.;
                          for l = 1:5
                              cdip(l) = c0dip(l) + (m_ijk-1)./m_ijk*c1dip(l) + (m_ijk-1)./m_ijk*(m_ijk-2)./m_ijk.*c2dip(l);                        
                              J3 = J3 + cdip(l).*(eta.^(l-1));
                              dJ3_det = dJ3_det + cdip(l).*(l-1).*(eta.^(l-2));                     
                              detJ3_det = detJ3_det + cdip(l).*(l+1).*(eta.^l);
                          end
                          A3 = A3 + x{i}.*x{j}.*x{k}.*epsilonij(i, i)./T.*epsilonij(j, j)./T.*epsilonij(k, k)./T.*(sigmaij(i,i).^3).*(sigmaij(j,j).^3).*(sigmaij(k, k).^3)./sigmaij(i,j)./sigmaij(i, k)./sigmaij(j, k).*dipNum(i).*dipNum(j).*dipNum(k).*dipmSQ(i).*dipmSQ(j).*dipmSQ(k).*J3;                    
                          dA3_det = dA3_det + x{i}.*x{j}.*x{k}.*epsilonij(i, i)./T.*epsilonij(j, j)./T.*epsilonij(k, k)./T.*(sigmaij(i,i).^3).*(sigmaij(j,j).^3).*(sigmaij(k, k).^3)./sigmaij(i,j)./sigmaij(i, k)./sigmaij(j, k).*dipNum(i).*dipNum(j).*dipNum(k).*dipmSQ(i).*dipmSQ(j).*dipmSQ(k).*detJ3_det;                 
                          if ((i == j) && (i == k))  
                              dA3_dx{i} = dA3_dx{i} + epsilonij(i, i)./T.*epsilonij(j, j)./T.*epsilonij(k, k)./T.*(sigmaij(i,i).^3).*(sigmaij(j,j).^3).*(sigmaij(k, k).^3)./sigmaij(i,j)./sigmaij(i, k)./sigmaij(j, k).*dipNum(i).*dipNum(j).*dipNum(k).*dipmSQ(i).*dipmSQ(j).*dipmSQ(k).*(x{i}.*x{j}.*x{k}.*dJ3_det.*pi/6.*rho_red.*segNum(i).*(d{i}.^3)+ 3*x{j}.*x{k}.*J3);                    
                          elseif ((i == j) || (i == k)) 
                              dA3_dx{i} = dA3_dx{i} + epsilonij(i, i)./T.*epsilonij(j, j)./T.*epsilonij(k, k)./T.*(sigmaij(i,i).^3).*(sigmaij(j,j).^3).*(sigmaij(k, k).^3)./sigmaij(i,j)./sigmaij(i, k)./sigmaij(j, k).*dipNum(i).*dipNum(j).*dipNum(k)*dipmSQ(i).*dipmSQ(j).*dipmSQ(k).*(x{i}.*x{j}.*x{k}.*dJ3_det.*pi/6.*rho_red.*segNum(i).*(d{i}.^3) + 2.*x{j}.*x{k}.*J3);
                          else                        
                              dA3_dx{i} = dA3_dx{i} + epsilonij(i, i)./T.*epsilonij(j, j)./T.*epsilonij(k, k)./T.*(sigmaij(i,i).^3).*(sigmaij(j,j).^3).*(sigmaij(k, k).^3)./sigmaij(i,j)./sigmaij(i, k)./sigmaij(j, k).*dipNum(i).*dipNum(j).*dipNum(k).*dipmSQ(i).*dipmSQ(j).*dipmSQ(k).*(x{i}.*x{j}.*x{k}.*dJ3_det.*pi/6.*rho_red.*segNum(i).*(d{i}.^3)+ x{j}.*x{k}.*J3);
                          end
                      end
                  end
              end

        
              A2 = -pi.*rho_red.*A2;
              A3 = -4/3.*pi*pi.*rho_red.*rho_red.*A3;        
              dA2_det = -pi.*rho_red./eta.*dA2_det;
              dA3_det = -4/3.*pi.*pi.*rho_red./eta.*rho_red./eta.*dA3_det;
              for i = 1:numC
                  dA2_dx{i} = -pi.*rho_red.*dA2_dx{i};            
                  dA3_dx{i} = -4/3.*pi*pi.*rho_red.*rho_red.*dA3_dx{i};
              end

              for i = 1:numC
                  dapolar_dx{i} = (dA2_dx{i}.*(1-A3./A2) + (dA3_dx{i}.*A2 - A3.*dA2_dx{i})./A2)./((1-A3./A2).^2);
              end
              mu_polar = cell(numC, 1);
              mu_polar{1}=0;
              mu_polar{2}=0;
              %A2.val(value(A2)==0) = 1.0e-16;
          %    A2.jac{1}.diagonal(value(A2)==0) = 1;
            % if (value(A2) ~= 0) % when the mole fraction of the polar compounds is 0 then A2 = 0 and division by 0 occurs
         
              dapolar_drho=((dA2_det.*(1-A3./A2)+(dA3_det.*A2-A3.*dA2_det)./A2)./(1-A3./A2)./(1-A3./A2));
              ares_polar = A2./(1-A3./A2);             
              %ares_polar.val(value(A2)==0) = 0;
              %ares_polar.jac{1}.diagonal(value(A2)==0) = 1;


%                   Zpolar = eta.*((dA2_det.*(1-A3./A2)+(dA3_det.*A2-A3.*dA2_det)./A2)./(1-A3./A2)./(1-A3./A2));
%                   Zpolar.val(value(A2)==0) = 0;
%                   Zpolar.jac{1}.diagonal(value(A2)==0) = 1;
% 
% 
%                   for i = 1:numC
%                       for j = 1:numC                  
%                           mu_polar{i} = mu_polar{i} + x{j}.*dapolar_dx{j};
%                       end
%                       mu_polar{i} = ares_polar + Zpolar + dapolar_dx{i} - mu_polar{i};
%                       mu_polar{i}.val(value(A2)==0) = 0;
%                       mu_polar{i}.jac{1}.diagonal(value(A2)==0) = 1;
% 
% 
%                   end

%             if isa(A2,'ADI')
%                   ares_polar = A2./(1-A3./A2);             
%                   ares_polar.val(value(A2)==0) = 0;
%                   ares_polar.jac{1}.diagonal(value(A2)==0) = 1;
% 
% 
%                   Zpolar = eta.*((dA2_det.*(1-A3./A2)+(dA3_det.*A2-A3.*dA2_det)./A2)./(1-A3./A2)./(1-A3./A2));
%                   Zpolar.val(value(A2)==0) = 0;
%                   Zpolar.jac{1}.diagonal(value(A2)==0) = 1;
% 
% 
%                   for i = 1:numC
%                       for j = 1:numC                  
%                           mu_polar{i} = mu_polar{i} + x{j}.*dapolar_dx{j};
%                       end
%                       mu_polar{i} = ares_polar + Zpolar + dapolar_dx{i} - mu_polar{i};
%                       mu_polar{i}.val(value(A2)==0) = 0;
%                       mu_polar{i}.jac{1}.diagonal(value(A2)==0) = 1;
% 
% 
%                   end
%             else
%                   ares_polar = A2./(1-A3./A2);             
%                   ares_polar(value(A2)==0) = 0;
% 
%                   Zpolar = eta.*((dA2_det.*(1-A3./A2)+(dA3_det.*A2-A3.*dA2_det)./A2)./(1-A3./A2)./(1-A3./A2));
%                   Zpolar(value(A2)==0) = 0;
% 
%                   for i = 1:numC
%                       for j = 1:numC                  
%                           mu_polar{i} = mu_polar{i} + x{j}.*dapolar_dx{j};
%                       end
%                       mu_polar{i} = ares_polar + Zpolar + dapolar_dx{i} - mu_polar{i};
%                       mu_polar{i}(value(A2)==0) = 0;
%                   end
%             
%             end
              kb   = model.kb;                  

              Apolar.values           = ares_polar;   
              Apolar.derivatives_rho           = dapolar_drho; 
              Apolar.derivatives_xi = dapolar_dx;
            end
        end
    end
end

%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
