classdef PCSaftPressure < StateFunction
%
%
% SYNOPSIS:
%   PCSaftPressure(model, varargin)
%
% DESCRIPTION:
%   compute the pressure from a state with a given pressure & temperature
%   and update reduced density
% PARAMETERS:
%   model    - a density model built with epc-saft EoS
%   varargin - state with a given pressure, temperature and composition
%
% RETURNS:
%   class instance
%
% EXAMPLE:
%
% SEE ALSO:
%    DensityModel, PCSaftDensity
%


    properties
    end
    
    methods
        function gp = PCSaftPressure(model, varargin)
            gp@StateFunction(model, varargin{:});
            gp = gp.dependsOn({'CompressibilityFactor', 'Density'});
            gp = gp.dependsOn({'T'}, 'state');
            
            gp.label = 'Pressure';
        end

        function P = evaluateOnDomain(prop, model, state)
            
        % We use the definition of the compressibility factor $P = Z\rho R T$
            
            Z    = model.getProp(state, 'CompressibilityFactor');
            dens = model.getProp(state, 'Density');
            T    = model.getProp(state, 'T');
            
            kb = model.kb;            
            P = Z*kb.*T.*dens*(1e10)^3;
            
        end
    end
end

%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
