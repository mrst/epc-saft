classdef DebyeConstant < StateFunction
%
%
% SYNOPSIS:
%   DebyeConstant(model, varargin)
%
% DESCRIPTION:
%      Calculate the Debey's constant used to compute the ionic 
%      Helmoltz free energy
% PARAMETERS:
%   model    - a density model built with epc-saft EoS
%   varargin - state with a given pressure, temperature and composition
%
% RETURNS:
%   kdebye - the values of the Debey constant   and its derivatives 
%   wrt the composition x
%
% EXAMPLE:
%
% SEE ALSO: 
%   IonicHelmoltzEnergy
%


    properties
    end
    
    methods
        function gp = DebyeConstant(model, varargin)
            gp@StateFunction(model, varargin{:});
            gp = gp.dependsOn({'Density', 'DielectricConstants'});
            gp = gp.dependsOn({'x', 'T'}, 'state');
            
            gp.label = 'Debye Constant';
        end

        function kdebye = evaluateOnDomain(prop, model, state)
        % Computes dielectric constant of salt from tan 2005 4442-4442
            
            kb   = model.kb;
            CN   = model.CN;
            numC = model.numC;
            e    = model.e;
            
            rho = model.getProp(state, 'Density');
            dilectriCst = model.getProp(state, 'DielectricConstants');
            x = model.getProp(state, 'x');
            T = model.getProp(state, 'T');
            
            if all(CN == 0)
                % in case there is no ionic effect. We set kd and dkx to zero
                kd = 0;
                for i = 1 : numC
                    dkx{i} = 0;
                end
            else            
                M = 0;
                if iscell(x)
                    for i = 1 : numC
                        M = M + x{i}.*(CN(i).*e).^2;
                    end
                else
                    M = sum(bsxfun(@times, x, (CN.*e).^2), 2);
                end
                kd  = (rho./kb./T./dilectriCst.*M.*(10.^10)).^(0.5);
                dkx = cell(1, numC);

                for i = 1 : numC
                    dkx{i} = 0.5.*(CN(i).*e).^2.*kd./M;
                end
                
            end
            
            kdebye.values         = kd;
            kdebye.derivatives_xi = dkx;

        end
    end
end

%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
