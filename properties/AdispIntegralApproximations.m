classdef AdispIntegralApproximations < StateFunction
%
%
% SYNOPSIS:
%   AdispIntegralApproximations(model, varargin)
%
% DESCRIPTION:
%   compute the integral functions I1 and I2 to compute
%   the Helmoltz free energy for the dispersion interactions
% PARAMETERS:
%   model - epc-saft model
%   state - state with computed reduced density  
%
% RETURNS:
%   I1 and I2: integral functions with their derivatives
%
% EXAMPLE:
%   see details in the report
% SEE ALSO: AdispChainConstants, AdispChainConstants
%


    properties
    end
    
    methods
        function gp = AdispIntegralApproximations(model, varargin)
            gp@StateFunction(model, varargin{:});
            gp = gp.dependsOn({'AdispChainConstants', 'AuxillaryVariables'});
            gp = gp.dependsOn({'ReducedDensity'}, 'state');
            
            gp.label = 'Integral approximations';
        end

        function I = evaluateOnDomain(prop, model, state)
        % Compute values of I1 and I2
            
               abcoefs_all = model.getProp(state, 'AdispChainConstants');
               AuxVars     = model.getProp(state, 'AuxillaryVariables');
               rho_red     = model.getProp(state, 'ReducedDensity');
               
               numC     = model.numC;
               abcoefs  = abcoefs_all.values;
               der_a    = abcoefs_all.derivatives_axi;
               der_b    = abcoefs_all.derivatives_bxi;
               daux_dxi = AuxVars.derivatives_xi;
               
               a = abcoefs{1};
               b = abcoefs{2};
           
               % Start computation
               I1 = 0;
               I2 = 0;
               

               dnuI1_dnu = 0;
               dnuI2_dnu = 0;
               
               % Intermediate variable
               rho_redj = cell(7,1);
               
               for j = 1 : 7
                   rho_redj{j} = rho_red.^(j-1);
                   I1 = I1 + a{j}.*rho_redj{j}; %Eq. A17 of reference
                   I2 = I2 + b{j}.*rho_redj{j}; %Eq. A17 of reference
                   

                   dnuI1_dnu = dnuI1_dnu + a{j}.*j.*rho_redj{j}; % Eq. A29 of reference
                   dnuI2_dnu = dnuI2_dnu + b{j}.*j.*rho_redj{j}; % Eq. A30 of reference
          
               end
               
               II = cell(2, 1);
               II{1} = I1;
               II{2} = I2;
            
               % Start computation of the derivatives 
               derxi_I1 = cell(1,numC);
               derxi_I2 = cell(1,numC);
               for i = 1:numC
                   sum1 = 0;
                   sum2 = 0;
                   for j = 1:7
                       cointer = ((j-1).*daux_dxi{4,i}).*rho_red.^(j-2);
                       sum1 = sum1 + a{j}.*cointer + der_a{j,i}.*rho_redj{j}; %Eq. A42 of reference
                       sum2 = sum2 + b{j}.*cointer + der_b{j,i}.*rho_redj{j}; %Eq. A43 of reference
                   end
                   derxi_I1{i} = sum1;
                   derxi_I2{i} = sum2;
               end
               

               
               I.values                = II;
               I.derivatives_I1xi      = derxi_I1;
               I.derivatives_I2xi      = derxi_I2;
               I.derivatives_dnuI1_dnu = dnuI1_dnu;            
               I.derivatives_dnuI2_dnu = dnuI2_dnu;
        end
    end
end

%{
  Copyright 2009-2021 SINTEF ICT, Applied Mathematics.

  This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

  MRST is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  MRST is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
