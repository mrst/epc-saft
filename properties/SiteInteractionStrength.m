classdef SiteInteractionStrength < StateFunction
%% "{ Joachim Gross and Gabriele Sadowski. "Perturbed-Chain SAFT: An
%% Equation of State Based on a Perturbation Theory for Chain Molecules".
%% In: Industrial & Engineering Chemistry Research 40.4 (Feb. 2001), pages 1244–
%% 1260. url: https://doi.org/10.1021/ie0003887}"
%
% SYNOPSIS:
%   SiteInteractionStrength(model, varargin)
%
% DESCRIPTION:
%  Compute the association strength $\Delta^{A_iB_j}$ from eq 24 in Ref. and its
%  derivative with respect to the density,
%  $\frac{\partial\Delta^{A_iB_j}}{\partial\rho}$, from eq A.4 in Ref.
% PARAMETERS:
%   model    - epc-saft EoS model
%   varargin - state with a given pressure, temperature and composition
%
% RETURNS:
%   class instance
%
% EXAMPLE:
%
% SEE ALSO:
% FreeSiteMoleFraction, RadialHardSpherDistribution
%


    properties
    end
    
    methods
        function gp = SiteInteractionStrength(model, varargin)
            gp@StateFunction(model, varargin{:});
            gp = gp.dependsOn({'RadialHardSpherDistribution'});
            gp = gp.dependsOn({'T'}, 'state');
            
            gp.label = 'Site Interaction Strength';
        end

        function delta_all = evaluateOnDomain(prop, model, state)

            ghs_all = model.getProp(state, 'RadialHardSpherDistribution');
            T = model.getProp(state, 'T');
            
            ghs       = ghs_all.values;
            dghs_rho  = ghs_all.derivatives_rho;
            dghs_xi = ghs_all.derivatives_xi;

            sigmaij = model.sigmaij;
            numAss  = model.numAss;
            numC    = model.numC;
            sites   = model.sites;
            
            kAss    = model.kappaAss_ij;
            eAss    = model.epsilonAss_ij;
            
            % Start computation
            delta      = cell(numAss, numAss);
            ddelta_rho = cell(numAss, numAss);
            ddelta_xi  = cell(numAss, numAss,numC);
            
            for i = 1 : numAss
                for j = 1 : numAss
                    compi  = sites.components(i);
                    compj  = sites.components(j);
                    coef1  = sigmaij(compi, compj).^3.*ghs{compi, compj};
                    dcoef1 = sigmaij(compi, compj).^3.*dghs_rho{compi, compj};
                    coef2  = kAss(i, j).*(exp(eAss(i, j)./T) - 1);
                    
                    delta{i, j}      = coef1.*coef2;
                    ddelta_rho{i, j} = dcoef1.*coef2; 
                    % safe-guard afgaint nan derivatives in  ddelta_rho
                    if isa(ddelta_rho{i,j},'ADI')
                        ddelta_rho{i, j}.val(isnan(ddelta_rho{i,j}.val)) = 0;
                        ddelta_rho{i, j}.jac{1}.diagonal(isnan(ddelta_rho{i,j}.jac{1}.diagonal)) = 0;
                    else                        
                        ddelta_rho{i, j}(isnan(ddelta_rho{i,j})) = 0;
                    end


                end
            end
            
            for k= 1 :numC
                for i = 1 : numAss
                    for j = 1 : numAss
                        compi  = sites.components(i);
                        compj  = sites.components(j);
                        dcoef1 = sigmaij(compi, compj).^3.*dghs_xi{compi, compj,k};
                        coef2  = kAss(i, j).*(exp(eAss(i, j)./T) - 1);
                        ddelta_xi{i, j, k} = dcoef1.*coef2; % DDelta_dxi
                           % safe-guard afgaint nan derivatives in  ddelta_rho
                           if isa(ddelta_xi{i, j, k},'ADI')                        
                               ddelta_xi{i, j, k}.val(isnan(ddelta_xi{i, j, k}.val)) = 0;                        
                               ddelta_xi{i, j, k}.jac{1}.diagonal(isnan(ddelta_xi{i, j, k}.jac{1}.diagonal)) = 0; 
                           else                               
                               ddelta_xi{i, j, k}(isnan(ddelta_xi{i, j, k})) = 0;
                           end
                    end
                end
            end
            
            delta_all.values           = delta;
            delta_all.derivatives_rho  = ddelta_rho;
            delta_all.derivatives_xi   = ddelta_xi;


        end
    end
end


%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
