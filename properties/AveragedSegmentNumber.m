classdef AveragedSegmentNumber < StateFunction
%
%
% SYNOPSIS:
%   AveragedSegmentNumber(model, varargin)
%
% DESCRIPTION:
%   Compute the averaged segment number & its derivatives with
%   respect to the composition x
% PARAMETERS:
%   model - epcsaft model
%   state - state with a given composition 
%
% RETURNS:
%   Avseg - values of averaged segment number and its derivatives 
%
% EXAMPLE:
%
% SEE ALSO: HardChainHelmoltzEnergy
%


    properties
    end
    
    methods
        function gp = AveragedSegmentNumber(model, varargin)
            gp@StateFunction(model, varargin{:});
            gp = gp.dependsOn({'x'}, 'state');
            
            gp.label = 'Averaged Segment Number';
        end

        function AvsegNum = evaluateOnDomain(prop, model, state)
            
            x = model.getProp(state, 'x');
            
            segNum   = model.segNum;
            numC     = model.numC;

            if iscell(x)
                avermseg = 0;
                daverm_xi = cell(1,numC);
                for i = 1 : numC
                    avermseg       = avermseg + x{i}.*segNum(i);
                    daverm_xi{i}   = segNum(i);
                end
            else
                avermseg    = sum(bsxfun(@times, x, segNum'), 2);
                daverm_xi   = segNum;
            end
            
            AvsegNum.values            =  avermseg;
            AvsegNum.derivatives_xi    =  daverm_xi;
        end
    end
end

%{
  Copyright 2009-2021 SINTEF ICT, Applied Mathematics.

  This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

  MRST is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  MRST is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
