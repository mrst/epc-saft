classdef  PcSaftDensityPropertyFunctions < StateFunctionGrouping
%
%
% SYNOPSIS:
%   PcSaftDensityPropertys(model)
%
% DESCRIPTION:
%   adding state functions for the density model
%
% PARAMETERS:
%   model - a density model built with epc-saft model
%
% RETURNS:
%   class instance  
%
% EXAMPLE:
%
% SEE ALSO:
%   DensityModel, PCSaftModel
%


    % Grouping for PC Saft
        
    properties
        initializePcsaftState
    end

    methods
        function props = PcSaftDensityPropertyFunctions(model)
            props@StateFunctionGrouping('PCSaftDensProps'); 
            props = props.setStateFunction('initializePcsaftState', initializePcsaftState(model)); 
        end
    end
end

%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
