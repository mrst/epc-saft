classdef TotalHelmoltzEnergy < StateFunction
%% "{ Joachim Gross and Gabriele Sadowski. "Perturbed-Chain SAFT: An
%% Equation of State Based on a Perturbation Theory for Chain Molecules".
%% In: Industrial & Engineering Chemistry Research 40.4 (Feb. 2001), pages 1244–
%% 1260. url: https://doi.org/10.1021/ie0003887}"
%
% SYNOPSIS:
%   TotalHelmoltzEnergy(model, varargin)
%
% DESCRIPTION:
%       Computes the total Helmoltz freee energy by summing their
%       counterpart from all interactions
% PARAMETERS:
%   model    - a density model built with epc-saft EoS
%   varargin - state with a given pressure, composition and temperature
%
% RETURNS:
%   class instance
%
% EXAMPLE:
%
% SEE ALSO: 
%   ChemicalPotentials
%



    properties
    end
    
    methods
        function gp = TotalHelmoltzEnergy(model, varargin)
            
            gp@StateFunction(model, varargin{:});
                        
   
            gp = gp.dependsOn({'DissipativeHelmoltzEnergy'});
            gp = gp.dependsOn({'HardChainHelmoltzEnergy'});
            gp = gp.dependsOn({'Density'});
            
            if model.useAssociative
                gp = gp.dependsOn({'AssociativeHelmoltzEnergy'});            
            end
            
            if model.useIonic                 
                gp = gp.dependsOn({'IonicHelmoltzEnergy'});
            end
            
            gp = gp.dependsOn({'DipolHelmoltzEnergy'});

            gp.label = 'Total Helmoltz Energy';
            
        end

        function totA = evaluateOnDomain(prop, model, state)

            numC   = model.numC;           

            As{1} = model.getProp(state, 'HardChainHelmoltzEnergy');
            As{2} = model.getProp(state, 'DissipativeHelmoltzEnergy');

            if model.useAssociative 
                As{end + 1} = model.getProp(state, 'AssociativeHelmoltzEnergy');            
            end
            
            if model.useIonic 
                As{end + 1} = model.getProp(state, 'IonicHelmoltzEnergy');                            
            end
            if model.usePolar 
                As{end + 1}  = model.getProp(state, 'DipolHelmoltzEnergy');
            end
               
            numAs = numel(As);
            
            % initialization
            totA.values          = 0;
            totA.derivatives_rho = 0; 
            totA.derivatives_xi  = cell(numC, 1); 
            for c = 1 : numC
                totA.derivatives_xi{c} = 0;
            end
            
            for i = 1 : numAs
                A = As{i};
                totA.values = totA.values + A.values;
                totA.derivatives_rho = totA.derivatives_rho + A.derivatives_rho;
                for c = 1 : numC
                    totA.derivatives_xi{c} = totA.derivatives_xi{c} + ...
                        A.derivatives_xi{c};
                end
            end

        end
    end
end


%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
