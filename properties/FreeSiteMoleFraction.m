classdef FreeSiteMoleFraction < StateFunction
%
%
% SYNOPSIS:
%   FreeSiteMoleFraction(model, varargin)
%
% DESCRIPTION:
%   Compute mole fraction of bounded molecules used to compute the
%   Helmoltz free energies from  the Hard-chain & association interactions
% PARAMETERS:
%   model    - a density model built with epc-saft EoS
%   varargin - state with a given pressure, composition and temperature
%
% RETURNS:
%   Xa_all - values of mole fraction and its derivatives wrt the
%   composition and density
%
% EXAMPLE:
%
% SEE ALSO: 
%   HardChainHelmoltzEnergy, AssociativeHelmoltzEnergy
%


    properties
    end
    
    methods
        function gp = FreeSiteMoleFraction(model, varargin)
            gp@StateFunction(model, varargin{:});
            gp = gp.dependsOn({'Density', 'SiteInteractionStrength'});
            gp = gp.dependsOn({'x'}, 'state');            
            gp.label = 'Mole Fractions of Free Site';
        end

        function Xa_all = evaluateOnDomain(prop, model, state)
        % Compute mole fraction of bounded molecules, $X^{A_j}$ in eq 22 ref2
        % and the derivatives with respect to density, $\frac{\partial
        % X^{A_j}}{\rho_i}$ see A.3 ref2

            [rho, delta_all] = model.getProps(state, 'Density', 'SiteInteractionStrength');
            x = model.getProp(state, 'x');
            
            numAss = model.numAss;
            numC   = model.numC;
            delta       = delta_all.values;
            ddelta_rho  = delta_all.derivatives_rho;
            ddelta_xi   = delta_all.derivatives_xi;
            
            
            % Initiate sol when numAss>0
            if sum(numAss)>0 
            % Start computation
            assocmodel = AssocFracModel(x, rho, delta, ddelta_rho, ddelta_xi, model);
            [state, failure, report] = assocmodel.solveAssocFrac();
            
            derRhoAssocmodel = DerAssocFracDrhoModel(assocmodel);
            [state_derrho, failure, report] = derRhoAssocmodel.solveDerAssocFrac(state);
            
            derXiAssocmodel = DerAssocFracDxiModel(assocmodel);
            [state_derxi, failure, report] = derXiAssocmodel.solveDerAssocFrac(state);
            
            Xa  = cell(numAss, 1);
            dXadrho = cell(numAss, 1);
            dXadxi = cell(numAss*numC, 1);
            
            % We take dimension of original AD problem (if any) from eqs2
            stateC = convertToCell(state);
            % eqs2 contains the derivatives with respect to the input variables.
            eqs2 = equationsAssocFrac(stateC, assocmodel);
            [ad_sample_orig, isAD] = getSampleAD(eqs2{:});
            

            
            isAD = false;
            if isAD
                % There is AD, we want to propagate the derivative to Xa and dXadrho.
                Xavals = cell(numAss, 1);
                [Xavals{:}] = assocmodel.getProps(state, assocmodel.assocNames{:});
                stateAD = assocmodel.getStateAD(state);
                
                % eqs1 contains the derivatives with respect to the
                % `AssocModel` primary variables.
                eqs1  = equationsAssocFrac(stateAD, assocmodel, 'scalarinput', true);
                 
                eqs1  = combineEquations(eqs1{:});
                eqs2  = combineEquations(eqs2{:});
                Xajac = -eqs1.jac{1}\eqs2.jac{1};
               
                nj = ad_sample_orig.getNumVars();
                jinds = [1; cumsum(nj) + 1];
                
                assocfracs = assocmodel.getProp(stateAD, 'assocFrac');
                ad_sample  = getSampleAD(assocfracs{:});
                ni = ad_sample.getNumVars(); 
                iinds = [1; cumsum(ni) + 1];
                
                for i = 1 : numel(ni)
                    jac = cell(1, numel(nj));
                    for j = 1 : numel(nj)
                        jac{j}  = Xajac(iinds(i) : (iinds(i + 1) - 1), jinds(j) : (jinds(j + 1) - 1));
                    end
                    Xa{i} = ADI(Xavals{i}, jac);
                end
                
                % We want to propagate the derivative to dXadrho. Note that there are two
                % derivatives here: the one that is sent throught AD in the
                % case of input has AD variable and the derivative that comes
                % in the definition of dXadrho, which is meant to be the
                % derivative of Xa with respect to dens see A.3 ref2.
                dXavals = cell(numAss, 1);
                [dXavals{:}] = derRhoAssocmodel.getProps(state_derrho, derRhoAssocmodel.derAssocNames{:});
                
                stateAD = derRhoAssocmodel.getStateAD(state_derrho);
                
                derassocfracs = derRhoAssocmodel.getProp(stateAD, 'derAssocFrac');
                ad_sample  = getSampleAD(derassocfracs{:});
                ni = ad_sample.getNumVars(); 
                iinds = [1; cumsum(ni) + 1];
                
                eqs1 = equationsDerAssocFracDrho(stateAD, convertToDouble(Xa), derRhoAssocmodel, 'scalarinput', true);
                stateC = convertToCell(state_derrho);
                eqs2 = equationsDerAssocFracDrho(stateC, Xa, derRhoAssocmodel, 'scalarinput', false);
                
                eqs1 = combineEquations(eqs1{:});
                eqs2 = combineEquations(eqs2{:});
                dXajac = -eqs1.jac{1}\eqs2.jac{1};
                for i = 1 : numel(ni)
                    jac = cell(1, numel(nj));
                    for j = 1 : numel(nj)
                        jac{j} = dXajac(iinds(i) : (iinds(i + 1) - 1), jinds(j) : (jinds(j + 1) - 1));
                    end
                    dXadrho{i} = ADI(dXavals{i}, jac);
                end
                
                % We propagate the derivative to dXadxi. Same comment as above holds
                dXavals = cell(numAss*numC, 1);
                [dXavals{:}] = derXiAssocmodel.getProps(state_derxi, derXiAssocmodel.derAssocNames{:});
                
                stateAD = derXiAssocmodel.getStateAD(state_derxi);
                
                derassocfracs = derRhoAssocmodel.getProp(stateAD, 'derAssocFrac');
                ad_sample  = getSampleAD(derassocfracs{:});
                ni = ad_sample.getNumVars(); 
                iinds = [1; cumsum(ni) + 1];
                
                eqs1 = equationsDerAssocFracDxi(stateAD, convertToDouble(Xa), derXiAssocmodel, 'scalarinput', true);
                stateC = convertToCell(state_derxi);
                eqs2 = equationsDerAssocFracDxi(stateC, Xa, derXiAssocmodel, 'scalarinput', false);
                
                eqs1 = combineEquations(eqs1{:});
                eqs2 = combineEquations(eqs2{:});
                tic
                dXajac = -eqs1.jac{1}\eqs2.jac{1};
                toc
                for i = 1 : numel(ni)
                    jac = cell(1, numel(nj));
                    for j = 1 : numel(nj)
                        jac{j} = dXajac(iinds(i) : (iinds(i + 1) - 1), jinds(j) : (jinds(j + 1) - 1));
                    end
                    dXadxi{i} = ADI(dXavals{i}, jac);
                end
                
            else
                [Xa{:}]  = assocmodel.getProps(state, assocmodel.assocNames{:});
                [dXadrho{:}] = derRhoAssocmodel.getProps(state_derrho, derRhoAssocmodel.derAssocNames{:});
                [dXadxi{:}] = derXiAssocmodel.getProps(state_derxi, derXiAssocmodel.derAssocNames{:});
            end
            
            
            
            Xa_all.values = Xa;
            Xa_all.derivatives_rho = dXadrho;
            Xa_all.derivatives_xi  = dXadxi;
            
            else
                
                Xa_all.values = [];            
                Xa_all.derivatives_rho = [];            
                Xa_all.derivatives_xi  = [];
            end
        end
    end
end

%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
