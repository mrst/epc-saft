classdef ChemicalPotentials < StateFunction
%% see "{Joachim Gross and Gabriele Sadowski. "Perturbed-Chain SAFT: An
%% Equation of State Based on a Perturbation Theory for Chain Molecules".
%% In: Industrial & Engineering Chemistry Research 40.4 (Feb. 2001), pages 1244–
%% 1260. url: https://doi.org/10.1021/ie0003887}"
%
% SYNOPSIS:
%   ChemicalPotentials(model, varargin)
%
% DESCRIPTION:
%   Compute  chemical potentials from the total helmoltz energy,
%   and composition and the compressibility factor
% PARAMETERS:
%   model - density model built from epc-saft EoS
%   varagin - state with a given pressure, temperature and composition
%
% RETURNS:
%   mu_i - chemical potential for each component
%
% EXAMPLE:
%   see Equation 25 of the report
%
% SEE ALSO: 
%   TotalHelmoltzEnergy
%



    properties
    end
    
    methods
        function gp = ChemicalPotentials(model, varargin)
            gp@StateFunction(model, varargin{:});
            gp = gp.dependsOn({'TotalHelmoltzEnergy'});
            gp = gp.dependsOn({'CompressibilityFactor'});
            gp = gp.dependsOn({'x'}, 'state');
            
            gp.label = 'Chemical Potentials';
        end

        function mu = evaluateOnDomain(prop, model, state)

            numC = model.numC;
            mu = cell(numC, 1);
            
            A = model.getProp(state, 'TotalHelmoltzEnergy');
            Z = model.getProp(state, 'CompressibilityFactor');
            x = model.getProp(state, 'x');
            
            a = A.values;
            dadxi = A.derivatives_xi;
            
            for c = 1 : numC
                mu{c} = a + dadxi{c} + Z-1;
                for j = 1 : numC
                    mu{c} = mu{c} - x{j}.*dadxi{j};
                end
            end            
            
        end
    end
end

%{
  Copyright 2009-2021 SINTEF ICT, Applied Mathematics.

  This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

  MRST is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  MRST is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
