classdef DissipativeHelmoltzEnergy < StateFunction
%
%
% SYNOPSIS:
%   DissipativeHelmoltzEnergy(model, varargin)
%
% DESCRIPTION:
%   Compute the dissipative Helmoltz free energy
% PARAMETERS:
%   model    - density model built with epc-saft EoS
%   varargin - state with a given pressure, temperature and composition
%
% RETURNS:
%   Adisp - the dissipative energy and its derivatives wrt the
%   composition x and density
%
% EXAMPLE:
%
% SEE ALSO: 
%   IonicHelmoltzEnergy, TotalHelmoltzEnergy
%


    properties
    end
    
    methods
        function gp = DissipativeHelmoltzEnergy(model, varargin)
            gp@StateFunction(model, varargin{:});
            gp = gp.dependsOn({'AveragedSegmentNumber', 'Density', ...
                               'AdispCompressibilityConstants', ...
                               'AdispIntegralApproximations', ...
                               'AdispMixingVals'});
            gp = gp.dependsOn({'ReducedDensity'}, 'state');
                               
            gp.label = 'Dissipative Helmoltz Energy';
        end

        function Adisp = evaluateOnDomain(prop, model, state)
            
            AvsegNum_all   = model.getProp(state, 'AveragedSegmentNumber');
            rho_red        = model.getProp(state, 'ReducedDensity');
            rho            = model.getProp(state, 'Density');
            C_all          = model.getProp(state, 'AdispCompressibilityConstants');
            I_all          = model.getProp(state, 'AdispIntegralApproximations');
            mixingvals_all = model.getProp(state, 'AdispMixingVals');
            
            numC     = model.numC;
            segNum   = model.segNum;
            
            AvsegNum  = AvsegNum_all.values;
            
            I         = I_all.values;
            I1        = I{1};
            I2        = I{2};
            der_I1    = I_all.derivatives_I1xi;
            der_I2    = I_all.derivatives_I2xi;
            dnuI1_dnu  = I_all.derivatives_dnuI1_dnu;
            dnuI2_dnu  = I_all.derivatives_dnuI2_dnu;
            
            C      = C_all.values;
            C1     = C{1};
            C2     = C{2};
            der_C1 = C_all.derivatives_C1xi;
            
            mixingvals = mixingvals_all.values;
            mixingval1 = mixingvals{1};
            mixingval2 = mixingvals{2};
            der_prom1  = mixingvals_all.derivatives_1xi;
            der_prom2  = mixingvals_all.derivatives_2xi;
            

            % Compute Adisp
            termA1 = -2.*pi.*rho.*I1.*mixingval1;
            termA2 = -pi.*rho.*AvsegNum.*C1.*I2.*mixingval2;
            A_disp = termA1 + termA2; %Eq. A10 of reference
            
            % Compute derivative  in function of  rho
            term1 = -2.*pi.*dnuI1_dnu.*mixingval1;
            term2 = pi.*AvsegNum.*(C1.*dnuI2_dnu + C2.*rho_red.*I2).*mixingval2;
            dAdisp_rho = term1 - term2;
            
            % Compute derivative  in function of  rho
            dAdisp_xi = cell(1,numC);
            for i = 1:numC
                term1 = -2.*pi*rho.*(der_I1{i}.*mixingval1 + I1.*der_prom1{i});
                term2 = -pi.*rho.*((segNum(i).*C1.*I2 + AvsegNum.*der_C1{i}.*I2 + AvsegNum.*C1.*der_I2{i}).*mixingval2 + AvsegNum.*C1.*I2.*der_prom2{i});
                dAdisp_xi{i} = term1 + term2; %Eq. A38 of reference
            end
            Adisp.values             = A_disp;
            Adisp.derivatives_rho    = dAdisp_rho;
            Adisp.derivatives_xi     = dAdisp_xi;

        end
    end
end


%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
