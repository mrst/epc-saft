classdef DielectricConstants < StateFunction
%
%
% SYNOPSIS:
%   DielectricConstants(model, varargin)
%
% DESCRIPTION:
%   Prepare the dielectric constants for the ionic Helmoltz free energy
% PARAMETERS:
%   model    - epc-saft model
%   varargin - state with a given temperature
%
% RETURNS:
%    dielec - the values of the dielectric constants
%
% EXAMPLE:
%
% SEE ALSO: 
%    DebyeConstant, IonicHelmoltzEnergy
%


    properties
    end
    
    methods
        function gp = DielectricConstants(model, varargin)
            gp@StateFunction(model, varargin{:});
            gp = gp.dependsOn({'T'}, 'state');
            
            gp.label = 'Dielectric Constants';
            
        end

        function dielec = evaluateOnDomain(prop, model, state)
            
            T = model.getProp(state, 'T');
            Er = 281.67- 1.0912.*T + 1.6644e-3.*T.^2  - 9.7592e-7.* T.^3;
            dielec = Er.*8.85416e-12;
            
        end
    end
end

%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}