classdef PCSaftFugacityCoefficient < StateFunction
%
%
% SYNOPSIS:
%   PCSaftFugacityCoefficient(model, varargin)
%
% DESCRIPTION:
%
% PARAMETERS:
%   model    - a density model built with epc-saft EoS                         
%   varargin - state with a given pressure , temperature and composition 
%                   or pre-computed  
%              compressibility factor (Z)
%              chemical potentials (mu) 
%
% RETURNS:
%   phi - fugacity coefficient    
%
% EXAMPLE:
%   phi{i} := mu{i} - sum_i(mu{i}- log(Z));
% SEE ALSO:
%   ChemicalPotentials,   CompressibilityFactor


% Fugacity
    properties
    end
    
    methods
        function gp = PCSaftFugacityCoefficient(model, varargin)
            gp@StateFunction(model, varargin{:});
            gp = gp.dependsOn({'CompressibilityFactor'});
            gp = gp.dependsOn({'ChemicalPotentials'});
            
            gp.label = 'FugacityCoefficient';
        end

        function phi = evaluateOnDomain(prop, model, state)

            muf   = model.getProp(state, 'ChemicalPotentials');
            Z     = model.getProp(state, 'CompressibilityFactor');
            numC  = model.numC;
            if iscell(muf)                 
                phi = cell(1,numC);
                for c = 1 : numC                                                            
                    v       = muf{c} -log(Z);                                           
                    phi{c} = exp(v);
                end                
            else               
                v   = bsxfun(@minus, muf, log(Z));
                phi = exp(v);
            end

        end
    end
end

%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
