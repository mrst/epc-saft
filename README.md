

# Implementation of PC-SAFT

This module provides an implementation of PCSAFT

## Reference papers

-   *Modeling of aqueous electrolyte solutions with perturbed-chain statistical associated fluid theory* Cameretti, Luca F and Sadowski, Gabriele and Mollerup, Joergen M, Industrial \& engineering chemistry research, 2005
-   *Perturbed-chain SAFT: An equation of state based on a perturbation theory for chain molecules* Gross, Joachim and Sadowski, Gabriele,
  Industrial and engineering chemistry research, 2001



