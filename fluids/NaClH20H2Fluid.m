function [numC,Properties,kij,lij,Cons, numAss, Ass, Tsite, numions, names] = NaClH20H2Fluid()
% SYNOPSIS:
%   function [numC,Properties,Binar,Cons, numAss, Ass, Tsite, numions,
%   names] = NaClH20Co2Fluid()
%
% DESCRIPTION: NaCl-H2O-H2 fluid from
%% "{S.Parvin et al.  Continuum scale modelling of salt precipitation in the context of 
%%  CO2 storage in saline aquifers with MRST compositional.
%%  https://doi.org/10.1016/j.ijggc.2020.103075}"
%
%
% RETURNS:
%   numC       - number of components Properties - fluid properties Binar
%   - Binary interaction parameters Cons       - Universal model constants
%   numAss     - number of association sites Ass        - Data for the
%   association model Tsite      - Type of association numions    - Ionic
%   molecules names      - names of components
%
% EXAMPLE:
%
% SEE ALSO: PCSaftModel, AsphaltenicCrudeOil
% 
%% names_comp = [H20; H2; Na; Cl];     
names = {'H2O', 'CO2', 'Na', 'Cl'};
%% Properties = [MW comp m sigma epsilon(epsilon/k) KAPA EAB #site #acceptor #Donor alfa CN Tc(K) Pc(Pa) Omega]    
Properties =    [18.01	    0.868	 1.02122  2.8898 207.747	0.044394	 1813.0 	2	1	1	0	0 1165.14	3208.23   0.344
                 2.0159	    0.01	 1.112    2.906	 26.627  	0.0000000000 0.000000	2	1	1	0	0 359.661 	188.026	  -0.219
                 22.9898	0.061    1.00000  1.9	 119.8060	0.044394 	 5569.7228	0	0	0   0 	1 4631.4	5076.32   0.09
                 35.453	    0.061    1.00000  3.62	 359.6604	0.044394 	 877.8375	0	0	0	0   1 750.42	1157.401  0.09];  
        
%% Binary interactions parameters           
kij = 0.*[0   -0.01  0     0.02
         -0.01 0    0.35 0.35
         0    0.35 0    0
         0.02 0.35 0    0];
     
lij = [0   -0.01  0     0.02
         -0.01 0    0.24 0.24
         0    0.24 0    0
         0.02 0.24 0    0];        
%% Number of components
numC = size(Properties,1); 
%% Association site
numAss  = sum(Properties(:,8));
%% Associated molecules : Associated molecules must be at first rows
numAsm  = nnz(Properties(:,8)); 
%% Ionic molecules : Ionic molecules must be at last rows
numions = nnz(Properties(:,11)); 
%% Type of association: 1 is acceptor, and  2 is donor
Ass  = cell(numC,2);
TAss =cell(1,numAsm);
for i = 1:numC
    Ass{i,1}   =  Properties(i,2); %comp
    Ass{i,2}   =  [ones(1,Properties(i,9)), zeros(1,Properties(i,10))];
    TAss {1,i} =  [ones(1,Properties(i,9)), zeros(1,Properties(i,10))];
end

Tsite= cell2mat(TAss);

%% Universal constants
Cons=[
    0.91056314450	-0.30840169180	-0.09061483510	0.72409469410	-0.57554980750	0.09768831160
    0.63612814490	0.18605311590	0.45278428060	2.23827918610	0.69950955210	-0.25575749820
    2.68613478910	-2.50300472590	0.59627007280	-4.00258494850	3.89256733900	-9.15585615300
    -26.54736249100	21.41979362900	-1.72418291310	-21.00357681500	-17.21547164800	20.64207597400
    97.75920878400	-65.25588533000	-4.13021125310	26.85564136300	192.67226447000	-38.80443005200
    -159.5915408700	83.31868048100	13.77663187000	206.55133841000	-161.82646165000	93.62677407700
    91.29777408400	-33.74692293000	-8.67284703680	-355.60235612000	-165.20769346000	-29.66690558500
    ];
end

%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

MRST is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along
with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}