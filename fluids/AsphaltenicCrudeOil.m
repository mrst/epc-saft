function [numC, Properties, Binar, Cons, numAss, Ass, Tsite, numions, names] = AsphaltenicCrudeOil()

%
%
% SYNOPSIS:
%   function [numC,Properties,Binar,Cons,numAss,Ass,Tsite,numions,names] = AsphaltenicCrudeOil()
%
% DESCRIPTION:  Asphaltenic crude oil fluid from
% "{D.L. Gonzalez, F.M. Vargas, G.J. Hirasaki, W.G. Chapman, 
%  Modeling study ofCO2-induced asphaltene precipitation, Energy Fuels 22 (2008)}"
%
% RETURNS:
%   numC       - number of components
%   Properties - fluid properties
%   Binar      - Binary interaction parameters
%   Cons       - Universal model constants
%   numAss     - number of association sites 
%   Ass        - Data for the association model
%   Tsite      - Type of association
%   numions    - Ionic molecules
%   names      - names of components
%
% EXAMPLE: 
%
% SEE ALSO: PCSaftModel AsphaltenicCrudeOilV2
%

%% names_comp = [N2; CO2; C1; Light-gas; Saturates; Aro+Resin; Asphaltene];
names = {'N2', 'CO2', 'C1', 'Light-Gas', 'Saturates', 'Aro+Resin', 'Asphaltene'};

%% Properties = [MW comp m sigma epsilon(epsilon/k) KAPA EAB #site #acceptor #Donor alfa CN Tc(K) Pc(Pa) Omega]

Properties =  [28.01	0.00495	1.205	3.313	90.960	0	0	0	0	0	0	0	126.3833	3394357.819	0.039
               44.01	0.14583	2.073	2.785	169.21	0	0	0	0	0	0	0	304.3277	7386353.174	0.239
               16.04	0.27334	1.000	3.704	150.03	0	0	0	0	0	0	0	190.7833	4605490.833	0.011
               44.60	0.21917	2.055	3.613	204.96	0	0	0	0	0	0	0	507.4000	3011981.490	0.296
               207.6	0.23853	5.967	3.932	254.05	0	0	0	0	0	0	0	746.2000	1589993.017	1.000
               270.5	0.11750	6.473	3.870	332.52	0	0	0	0	0	0	0	788.7777	1169992.000	1.280
               1700.	0.00068	29.50	4.300	392.56	0	0	0	0	0	0	0	1474.000	633995.6580	2.000];              
%% Binary interactions parameters
Binar =   [0	0	0.03	0.06	0.12	0.12	0.25
           0	0	0.05	0.01	0.12	0.11	0.11
           0.03	0.05	0	0	0.03	0.029	0.029
           0.06	0.01	0	0	0.01	0.01	0.01
           0.12	0.12	0.03	0.01	0	0.007	0.007
           0.12	0.11	0.029	0.01	0.007	0	0
           0.25	0.11	0.029	0.01	0.007	0	0];

%% Number of components
numC = size(Properties,1); 
%% Association site
numAss  = sum(Properties(:,8));
%% Associated molecules : Associated molecules must be at first rows
numAsm  = nnz(Properties(:,8)); 
%% Ionic molecules : Ionic molecules must be at last rows
numions = nnz(Properties(:,11)); 
%% Type of association: 1 is acceptor, and  2 is donor
Ass  = cell(numC,2);
TAss =cell(1,numAsm);
for i = 1:numC
    Ass{i,1}   =  Properties(i,2); 
    Ass{i,2}   =  [ones(1,Properties(i,9)), zeros(1,Properties(i,10))];
    TAss {1,i} =  [ones(1,Properties(i,9)), zeros(1,Properties(i,10))];
end

Tsite= cell2mat(TAss);

%% Universal constants
Cons = [0.91056314450	-0.30840169180	-0.09061483510	0.72409469410	-0.57554980750	0.09768831160
        0.63612814490	0.18605311590	0.45278428060	2.23827918610	0.69950955210	-0.25575749820
        2.68613478910	-2.50300472590	0.59627007280	-4.00258494850	3.89256733900	-9.15585615300
        -26.54736249100	21.41979362900	-1.72418291310	-21.00357681500	-17.21547164800	20.64207597400
        97.75920878400	-65.25588533000	-4.13021125310	26.85564136300	192.67226447000	-38.80443005200
        -159.5915408700	83.31868048100	13.77663187000	206.55133841000	-161.82646165000	93.62677407700
        91.29777408400	-33.74692293000	-8.67284703680	-355.60235612000	-165.20769346000	-29.66690558500];
end

%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}

