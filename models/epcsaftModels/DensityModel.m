classdef DensityModel < PhysicalModel
%
%
% SYNOPSIS:
%   DensityModel(PcSaftModel, varargin)
%
% DESCRIPTION:
%   A density model buid with epc-saft EoS to compute the reduced density 
%   for a given pressure, composition and temperature
% PARAMETERS:
%   PcSaftModel - epc-saft model 
%   model - density model  
%   state.pressure - fluid pressure 
%   state.components - composition
%   state.T - temperature
%
% RETURNS:
%   new state with updated reduced density
%
% EXAMPLE:
%
% SEE ALSO: initializePcsaftState
%


    
    properties
        pcsaftmodel
        nonLinearMaxIterations
        pressure 
        PcSaftDensityPropertyFunctions
        ParentModel
    end

    methods
        function model = DensityModel(PcSaftModel, varargin)
            
            model = model@PhysicalModel([], varargin{:});
            model.ParentModel = PhysicalModel([], varargin{:});
            model.pcsaftmodel = PcSaftModel;
            model.nonLinearMaxIterations = 50;
            model.verbose = false;
            model.pressure = [];
            model = model.setupStateFunctionGroupings();
            model.AutoDiffBackend = DiagonalAutoDiffBackend('useMex', true, 'rowmajor', false);
        end
        
        
        function [vars, names, origin] = getPrimaryVariables(model, state)
        % Get primary variables from state
            names = {'reducedDensity'};
            
            nvars = numel(names);
            vars = cell(1, nvars);
            [vars{:}] = model.getProps(state, names{:});
            
            origin = cell(1, nvars);
            [origin{:}] = deal(class(model));
        end
         
        function [values, tolerances, names] = getConvergenceValues(model, problem, n)
            if nargin == 2
                n = inf;
            end
            Pressure = model.pressure;
            residual = value(problem.equations{1});
            values = max(abs(residual./Pressure));
            tolerances = repmat(model.nonlinearTolerance, size(values));
            names = strcat(problem.equationNames, ' (', problem.types, ')');
        end
        
        

        function [eqs, names, types, state] = getModelEquations(model, state0, state, dt, drivingForces)

            PcSaftModel = model.pcsaftmodel;
            
            x = model.getProp(state, 'x');
            x = convertToCell(x);
            
            pcstate = state;
            pcstate = PcSaftModel.setProp(pcstate, 'x', x);
            pcstate = PcSaftModel.initStateFunctionContainers(pcstate);
            P = PcSaftModel.getProp(pcstate, 'Pressure');
            
            eqs{1} = (P - model.pressure);
            
            if isa(eqs{1},'ADI')&& ~all(isfinite([eqs{1}.val' sum(eqs{1}.jac{:})]))                                         
                warning('Linear system  must have finite entries.');            
            elseif  ~isa(   eqs{1},'ADI')&& ~all(isfinite(   eqs{1}))                             
                warning('Linear system rhs must have finite entries.');
            end
            pVars = {'reducedDensity'};
            names = {'pressurediff'};
            types = {'cells'};

        end

        function [state, report] = updateState(model, state, problem, dx, drivingForces)
            [state, report] = updateState@PhysicalModel(model, state, problem, dx, drivingForces);
            state = model.capProperty(state, 'reducedDensity', 0, pi./3./sqrt(2));
        end
        
            
        function [redrho, state, report, varnames] = computeDensity(model, x, T, pressure, initialguess, varargin)
        % We compute reduced density for given pressure, composition and temperature
            
            opt = struct('initAD', false);
            opt = merge_options(opt, varargin{:});
           
            % assign pressure in model
            model.pressure = pressure;
            
            PcSaftModel = model.pcsaftmodel;
            
            solver = NonLinearSolver();
            solver.maxIterations = model.nonLinearMaxIterations;
            
            inputstate.reducedDensity = initialguess;
            inputstate.components = x;
            inputstate.T = T;
            
            inputstate0   = inputstate; % not used
            dt            = 0; % dummy timestep (not used)
            drivingForces = []; % not used;

            [state, failure, report] = solveMinistep(solver, model, inputstate, ...
                                                     inputstate0, dt, ...
                                                     drivingForces);
            
            converged = report.Converged;
            if ~converged
                nlreport = report.NonlinearReport{end};
                failure  = nlreport.Failure;
                if failure
                    msg = ['Model step resulted in failure state. Reason: ', ...
                           nlreport.FailureMsg]; %#ok<AGROW>
                    error(msg);
                end
                warning('density computation did not converge');
            end

            redrho = model.getProp(state, 'reducedDensity');
            varnames = {};
            
            x = convertToCell(x);
            
            if opt.initAD
                
                %% We propagate the derivative to redrho
                xorig = x;
                Torig = T;
                porig = pressure;
                
                [pressure, redrho, T, x{:}] = model.AutoDiffBackend.initVariablesAD(pressure, redrho, T, x{:});
                
                numC = PcSaftModel.numC;
                
                clear pcstate
                pcstate.components = x;
                pcstate.reducedDensity = redrho;
                pcstate.T = T;
                
                pcstate = PcSaftModel.initStateFunctionContainers(pcstate);
                P = PcSaftModel.getProp(pcstate, 'Pressure');
                F = P - pressure;
                
                if isa(model.AutoDiffBackend, 'DiagonalAutoDiffBackend')
                    dFdp  = F.jac{1}.diagonal(1);
                    dFdrr = F.jac{1}.diagonal(2);
                    dFdT  = F.jac{1}.diagonal(3);
                    for i = 1 : numC
                        dFdx{i} = F.jac{1}.diagonal(i + 3);
                    end
                    jac(1) = -dFdrr\dFdp;
                    for i = 1 : numC
                        jac(i + 1) = -dFdrr\dFdx{i};
                    end                
                    jac(numC + 2) = -dFdrr\dFdT;
                
                    redrho.jac{1}.diagonal = jac;
                else
                    dFdp  = F.jac{1};
                    dFdrr = F.jac{2};
                    dFdT  = F.jac{3};
                    for i = 1 : numC
                        dFdx{i} = F.jac{i + 3};
                    end
                    jac{1} = -dFdrr\dFdp;
                    for i = 1 : numC
                        jac{i + 1} = -dFdrr\dFdx{i};
                    end                
                    jac{numC + 2} = -dFdrr\dFdT;
                
                    redrho.jac = jac;
                end

                x = xorig;
                pressure = porig;
                T = Torig;
                [pressure, x{:}, T] = model.AutoDiffBackend.initVariablesAD(pressure, x{:}, T);
                
                state.pressure = pressure;
                state.components = x;
                state.T = T;
                state.redrho = redrho;
                
                cnames = PcSaftModel.cnames;
                varnames = {'Pressure', cnames{:}, 'T'};
            else
                
                state.pressure = pressure;
                state.components = x;
                state.T = T;
                state.reducedDensity = redrho;
                
            end
            
        end

        function [fn, index] = getVariableField(model, name, varargin)
            
            varfound = false;
            failed   = false;
            
            while ~varfound
                if strcmpi(name, 'reducedDensity')
                    varfound = true;
                    fn = 'reducedDensity';
                    index = 1;
                    break
                end
                
                if strcmpi(name, 'x')
                    varfound = true;
                    fn = 'components';
                    index = ':';
                    break
                end

                if strcmpi(name, 't')
                    varfound = true;
                    fn = 'T';
                    index = 1;
                    break
                end
                
                varfound = true; % to exit the loop
                failed   = true;
            end
            
            if failed
                [fn, index] = deal([]);
            end
            
        end
                     
        
        function model = setupStateFunctionGroupings(model, varargin)
            if isempty(model.PcSaftDensityPropertyFunctions)
                dispif(model.verbose, 'Setting up PcSaftDensityPropertyFunctions...');
                model.PcSaftDensityPropertyFunctions = PcSaftDensityPropertyFunctions(model); %#ok
                dispif(model.verbose, ' Ok.\n');
            end
            
        end
        
        
        
        function model = removeStateFunctionGroupings(model)
            model.PcSaftDensityPropertyFunctions = [];
        end
         




function containers = getStateFunctionGroupings(model)
            containers = getStateFunctionGroupings@PhysicalModel(model);
            extra = {model.PcSaftDensityPropertyFunctions};
            extra = extra(~cellfun(@isempty, extra));
            containers = [containers, extra];
end
        
        
        
    end
    
end


%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}


