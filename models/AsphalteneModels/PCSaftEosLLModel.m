classdef PCSaftEosLLModel < GenericEquationOfStateModel
%
%
% SYNOPSIS:
%   PCSaftEosLLModel(densitymodel)
%
% DESCRIPTION:
%    A model encompassing routines to solve the Liquid-Heavy Liquid equilibrium using
%    the Sequential Substituion Iterations (SSI) and a Density Model built 
%    with  ePC-saft EoS
% PARAMETERS:
%   densitymodel - density model built with epc-saft EoS
%   fluid -        fluid properties input
% RETURNS:
%   state - Updated state where saturations and compositions solve
%           two-phases Liquid-Heavy Liquid equilibrium equations
%
% SEE ALSO: PCSaftEosVLModel, PCSaftEosLSModel
%

    properties
        densitymodel
    end
    
    
    methods
        
        function model = PCSaftEosLLModel(densitymodel)
            fluid = densitymodel.pcsaftmodel;
            model = model@GenericEquationOfStateModel(fluid, 2);
            model.densitymodel = densitymodel;
        end                       
        
        function n = getReferenceKIndex(model)
        % the reference phase is the vapor phase
            n = 2;
        end
        
        function names = getPhaseNames(model)
            names = {'L', 'HL'};
        end


        function [state, report] = stepFunction(model, state, state0, dt, drivingForces, linsolve, nonlinsolve, iteration, varargin) %#ok
            
           % fprintf('iteration %d\n', iteration);
            
            pcsaftmodel = model.CompositionalMixture;
            
           
            T = model.getProp(state, 'T');
            P = model.getProp(state, 'pressure');
            z = model.getProp(state, 'components');
            
                        
            numC = model.getNumberOfComponents();                        
            nc   = numel(P);
            assert(all(P >= 0))
            
            
            % Basic assertions
            assert(all(sum(z, 2) > 0.999), 'Molar fractions must sum up to unity.')
            assert(iteration > 0);

            
            %% If we get here, we are using a proper flash
            if iteration == 1
                % Book-keeping
                Nph = model.parsePhaseFlag({'L', 'HL'}); % two phase gas liq
                state.Nph = Nph*ones(nc, 1);                
                % Count total iterations
                state.eos.itCount   = zeros(nc, 1);
                state.eos.itDens    = zeros(nc, 2);
                state.eos.converged = false(nc, 1);
            end
            
            z0   = model.getProp(state, 'components');
            x0   = model.getProp(state, 'X_L');
            s0   = model.getProp(state, 'X_HL');
            nl10 = model.getProp(state, 's_L');            
            nl20 = model.getProp(state, 's_HL');
            k0   = model.getProp(state, 'K_L');
            ZL0  = model.getProp(state, 'Z_L');            
            ZS0  = model.getProp(state, 'Z_HL');
            
            redrho_L0  = model.getProp(state, 'reducedDensity_L'); 
            redrho_S0  = model.getProp(state, 'reducedDensity_HL'); 
            
            if iteration == 1
                
                [phil, ~, ZL0, redrho_L0, itDens_L] = getCompressibilityAndFugacity(model, redrho_L0, P, T, x0, 'Liquid',[]); 
                [phis, ~, ZS0, redrho_S0, itDens_S] = getCompressibilityAndFugacity(model, redrho_S0, P, T, s0, 'Solid',[]);             
                
                k0 = phil./phis; 
                s0 = x0.*k0; 
                s0 = s0./sum(s0,2);
                nl10 = 0.9.*ones(nc,1);
                nl20 = 1 - nl10;
            end
            
            %% Only apply calculations for cells that have not converged yet
            active = ~state.eos.converged;
            active = find(active);
            
            z = z0(active, :);
            x = x0(active, :);
            s = s0(active, :);
            k = k0(active, :);
            nl1 = nl10(active);
            nl2 = nl20(active);
            ZL  = ZL0(active,:);
            ZS  = ZS0(active,:);
            redrho_L = redrho_L0(active,:);
            redrho_S = redrho_S0(active,:);

            nl1 = solveRachfordRiceVLE(nl1, k, z); 
            nl2 = 1 - nl1; 
        
            %% Convergence to a pure L phase
            
            lconverged = (nl2 < 10^-10);
            
            if any(active) && any(lconverged)
                update = active(lconverged);
                nl10(update) = 1; 
                nl20(update) = 0;
                s0(update, :) = zeros(nnz(lconverged), numC); 
                x0(update, :) = z(lconverged, :);
                k0(update, :) = nan(nnz(lconverged), numC);
                
                %%
                redrho_S0(update) = 0;
                ZS0(update) = 1;
                
                active = active(~lconverged);

                z = z(~lconverged, :);
                x = x(~lconverged, :);
                s = s(~lconverged, :);
                k = k(~lconverged, :);
                nl1 = nl1(~lconverged);
                nl2 = nl2(~lconverged);
                
                redrho_L  = redrho_L(~lconverged, :);           
                redrho_S  = redrho_S(~lconverged, :);
            end
            
            hlconverged = (nl2 >= 1);
            
            if any(active) && any(hlconverged)
                
                %% Convergence to a pure HL phase
                
                update = active(hlconverged);
                nl10(update) = 0; 
                nl20(update) = 1;
                x0(update, :) = zeros(nnz(hlconverged), numC);
                s0(update, :) = z(hlconverged, :); 
                k0(update, :) = zeros(nnz(hlconverged), numC);
                
                                
                redrho_L0(update) = 0;
                ZL0(update) = 1;
                
                active = active(~hlconverged);
                z = z(~hlconverged, :);
                x = x(~hlconverged, :);
                s = s(~hlconverged, :);
                k = k(~hlconverged, :);
                nl1 = nl1(~hlconverged);
                nl2 = nl2(~hlconverged);
                
                redrho_L  = redrho_L(~hlconverged, :);           
                redrho_S  = redrho_S(~hlconverged, :);
                
            end
            
            if any(active)

                
                %% Convergence to a two phase system L and HL
                
                x = z./(1 + nl2.*(k - 1)); 	
                s = k.*x; 
                
                x(x < 0) = 1.0e-06; 
                s(s < 0) = 1.0e-08; 
                
                x = bsxfun(@rdivide, x, sum(x, 2)); 
                s = bsxfun(@rdivide, s, sum(s, 2)); 
                                   
                % Tolerance for inversing density model
                tol = 10^-6;
                %% Heavy-Liquid fugacity coeficcient 
                [~, fso, ZS, redrho_S, itDens_S] = getCompressibilityAndFugacity(model, redrho_S, P(active), T(active), s, 'Solid', tol); 
                
                %% Liquid fugacity coeficcient 
                [~, flo, ZL, redrho_L, itDens_L] = getCompressibilityAndFugacity(model, redrho_L, P(active), T(active), x, 'Liquid',tol);

                
                %% Convergence criteria
                Residuals = sum((1 - (flo./fso)).^2,2); 
                hhlconverged = (Residuals < 1e-12);
                
                x0(active, :) = x;
                s0(active, :) = s;
                k0(active, :) = k.*(flo./fso); 
                ZL0(active) = ZL;
                ZS0(active) = ZS;
                  
                redrho_L0(active) = redrho_L;                
                redrho_S0(active) = redrho_S;
                
                nl10(active) = nl1;
                nl20(active) = nl2;


                convc = Residuals(~hhlconverged);
                active = active(~hhlconverged);

            end
            
            state = model.setProp(state, 'X_L' , x0);
            state = model.setProp(state, 'X_HL', s0);
            state = model.setProp(state, 'K_L' , k0);
            state = model.setProp(state, 's_L' , nl10);
            state = model.setProp(state, 's_HL', nl20);
            state = model.setProp(state, 'Z_L' , ZL0);
            state = model.setProp(state, 'Z_HL', ZS0);

            state = model.setProp(state, 'reducedDensity_L', redrho_L0); 
            state = model.setProp(state, 'reducedDensity_HL', redrho_S0);  
            state.convcriteria = convc;
            
            

            itDens = [itDens_L, itDens_S];
            state.eos.itCount(active)  = state.eos.itCount(active) + 1;
            state.eos.itDens(active,1:2) = state.eos.itDens(active,1:2) + itDens;           
            converged = true(nc, 1);
            converged(active) = false;
            state.eos.converged = converged;
                
            failure = false;
            failureMsg = '';

            %% Residuals is here the max of cell-residuals 
            %% Attention only  cell residuals are used for convergence check
            report = model.makeStepReport('Failure'           , failure              , ...
                                          'FailureMsg'        , failureMsg           , ...
                                          'Converged'         , all(converged), ...
                                          'Residuals'         , max(Residuals), ...
                                          'ResidualsConverged', converged );
            report.ActiveCells = sum(active);
            
        end

        function [x, s, nl1, nl2, Nph, Z_L, Z_S, redrho_L, redrho_S, eos] = solveFlash(model, redrho_L, redrho_S, P, T, z,  x, s)
            
            state.pressure = P;
            state.T = T;
            state.components = z;
                        
            state = model.validateState(state);
            
            % We insert (maybe) a better initialization for the density calculation
            state = model.setProp(state, 'reducedDensity_L', redrho_L);
            state = model.setProp(state, 'reducedDensity_HL', redrho_S);
               
            state = model.setProp(state, 'X_L', x);
            state = model.setProp(state, 'X_HL', s);
            
            solver = getDefaultFlashNonLinearSolver;
            
            [state, ~] = solver.solveTimestep(state, 1000.*year, model);
            
            x   = model.getProp(state, 'X_L');
            s   = model.getProp(state, 'X_HL');
            
            nl1 = model.getProp(state, 's_L');
            nl2 = model.getProp(state, 's_HL');
            
            Z_L = model.getProp(state, 'Z_L');
            Z_S = model.getProp(state, 'Z_HL'); 
            
            redrho_L = model.getProp(state, 'reducedDensity_L');
            redrho_S = model.getProp(state, 'reducedDensity_HL');
            

            Nph = state.Nph;
            eos = state.eos;

        end
        
        function [state, report] = updateAfterConvergence(model, state0, state, dt, drivingForces)
            [state, report] = updateAfterConvergence@PhysicalModel(model, state0, state, dt, drivingForces);
        end
                
    end

end


%{
Copyright 2009-2021 SINTEF ICT, Applied Mathematics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
