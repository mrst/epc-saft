classdef PCSaftEosVLModel < GenericEquationOfStateModel
%
%
% SYNOPSIS:
%   PCSaftEosVLModel(densitymodel)
%
% DESCRIPTION:
%    A model encompassing routines to solve the Vapor-Heavy Liquid equilibrium using
%    the Sequential Substituion Iterations (SSI) and a Density Model built 
%    with  ePC-saft EoS
% PARAMETERS:
%   densitymodel - density model built with epc-saft EoS
%   fluid -        fluid properties input
% RETURNS:
%   state - Updated state where saturations and compositions solve
%           two-phases Vapor-Heavy Liquid equilibrium equations
%
% SEE ALSO: PCSaftEosVLLModel, PCSaftEosLLModel
%

    properties
        densitymodel
    end
    
    methods
        
        function model = PCSaftEosVLModel(densitymodel)
            fluid = densitymodel.pcsaftmodel;
            model = model@GenericEquationOfStateModel(fluid, 2);
            model.densitymodel = densitymodel;
        end

        
        function n = getReferenceKIndex(model)
        % the reference phase is the liquid phase
            n = 2;
        end
        
        function names = getPhaseNames(model)
            names = {'V', 'L'};
        end


        function [state, report] = stepFunction(model, state, state0, dt, drivingForces, linsolve, nonlinsolve, iteration, varargin) %#ok

         %   fprintf('iteration %d\n', iteration);
            
            pcsaftmodel = model.CompositionalMixture;
            
            T = model.getProp(state, 'T');
            P = model.getProp(state, 'pressure');
            z = model.getProp(state, 'components');
            
            nc = numel(P);
            assert(all(P >= 0))
            
            % Basic assertions
            assert(all(sum(z, 2) > 0.999), 'Molar fractions must sum up to unity.')
            assert(iteration > 0);

            if iteration == 1
                % Book-keeping
                Nph = model.parsePhaseFlag({'V', 'L'}); % Two-phase Vapor-Liquid
                state.Nph = Nph*ones(nc, 1);
                
                % Count total iterations
                state.eos.itCount   = zeros(nc, 1);
                state.eos.itDens    = zeros(nc,2 );
                state.eos.converged = false(nc, 1);
            end

            numC = model.getNumberOfComponents();
            
            z0   = model.getProp(state, 'components');
            x0   = model.getProp(state, 'X_L');
            y0   = model.getProp(state, 'X_V');
            nl10 = model.getProp(state, 's_L');  

            nv0  = model.getProp(state, 's_V');
            kv0   = model.getProp(state, 'K_V');
            kl0   = model.getProp(state, 'K_L');

            
            ZL0  = model.getProp(state, 'Z_L');            
            ZV0  = model.getProp(state, 'Z_V');           
            redrho_L0  = model.getProp(state, 'reducedDensity_L'); 
            redrho_V0  = model.getProp(state, 'reducedDensity_V');            

            Nph0 = state.Nph;
            nl20 = zeros(nc,1);

            
            if iteration == 1
                nl10 = 0.9.*ones(nc,1);
                nv0  = 1.0 - nl10;  
            end
            
            %% Only apply calculations for cells that have not converged yet
            active = ~state.eos.converged;
            active = find(active);
            
            z = z0(active, :);
            x = x0(active, :);
            y = y0(active, :);
            
            kv   = kv0(active, :); 
            kl   = kl0(active, :);            

            nl1 = nl10(active);
            nv  = nv0(active);
            nl2  = nl20(active);

            
            ZL  = ZL0(active);
            ZV  = ZV0(active);
            Nph = Nph0(active);
            
             
            redrho_L  = redrho_L0(active);
            redrho_V  = redrho_V0(active);
            nl1 = solveRachfordRiceVLE(nl1, kv, z);
            
            nv = 1 - nl1; 
            
            %% Convergence to a pure L phase
            lconverged = (nv < 1e-8);
            if any(lconverged)
                update = active(lconverged);
                nl10(update) = 1; 
                nv0(update)  = 0;
                y0(update, :) = zeros(nnz(lconverged), numC); 
                x0(update, :) = z(lconverged, :);
                kv0(update, :) = nan(nnz(lconverged), numC);
                Nph0(update)  = model.parsePhaseFlag({'L'});
                
                redrho_V0(update) = 0;
                ZV0(update) = 1;
                
                active = active(~lconverged);
                z = z(~lconverged, :);
                x = x(~lconverged, :);
                y = y(~lconverged, :);
                kv = kv(~lconverged, :);
                kl = kl(~lconverged, :);
                nl1 = nl1(~lconverged);
                nv = nv(~lconverged);
                nl2 = nl2(~lconverged);

                Nph = Nph(~lconverged, :);


                redrho_L  = redrho_L(~lconverged, :);           
                redrho_V  = redrho_V(~lconverged, :);
            end
            
            
            
            %% Convergence to a pure V phase
            hlconverged = (nv >= 1);
            if any(hlconverged)                
                update = active(hlconverged);
                nl10(update) = 0; 
                nv0(update)  = 1;
                x0(update, :) = zeros(nnz(hlconverged), numC);
                y0(update, :) = z(hlconverged, :); 
                kv0(update, :) = zeros(nnz(hlconverged), numC);
                Nph0(update)  = model.parsePhaseFlag({'V'});
                 
                redrho_L0(update) = 0;
                ZL0(update) = 1;
                
                active = active(~hlconverged);
                z = z(~hlconverged, :);
                x = x(~hlconverged, :);
                y = y(~hlconverged, :);
                kv = kv(~hlconverged, :);
                kl = kl(~hlconverged, :);
                nl1 = nl1(~hlconverged);
                nv  = nv(~hlconverged);
                nl2 = nl2(~hlconverged);

                Nph = Nph(~hlconverged, :);

                

                redrho_L  = redrho_L(~hlconverged, :);           
                redrho_V  = redrho_V(~hlconverged, :);
                
            end
            
            if any(active)
                
                %% Convergence to a two phase system L and HL                                 
                % Liquid component fraction                                 
                [x, sumx] = computeLiquid( nl2, nv, kl, kv, z);                            
                % Vapor component fraction                            
                y = computeVapor( nl2, nv, kl, kv, z); 
                

                %% Vapor fugacity coefficient 
                Tol = [];
                [~, fvo, ZV, redrho_V, itDens_V] = getCompressibilityAndFugacity(model, redrho_V, P(active), T(active), y, 'Gas', Tol); 
                %% Liquid fugacity coefficient 
                [~, flo, ZL, redrho_L, itDens_L] = getCompressibilityAndFugacity(model, redrho_L, P(active), T(active), x, 'Liquid', Tol);
                %% Convergence criteria
                SinglePhase = nl1 == 1 | nl1 == 0; 
                f_r = bsxfun(@times, sumx, flo./fvo);
                f_r(SinglePhase, :) = 1;
                f_r(z == 0) = 1;
                f_r(y == 0) = 1;
                Residuals = sum((1 - f_r).^2, 2); 
                hhlconverged = (Residuals < 1e-12);
                
                % Update phase composition and phase molar
                
                x0(active, :) = x;
                y0(active, :) = y;
                kv0(active, :) = kv.*(flo./fvo); 
                ZL0(active)   = ZL;
                ZV0(active)   = ZV;                  
                redrho_L0(active) = redrho_L;                
                redrho_V0(active) = redrho_V;
                nl10(active)  = nl1;
                nv0(active)   = nv;
                Nph0(active)   = Nph;

                convc = Residuals(~hhlconverged);
                active = active(~hhlconverged);
                
            end
            
            state = model.setProp(state, 'X_L', x0);
            state = model.setProp(state, 'X_V', y0);
            state = model.setProp(state, 'K_V', kv0);
            state = model.setProp(state, 's_L', nl10);
            state = model.setProp(state, 's_V', nv0);
            state = model.setProp(state, 'Z_L', ZL0);
            state = model.setProp(state, 'Z_V', ZV0);  
            state = model.setProp(state, 'reducedDensity_L', redrho_L0); 
            state = model.setProp(state, 'reducedDensity_V', redrho_V0);  
            
            state.Nph = Nph0;
            state.convcriteria = convc;
            
            
           itDens = [1,1];
           state.eos.itCount(active)  = state.eos.itCount(active) + 1;
           state.eos.itDens(active,1:2) = state.eos.itDens(active,1:2) + itDens;
 
            
            converged = true(nc, 1);
            converged(active) = false;
            state.eos.converged = converged;            
            failure = false;
            failureMsg = '';
            
            %% Residuals is here  the max of cell-residuals 
            %% Attention only  cell residuals are used for convergence check

            report = model.makeStepReport('Failure'           , failure              , ...
                                          'FailureMsg'        , failureMsg           , ...
                                          'Converged'         , all(converged), ...
                                          'Residuals'         , max(Residuals), ...
                                          'ResidualsConverged', converged );
            report.ActiveCells = sum(active);
            
        end

        function [x, y, nl1, nv, Nph, Z_L, Z_V,  redrho_L, redrho_V, eos] = solveFlash(model, redrho_L, redrho_V,  P, T, z, k)
            
            
            state.pressure = P;
            state.T = T;
            state.components = z;
            
            state = model.validateState(state);
            
            %% Use previous density calculation
            state = model.setProp(state, 'reducedDensity_L', redrho_L);
            state = model.setProp(state, 'reducedDensity_V', redrho_V);
            
            
            state = model.setProp(state, 'K_V', k);
            
            solver = getDefaultFlashNonLinearSolver;
            
            [state, ~] = solver.solveTimestep(state, 1000*year, model);
            
            x   = model.getProp(state, 'X_L');
            y   = model.getProp(state, 'X_V');
            nl1 = model.getProp(state, 's_L');
            nv  = model.getProp(state, 's_V');
            Z_L = model.getProp(state, 'Z_L');
            Z_V = model.getProp(state, 'Z_V'); 
            redrho_L = model.getProp(state, 'reducedDensity_L');
            redrho_V = model.getProp(state, 'reducedDensity_V'); 

            Nph = state.Nph;
            eos = state.eos;
                     
            
        end
        
        function [state, report] = updateAfterConvergence(model, state0, state, dt, drivingForces)
            [state, report] = updateAfterConvergence@PhysicalModel(model, state0, state, dt, drivingForces);
        end
        
    end

end


%{
  Copyright 2009-2021 SINTEF ICT, Applied Mathematics.

  This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

  MRST is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  MRST is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
    