classdef PCSaftAsphEosModel < GenericEquationOfStateModel
%
%
% SYNOPSIS:
%   PCSaftAsphEosModel(fluid)
%
% DESCRIPTION:
%   A model encompassing routines to solve phase equilibria 
%   using epc-saft EoS
%
% PARAMETERS:
%   fluid - fluid properties input
%
% RETURNS:
%      eosmodel - fluid model with all routines to solve phase equilbria
%      for an Asphaltenic fluid
%
% SEE ALSO:
%      PCSaftSaltEosModel
    
    properties
        densitymodel
        eosVLmodel
        eosLLmodel         
        eosVLLmodel
    end
    
    methods
        
        function model = PCSaftAsphEosModel(fluid)            
            model = model@GenericEquationOfStateModel(fluid, 3); 
            densitymodel =  DensityModel(fluid);polar contributions pc saft derivative
            model.densitymodel = densitymodel;
            model.eosVLmodel = PCSaftEosVLModel(densitymodel);
            model.eosLLmodel = PCSaftEosLLModel(densitymodel);
            model.eosVLLmodel = PCSaftEosVLLModel(densitymodel);

        end
                                
        
        function n = getReferenceKIndex(model)
        %% the reference phase is the Vapor phase
            n = 2;
        end
        
        function names = getPhaseNames(model)
            names = {'V', 'L', 'HL'};
        end

        function [state, report] = stepFunction(model, state, state0, dt, drivingForces, linsolve, nonlinsolve, iteration, varargin) %#ok
            %% Compute a single step of the solution process for a given
            %% equation of state (thermodynamic flash calculation);

            % x : Liquid Molar Fraction
            % y : Gas Molar Fraction
            % s : Heavy-Liquid Molar Fraction
                
            pcsaftmodel = model.CompositionalMixture;
            
            T = model.getProp(state, 'T');
            P = model.getProp(state, 'pressure');
            nc = numel(P);
            
            x = model.getProp(state, 'X_L'); 
            y = model.getProp(state, 'X_V');
            s = model.getProp(state, 'X_HL');
            
            nl1 = model.getProp(state, 's_L'); 
            nv  = model.getProp(state, 's_V');
            nl2 = model.getProp(state, 's_HL');
            
            redrho_L  = model.getProp(state, 'reducedDensity_L'); 
            redrho_V  = model.getProp(state, 'reducedDensity_V');
            redrho_HL  = model.getProp(state, 'reducedDensity_HL');    
           
            Z_L  = model.getProp(state, 'Z_L'); 
            Z_V  = model.getProp(state, 'Z_V');
            Z_HL  = model.getProp(state, 'Z_HL');    

            z = model.getProp(state, 'components');
            numC = pcsaftmodel.getNumberOfComponents();
            
            %% Basic assertions
            assert(all(sum(z, 2) > 0.999), 'Molar fractions must sum up to unity.')
            assert(iteration > 0);
            assert(all(P >= 0));            
            Nph = zeros(nc, 1);
            Nph(:) = model.parsePhaseFlag({'L'});     % Single-Phase Liquid/ Sub-cool Liq
            eos.itCount  = zeros(nc, 1);
            eos.converged((1:nc)',1) = false;
            eos.itDens   = zeros(nc, 3);

            
            
            TC = pcsaftmodel.TC;
            PC = pcsaftmodel.PC;
            omega = pcsaftmodel.omega;    
            acf = model.CompositionalMixture.acentricFactors;            
            % Estimate equilibrium constants using Wilson equation           
            [Pr, Tr] = model.getReducedPT(P, T, false);
            k = exp(5.37.*(bsxfun(@times, 1 + acf, 1 - 1./Tr)))./Pr;
            %k = (PC./P.*exp(5.37.*(1 + omega).*(1 - TC./T))); % Wilson

            active = (1 : nc)';
            
            z0 = z;
            
            x0 = x;
            y0 = y;
            s0 = s;
            
            
            eos0 = eos;
            
            nl10 = nl1;
            nv0  = nv;
            nl20 = nl2;
            
            Nph0 = Nph;
            
            redrho_L0  = redrho_L; 
            redrho_V0  = redrho_V;
            redrho_HL0  = redrho_HL;

            Z_L0  = Z_L; 
            Z_V0  = Z_V;
            Z_HL0  = Z_HL;
                        
            %% Initial Stability Check 
            
            check2 = sum(z./k, 2);
            check1 = sum(z.*k, 2);

            if any(check2 < 1) 
                
                converged = (check2 < 1);
                update = active(converged);
                
                Nph0(update,:) = model.parsePhaseFlag({'V'});  % Single-Phase Gas/ Super-Heated Vapour
                x0(update, :) = zeros(nnz(converged), numC); 
                y0(update, :) = z(update, :); 
                nl10(update, :) = 0; 
                nv0(update, :)  = 1; 
                nl20(update, :) = 0;
                Z_L0(update, :) = 1; 
                Z_HL0(update, :) = 1;
                redrho_L0(update, :) = 0; 
                redrho_HL0(update, :) = 0;               
                
                [~, ~, Z_V_update, redrho_V_update, itDensV_update] = getCompressibilityAndFugacity(model, redrho_V(converged), ...
                                                                                           P(converged), T(converged), ...
                                                                                           y(converged, :), 'Gas',[]);
                redrho_V0(update, :)  = redrho_V_update;
                Z_V0(update, :)       = Z_V_update;
                
                % update iterations counter (flash/stability/density_solves)
                eos0.itCount(update)   = iteration;
                eos0.converged(update) = 1;
                eos0.itDens(update,1)  = itDensV_update;                                                                          
                
                % Setup unconverged variables
                setupUnconvergedVariables
                
            end


            if any(check1 < 1)
                
                converged = (check1 < 1); % (dimension de active)
                update = active(converged); % 
                
                Nph0(update,:) = model.parsePhaseFlag({'L'});  % Single-Phase Liquid/Sub-Cool Liq
                
                x0(update, :) = z(converged, :); 
                y0(update, :) = zeros(nnz(converged), numC); 
                s0(update, :) = zeros(nnz(converged), numC); 
                
                nl10(update, :) = 1; 
                nv0(update, :)  = 0; 
                nl20(update, :) = 0;
                
                Z_V0(update, :)  = 1; 
                Z_HL0(update, :)  = 1;
                
                redrho_V0(update, :)  = 0; 
                redrho_HL0(update, :)  = 0;
            
                [~, ~, Z_L_update, redrho_L_update, itDens_L] = getCompressibilityAndFugacity(model, redrho_L(converged), ...
                                                                                           P(converged), T(converged), ...
                                                                                           x(converged, :), 'Liquid',[]); ...
                                                                                       
                redrho_L0(update, :)  = redrho_L_update;
                Z_L0(update, :)  = Z_L_update;
                
                
                % update iterations counter (flash/stability/density_solves)
                eos0.itCount(update)   = iteration;
                eos0.converged(update) = 1;
                eos0.itDens(update,2)  = itDens_L; 
                
                
            end
            
            %% Two-phase Flash VLE calculation
            if any((check1 > 1) & (check2 > 1))

                converged = (check1 > 1) & (check2 > 1); % (dimension de active)
                update = active(converged); 

                fprintf('Compute V-L flash\n');
                eosVLmodel = model.eosVLmodel;                
                [x_update, y_update, nl1_update, nv_update,Nph_update, Z_L_update, Z_V_update, redrho_L_update, redrho_V_update, eos_update] = eosVLmodel.solveFlash(redrho_L(converged,:), redrho_V(converged,:), P(converged,:), T(converged,:), z(converged,:), k(converged,:));
                x(update,:) = x_update;
                y(update,:) = y_update;
                nl1(update,:) = nl1_update;
                nv(update,:) = nv_update;
                Nph(update,:) = Nph_update;
                
                Z_L(update,:) = Z_L_update;
                Z_V(update,:) = Z_V_update;
                redrho_L(update,:) = redrho_L_update;
                redrho_V(update,:) = redrho_V_update;
                
                            
                % Keep  iteration number                
                eos.converged(update,:) = eos.converged(update,:) + eos_update.converged;            
                eos.itCount(update,:)   = eos.itCount(update,:)   + eos_update.itCount;
                eos.itDens(update,1:2)  = eos.itDens(update,1:2)  + eos_update.itDens;
                
            end

            %% Check for Asphatene precipitation 
            if any(Nph > 1)
                fprintf('Compute L-HL stability\n');
                [s, Y, Nph, redrho_L, redrho_HL, it, itDens_L, itDens_HL] = teststabilityLL(model, redrho_L, redrho_HL, P, T, z, x, y, nv, nl1, Nph);                
                eos.itCount = eos.itCount + it;
                eos.itDens(:,2:3)    = eos.itDens(:,2:3)   + [itDens_L, itDens_HL];


            end
            
            % shortcut
            phaseflag = @(flagstr) model.parsePhaseFlag(flagstr);
            
            if any(Nph == phaseflag({'V', 'L'})) % Two-phases: Gas (V) and Liquid (L)

                converged = (Nph == phaseflag({'V', 'L'}));
                update = active(converged);
                

                Nph0(update,:) = model.parsePhaseFlag({'V', 'L'});  % Single-Phase Liquid/Sub-Cool Liq
                
                x0(update, :) = x(converged, :); 
                y0(update, :) = y(converged, :);  
                s0(update, :) = zeros(nnz(update), numC);  

                eos0.itCount(update,:)   = eos.itCount(converged,:);
                eos0.converged(update,:) = eos.converged(converged,:);
                eos0.itDens(update,:)    = eos.itDens(converged,:);



                
                nl10(update) = nl1(converged); 
                nv0(update) = nv(converged); 
                nl20(update) = 0; 
                
                Z_L0(update) = Z_L(converged);
                Z_V0(update) = Z_V(converged);
                Z_HL0(update) = 1;
                
                redrho_L0(update) = redrho_L(converged);
                redrho_V0(update) = redrho_V(converged);
                redrho_HL0(update) = 0;                
                
                % Setup unconverged variables
                setupUnconvergedVariables

            end
            
            if any(Nph == phaseflag({'V'}))  % Single-Gas Phase
                                
                converged = (Nph == phaseflag({'V'})); % (dimension de active)
                update = active(converged); % 
                
                
                Nph0(update) =  phaseflag({'V'});                
                
                nl10(update) = 0; 
                nl20(update) = 0; 
                nv0(update)  = 1; 
                
                y0(update, :) = z(converged, :); 
                x0(update, :) = zeros(nnz(update), numC); 
                s0(update, :) = zeros(nnz(update), numC); 
                

                Z_L0(update, :) = 1; 
                Z_HL0(update, :) = 1;
                redrho_L0(update, :) = 0;
                redrho_HL0(update, :) = 0;
                
                [~, ~, Z_V_update, redrho_V_update, itDens_V] = getCompressibilityAndFugacity(model, redrho_V(converged), ...
                                                                  P(converged), T(converged), y(converged, :), 'Gas');
                    
                redrho_V0(update, :) = redrho_V_update;
                Z_V0(update, :) = Z_V_update;
                
                
               % Keep update iterations count
                eos0.itDens(update,1)    = eos0.itDens(update,1) + itDens_V;
               
                % Setup unconverged variables
                setupUnconvergedVariables
                
            end
            
            if any(Nph == phaseflag({'L'}))  % Single-Liquid Phase
                
                converged = (Nph == phaseflag({'L'}));                
                update = active(converged);
                
                

                eos0.itCount(update,:)   = eos.itCount(converged,:);
                eos0.converged(update,:) = eos.converged(converged,:);
                
                Nph0(update) =  phaseflag({'L'});                

                x0(update, :) = z(converged, :); 
                y0(update, :) = zeros(nnz(update), numC); 
                s0(update, :) = zeros(nnz(update), numC); 
                
                nl10(update) = 1; 
                nv0(update) = 0; 
                nl20(update) = 0; 
                
                Z_V0(update) = 1; 
                Z_HL0(update) = 1;
                redrho_V0(update) = 0;
                redrho_HL0(update) = 0;
                
                [~, ~, Z_L_update, redrho_L_update, itDens_L] = getCompressibilityAndFugacity(model, redrho_L(converged), ...
                                                                  P(converged), T(converged), x(converged, :), ...
                                                                  'Liquid',[]);
                     
                redrho_L0(update) = redrho_L_update;
                Z_L0(update) = Z_L_update;
                
                
                % Keep update iterations count
                eos0.itDens(update,2)    = eos0.itDens(update,2) + itDens_L;

                % Setup unconverged variables
                setupUnconvergedVariables

            end
            
                            

            
            if any(Nph == phaseflag({'HL', 'L'})) % Two-phases: Heavy-Liquid (HL or S) and Liquid (L)              
                
                converged = (Nph == phaseflag({'HL', 'L'}));
                update = active(converged);
                
                
                eos0.itCount(update,:)   = eos.itCount(converged,:);
                eos0.converged(update,:) = eos.converged(converged,:);
                                
                fprintf('Compute L-HL flash\n');                
                eosLLmodel = model.eosLLmodel;
                
                [x_update, s_update, nl1_update, nl2_update, Nph_update, Z_L_update, Z_HL_update, redrho_L_update, redrho_HL_update, eos_update] = eosLLmodel.solveFlash(redrho_L(converged), redrho_HL(converged), P(converged), T(converged), z(converged,:), x(converged,:), s(converged,:));
                
                x0(update, :) = x_update; 
                y0(update, :) = zeros(nnz(update), numC); 
                s0(update, :) = s_update; 
                
                nl10(update, :) = nl1_update; 
                nv0(update, :) = 0; 
                nl20(update, :) = nl2_update; 
                
                Z_L0(update, :) = Z_L_update;
                Z_V0(update, :) = 1; 
                Z_HL0(update, :) = Z_HL_update;
                
                redrho_L0(update, :) = redrho_L_update;
                redrho_V0(update, :) = 0; 
                redrho_HL0(update, :) = redrho_HL_update;                
                
                Nph0(update) = Nph_update;
                
                % Keep  iteration number                
                eos0.converged(update,:) = eos_update.converged;            
                eos0.itCount(update,:)   = eos0.itCount(update,:) + eos_update.itCount;
                eos0.itDens(update,2:3)    = eos0.itDens(update,2:3)   + eos_update.itDens;

                
                % Setup unconverged variables
                setupUnconvergedVariables

                if any(nl2 == 0)
                    update = (nl2 == 0);
                    s(update, :)     = 0; 
                    Z_HL(update)      = 1; 
                    redrho_HL(update) = 0;
                end
            
            end

            if any(Nph == phaseflag({'V', 'L', 'HL'})) % Three-phases: Gas (V), Liquid (L) and Heavy-Liquid (HL)                
                
                converged = (Nph == phaseflag({'V', 'L', 'HL'}));
                fprintf('Compute V-L-HL flash\n');
                eosVLLmodel = model.eosVLLmodel;  
                
                
               [x, y, s,  nl1, nv, nl2, Nph, Z_L, Z_V, Z_HL,   redrho_L, redrho_V, redrho_HL, eos_update] = eosVLLmodel.solveFlash( redrho_L, redrho_V, redrho_HL, P, T, z, x, y, s, nv); 
                                
                
               if any(nl2 == 0)
                    update = (nl2 == 0);
                    s(update,:)        = 0; 
                    Nph(update)      = phaseflag({'V', 'L'}); 
                    Z_HL(update)      = 1; 
                    redrho_HL(update) = 0;
                end

                update = active(converged);
                
                x0(update, :) = x;
                y0(update, :) = y;
                s0(update, :) = s;
                
                nl10(update) = nl1; 
                nv0(update)  = nv; 
                nl20(update) = nl2; 
                
                Z_L0(update, :) = Z_L;
                Z_V0(update, :) = Z_V; 
                Z_HL0(update, :) = Z_HL;
                
                redrho_L0(update, :) = redrho_L;
                redrho_V0(update, :) = redrho_V; 
                redrho_HL0(update, :) = redrho_HL;     
                

                % Keep  iteration number                
                eos0.converged(update,:) = eos_update.converged;            
                eos0.itCount(update,:)   = eos0.itCount(update,:) + eos_update.itCount;
                eos0.itDens(update,:)   = eos0.itDens(update,:) + eos_update.itDens;

                
                active = active(~converged);
                
            end

            assert(isempty(active), 'problem: we missed come cases');
            
            % Update Saturation Values
            state = model.setProp(state, 's_V'  , nv0);
            state = model.setProp(state, 's_L'  , nl10);
            state = model.setProp(state, 's_HL' , nl20);
            % Update Molar Fraction Values            
            state = model.setProp(state, 'X_V' , y0);
            state = model.setProp(state, 'X_L' , x0);
            state = model.setProp(state, 'X_HL', s0);
            % Update Compressibility Factor Values
            state = model.setProp(state, 'Z_V' , Z_V0);
            state = model.setProp(state, 'Z_L' , Z_L0);
            state = model.setProp(state, 'Z_HL', Z_HL0);        
            % Update Reduced Density
            state = model.setProp(state, 'reducedDensity_V' , redrho_V0);
            state = model.setProp(state, 'reducedDensity_L' , redrho_L0);
            state = model.setProp(state, 'reducedDensity_HL', redrho_HL0);

            state.eos = eos0;
            failure   = false;
            converged = true;
            % values_converged = values <= model.nonlinearTolerance;
            if model.verbose
                numC = model.getNumberOfComponents;
                compnames = arrayfun(@(x) sprintf('comp %d', x), (1 : numC), 'uniformoutput', false);
                printConvergenceReport(compnames, values, values_converged, iteration);
            end
            report = model.makeStepReport('Failure'  , failure, ...
                                          'Converged', converged);
        end

        
        function [state, report] = updateAfterConvergence(model, state0, state, dt, drivingForces)
            [state, report] = updateAfterConvergence@PhysicalModel(model, state0, state, dt, drivingForces);
        end
        

        function y = computeVapor(model, L, K, z)
            if isstruct(L)
                y = model.computeVapor(L.L, L.K, L.components);
                return
            end
            if iscell(z)
                y = z;
                sv = 0;
                for i = 1:numel(z)
                    bad = ~isfinite(K{i});
                    y{i} = K{i}.*z{i}./(L + (1-L).*K{i});
                    y{i}(bad) = 0;
                    sv = sv + value(y{i});
                end
                y = cellfun(@(k, zi) k.*zi./(L + (1-L).*k), K, z, 'UniformOutput', false);
                y = cellfun(@(x) x./sv, y, 'UniformOutput', false);
                assert(all(cellfun(@(x) all(isfinite(value(x))), y)));
            else
                y = K.*z./bsxfun(@plus, L, bsxfun(@times, 1-L, K));
                y(~isfinite(K)) = z(~isfinite(K));
                y = bsxfun(@rdivide, y, sum(y, 2));
                assert(all(isfinite(y(:))));
            end
        end
        
        function [x, sv] = computeLiquid(model, L, K, z)
            if isstruct(L)
                x = model.computeLiquid(L.L, L.K, L.components);
                return
            end
            if iscell(z)
                x = z;
                sv = 0;
                for i = 1:numel(z)
                    bad = ~isfinite(K{i});
                    x{i} = z{i}./(L + (1-L).*K{i});
                    x{i}(bad) = 0;
                    sv = sv + value(x{i});
                end
                x = cellfun(@(x) x./sv, x, 'UniformOutput', false);
                assert(all(cellfun(@(x) all(isfinite(value(x))), x)));
            else
                tmp = bsxfun(@times, 1-L, K);
                x = z./bsxfun(@plus, L, tmp);
                x(~isfinite(K)) = 0;
                sv = sum(x, 2);
                x = bsxfun(@rdivide, x, sv);
                assert(all(isfinite(x(:))));
            end
        end
        
        
        function [s, sums] = computeHeavyLiquid(model, HL, V, Kl, Kv, z)
        % return vapor (y) and salt (s) compositions. 
            if isstruct(HL)
                [s, sums] = model.computeHeavyLiquid(HL.HL, HL.V, HL.Kl, HL.Kv, HL.components); 
                return
            end
            
            if iscell(z)
                s = z; 
                sums = 0; 
                
                for i = 1:numel(z)
                    bad =~isfinite(Kv{i}) & ~isfinite(Kl{i}); 
                    s{i} = Kl{i}.*z{i}./(1 + V.*(Kv{i} - 1) + HL.*(Kl{i} - 1)); 
                    s{i}(bad) = 0; 
                    sums = sums + double(s{i}); 
                end
                s = cellfun(@(kv, kl, zi) kl.*zi./(1 + HL.*(kl - 1) + V.*(kv - 1)), Kv, Kl, z, 'UniformOutput', false); 
                s = cellfun(@(x) x./sums, s, 'UniformOutput', false); 
                assert(all(cellfun(@(x) all(isfinite(double(x))), y))); 
                
            else
                s = Kl.*z./(1 + (bsxfun(@times, V, (Kv - 1))) + (bsxfun(@times, (Kl - 1), HL))); 
                s(~isfinite(Kl)) = z(~isfinite(Kl)); 
                s = bsxfun(@rdivide, s, sum(s, 2)); 
                sums = sum(s, 2); 
                assert(all(isfinite(s(:)))); 
            end
        end

        function [sL, sV] = computeSaturations(model, rhoL, rhoV, x, y, L, Z_L, Z_V)
            sL = L.*Z_L./(L.*Z_L + (1-L).*Z_V);
            sV = (1-L).*Z_V./(L.*Z_L + (1-L).*Z_V);
        end

        
        function L = solveRachfordRice(model, L, K, z)
            error('not used here, for the moment')
            L = solveRachfordRiceVLE(L, K, z, 'min_z', model.minimumComposition);
        end
        
    end

end



%{
Copyright 2009-2021 SINTEF ICT, Applied Mathematics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
