% A helping script to update a given state 
z(converged,:) = z0(update,:) ;            
x(converged,:) = x0(update,:);
y(converged,:) = y0(update,:);
s(converged,:) = s0(update,:);            
           
nl1(converged) = nl10(update);
nv(converged)  = nv0(update);
nl2(converged) = nl20(update);
                      
Nph(converged) = Nph0(update);

redrho_L(converged)  = redrho_L0(update); 
redrho_V(converged)  = redrho_V0(update);
redrho_HL(converged)  = redrho_HL0(update);

Z_L(converged) = Z_L0(update); 
Z_V(converged) = Z_V0(update);            
Z_HL(converged) = Z_HL0(update);                       
K_L(converged,:) = K_L0(update,:);            
K_V(converged,:) = K_V0(update,:);

%{
Copyright 2009-2021 SINTEF ICT, Applied Mathematics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}