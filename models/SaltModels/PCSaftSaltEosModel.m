classdef PCSaftSaltEosModel < GenericEquationOfStateModel
%
%
% SYNOPSIS:
%   PCSaftSaltEosModel(fluid)
%
% DESCRIPTION:
%   A model encompassing routines to solve phase equilibria 
%   using epc-saft EoS
% PARAMETERS:
%   fluid - fluid properties input
%
% RETURNS:
%      eosmodel - Updated model with all routines to solve phase equilbria
%      for a Salty fluid
% EXAMPLE:
%
% SEE ALSO: PCSaftAsphEosModel
%
    
    properties
        %% The model requires
        % (1)  Density Model 
        densitymodel 
        % (2) Vapor-Liquid EOS  Model 
        eosVLmodel
        % (3) Liquid-Solid EOS  Model 
        eosLSmodel         
        % (4) Vapor-Liquid-Solid EOS  Model 
        eosVLSmodel
    end
    
    methods
        
        function model = PCSaftSaltEosModel(fluid)            
            model = model@GenericEquationOfStateModel(fluid, 3); 
            densitymodel =  DensityModel(fluid);
            model.densitymodel = densitymodel;
            model.eosVLmodel = PCSaftEosVLModel(densitymodel);
            model.eosLSmodel = PCSaftEosLSModel(densitymodel);
            model.eosVLSmodel = PCSaftEosVLSModel(densitymodel);

        end
                                
        
        function n = getReferenceKIndex(model)
        % the reference phase is the vapor phase
            n = 2;
        end
        
        function names = getPhaseNames(model)
            % Here, V = Vapor and  L = Liquid  while HL=  Solid (pure)
            names = {'V', 'L', 'HL'};
        end

        function [state, report] = stepFunction(model, state, state0, dt, drivingForces, linsolve, nonlinsolve, iteration, varargin) %#ok
            %% Compute a single step of the solution process for a given
            %% equation of state (thermodynamic flash calculation);
            %% The pure solid phase is only present in th Salt 

            % X_L  or x: Liquid Molar Fraction
            % X_V  or y: Gas Molar Fraction
            % X_HL or s: Solid Molar Fraction
                
            
            pcsaftmodel = model.CompositionalMixture;
            
            T = model.getProp(state, 'T');
            P = model.getProp(state, 'pressure');
            nc = numel(P);
            
            x = model.getProp(state, 'X_L'); 
            y = model.getProp(state, 'X_V');
            s = model.getProp(state, 'X_HL');
            
            nl1 = model.getProp(state, 's_L'); 
            nv  = model.getProp(state, 's_V');
            nl2 = model.getProp(state, 's_HL');
            
            redrho_L  = model.getProp(state, 'reducedDensity_L'); 
            redrho_V  = model.getProp(state, 'reducedDensity_V');
            redrho_HL  = model.getProp(state, 'reducedDensity_HL');    
           
            Z_L  = model.getProp(state, 'Z_L'); 
            Z_V  = model.getProp(state, 'Z_V');
            Z_HL  = model.getProp(state, 'Z_HL'); 
            
            
            K_V  = model.getProp(state, 'K_V');
            K_L  = model.getProp(state, 'K_L'); 

            z = model.getProp(state, 'components');
            numC = pcsaftmodel.getNumberOfComponents();
            
            %% Basic assertions
            assert(all(sum(z, 2) > 0.999), 'Molar fractions must sum up to unity.')
            % shortcut
            phaseflag = @(flagstr) model.parsePhaseFlag(flagstr);
            
            
            % safe-guard sgainst iteration number
            assert(iteration > 0);
            % safe-guard sgainst iteration number
            assert(all(P >= 0));            
            %% Initiate phase numbers: we start with assuming single-Phase Liquid/Sub-cool Liquid in all cells
            Nph = zeros(nc, 1);            
            Nph(:) = phaseflag({'L'}); 
                        
            % eos output
            eos.itCount  = zeros(nc, 1);
            eos.converged((1:nc)',1) = false;            

            %% Estimate equilibrium constants using Wilson equation           
            acf = model.CompositionalMixture.acentricFactors;            
            [Pr, Tr] = model.getReducedPT(P, T, false);
            k = exp(5.37.*(bsxfun(@times, 1 + acf, 1 - 1./Tr)))./Pr;

            
            %% We just start so all cells still active
            active = (1 : nc)';
                        
            % Reductio process
            z0 = z;            
            x0 = x;
            y0 = y;
            s0 = s;
                       
            
            nl10 = nl1;
            nv0  = nv;
            nl20 = nl2;
            
            Nph0 = Nph;
            
            redrho_L0   = redrho_L; 
            redrho_V0   = redrho_V;
            redrho_HL0  = redrho_HL;

            Z_L0   = Z_L; 
            Z_V0   = Z_V;
            Z_HL0  = Z_HL;  
            
            K_L0  = K_L; 
            K_V0  = K_V;
                        
            %% We begin by checking stability             
            check2 = sum(z./k, 2);
            check1 = sum(z.*k, 2);
            

            %% Check Single-Vapor-Phase
            if any(check2 < 1) 
                
                converged = (check2 < 1);
                update = active(converged);
                
                Nph0(update,:) = model.parsePhaseFlag({'V'});  % Single-Phase Gas/ Super-Heated Vapour
                x0(update, :) = zeros(nnz(converged), numC); 
                y0(update, :) = z(update, :); 
                s0(update, :) = zeros(nnz(converged), numC); 
                nl10(update, :) = 0; 
                nv0(update, :)  = 1; 
                nl20(update, :) = 0;
                Z_L0(update, :) = 1; 
                Z_HL0(update, :) = 1;
                redrho_L0(update, :) = 0; 
                redrho_HL0(update, :) = 0;
                                
                [~, ~, Z_V_update, redrho_V_update, ~] = getCompressibilityAndFugacity(model, redrho_V(converged), ...
                                                                                           P(converged), T(converged), ...
                                                                                           y(converged, :), 'Gas',[]);
                redrho_V0(update, :)  = redrho_V_update;
                Z_V0(update, :)       = Z_V_update;
                
                                
                %% Since we are NOT yet sure, we keep converged variables
                    fieldnames = {'z', 'x', 'y', 's', 'nl1', 'nv', 'nl2', 'Nph', 'redrho_L', 'redrho_V', 'redrho_HL', 'Z_L', 'Z_V', 'Z_HL', 'K_L', 'K_V'};
                    vars = collectVariables(model, [], fieldnames, z, x, y, s, nl1, nv, nl2, Nph, redrho_L, redrho_V, redrho_HL, Z_L, Z_V, Z_HL, K_L, K_V);
                    fieldnames0 = cellfun(@(str) (sprintf('%s0', str)), fieldnames, 'uniformoutput', false);
                    vars = collectVariables(model, vars, fieldnames0, z0, x0, y0, s0, nl10, nv0, nl20, Nph0, redrho_L0, redrho_V0, redrho_HL0, Z_L0, Z_V0, Z_HL0, K_L0, K_V0);
                    [z, x, y, s, nl1, nv, nl2, Nph, redrho_L, redrho_V, redrho_HL, Z_L, Z_V, Z_HL, K_L, K_V] = updateConvergedVariables(model, vars, converged, update);
            end


            %% Check Single-Liquid-Phase
            if any(check1 < 1)
                
                converged = (check1 < 1); 
                update = active(converged); 
                
                Nph0(update,:) = model.parsePhaseFlag({'L'});  
                
                x0(update, :) = z(converged, :); 
                y0(update, :) = zeros(nnz(converged), numC); 
                s0(update, :) = zeros(nnz(converged), numC); 
                
                nl10(update, :) = 1; 
                nv0(update, :)  = 0; 
                nl20(update, :) = 0;
                
                Z_V0(update, :)  = 1; 
                Z_HL0(update, :)  = 1;
                
                redrho_V0(update, :)  = 0; 
                redrho_HL0(update, :)  = 0;
            
                [~, ~, Z_L_update, redrho_L_update, ~] = getCompressibilityAndFugacity(model, redrho_L(converged), ...
                                                                                           P(converged), T(converged), ...
                                                                                           x(converged, :), 'Liquid',[]); ...
                                                                                       
                redrho_L0(update, :)  = redrho_L_update;
                Z_L0(update, :)  = Z_L_update;
                                
                %% We are still NOT sure, so we keep converged variables
                    fieldnames = {'z', 'x', 'y', 's', 'nl1', 'nv', 'nl2', 'Nph', 'redrho_L', 'redrho_V', 'redrho_HL', 'Z_L', 'Z_V', 'Z_HL', 'K_L', 'K_V'};
                    vars = collectVariables(model, [], fieldnames, z, x, y, s, nl1, nv, nl2, Nph, redrho_L, redrho_V, redrho_HL, Z_L, Z_V, Z_HL, K_L, K_V);
                    fieldnames0 = cellfun(@(str) (sprintf('%s0', str)), fieldnames, 'uniformoutput', false);
                    vars = collectVariables(model, vars, fieldnames0, z0, x0, y0, s0, nl10, nv0, nl20, Nph0, redrho_L0, redrho_V0, redrho_HL0, Z_L0, Z_V0, Z_HL0, K_L0, K_V0);
                    [z, x, y, s, nl1, nv, nl2, Nph, redrho_L, redrho_V, redrho_HL, Z_L, Z_V, Z_HL, K_L, K_V] = updateConvergedVariables(model, vars, converged, update);                                                                                                 
            end
                   
            %% Continue with Vapor Stability ONLY for  Solid-free cells
            if any( nl2 == 0)
                converged = (nl2 == 0);
                update = active(converged);
                [y0(update, :), Nph0(update), redrho_L0(update), redrho_V0(update)] = model.initiateVaporStability(P(update), T(update), z(update, :), redrho_L(update), redrho_V(update)); 
                %% We are still NOT sure, so we keep converged variables
                    fieldnames = {'z', 'x', 'y', 's', 'nl1', 'nv', 'nl2', 'Nph', 'redrho_L', 'redrho_V', 'redrho_HL', 'Z_L', 'Z_V', 'Z_HL', 'K_L', 'K_V'};
                    vars = collectVariables(model, [], fieldnames, z, x, y, s, nl1, nv, nl2, Nph, redrho_L, redrho_V, redrho_HL, Z_L, Z_V, Z_HL, K_L, K_V);
                    fieldnames0 = cellfun(@(str) (sprintf('%s0', str)), fieldnames, 'uniformoutput', false);
                    vars = collectVariables(model, vars, fieldnames0, z0, x0, y0, s0, nl10, nv0, nl20, Nph0, redrho_L0, redrho_V0, redrho_HL0, Z_L0, Z_V0, Z_HL0, K_L0, K_V0);
                    [z, x, y, s, nl1, nv, nl2, Nph, redrho_L, redrho_V, redrho_HL, Z_L, Z_V, Z_HL, K_L, K_V] = updateConvergedVariables(model, vars, converged, update);                                                

            end
            
            
            %% We are now sure for Single-Gas Phase
            if any(Nph == phaseflag({'V'}))                                  
                converged = (Nph == phaseflag({'V'}));
                update = active(converged); % 
                                
                Nph0(update) =  phaseflag({'V'});                
                
                nl10(update) = 0; 
                nl20(update) = 0; 
                nv0(update)  = 1; 
                
                y0(update, :) = z(converged, :); %
                x0(update, :) = zeros(nnz(update), numC); 
                s0(update, :) = zeros(nnz(update), numC); 
                

                Z_L0(update, :) = 1; 
                Z_HL0(update, :) = 1;
                redrho_L0(update, :) = 0;
                redrho_HL0(update, :) = 0;
                
                [~, ~, Z_V_update, redrho_V_update, ~] = getCompressibilityAndFugacity(model, redrho_V(converged), ...
                                                                  P(converged), T(converged), y(converged, :), 'Gas');
                    
                redrho_V0(update, :) = redrho_V_update;
                Z_V0(update, :) = Z_V_update;
               
                %% Here, we are SURE, so we can  setup reduction process 
                %% and keep only non-converged variables
                [P, T, Nph, z, x, y, s, nl1, nv , nl2, redrho_L, redrho_V, redrho_HL, Z_L, Z_V, Z_HL, active] = ...
                updateUnConvergedVariables(model, P, T, Nph, z, x, y, s, nl1, nv , nl2, redrho_L, redrho_V, redrho_HL, ...
                                            Z_L, Z_V, Z_HL, active, converged);     
                eos.converged(update,:) = true;
            end
            
            
            
            %% Attention, we are NOT sure for Single-Liquid Phase
            if any(Nph == phaseflag({'L'})) 
                            
                converged = (Nph == phaseflag({'L'}));                
                update    = active(converged);
                
                Nph0(update) =  phaseflag({'L'});                

                x0(update, :) = z(converged, :); 
                y0(update, :) = zeros(nnz(update), numC); 
                s0(update, :) = zeros(nnz(update), numC); 
                
                nl10(update) = 1; 
                nv0(update)  = 0; 
                nl20(update) = 0; 
                
                Z_V0(update) = 1; 
                Z_HL0(update) = 1;
                redrho_V0(update) = 0;
                redrho_HL0(update) = 0;
                
                [~, ~, Z_L_update, redrho_L_update, ~] = getCompressibilityAndFugacity(model, redrho_L(converged), ...
                                                                  P(converged), T(converged), x(converged, :), ...
                                                                  'Liquid',[]);
                     
                redrho_L0(update) = redrho_L_update;
                Z_L0(update) = Z_L_update;
                
                %% We are still NOT sure, so we keep converged variables
                    fieldnames = {'z', 'x', 'y', 's', 'nl1', 'nv', 'nl2', 'Nph', 'redrho_L', 'redrho_V', 'redrho_HL', 'Z_L', 'Z_V', 'Z_HL', 'K_L', 'K_V'};
                    vars = collectVariables(model, [], fieldnames, z, x, y, s, nl1, nv, nl2, Nph, redrho_L, redrho_V, redrho_HL, Z_L, Z_V, Z_HL, K_L, K_V);
                    fieldnames0 = cellfun(@(str) (sprintf('%s0', str)), fieldnames, 'uniformoutput', false);
                    vars = collectVariables(model, vars, fieldnames0, z0, x0, y0, s0, nl10, nv0, nl20, Nph0, redrho_L0, redrho_V0, redrho_HL0, Z_L0, Z_V0, Z_HL0, K_L0, K_V0);
                    [z, x, y, s, nl1, nv, nl2, Nph, redrho_L, redrho_V, redrho_HL, Z_L, Z_V, Z_HL, K_L, K_V] = updateConvergedVariables(model, vars, converged, update);                
            end
            
            
            
            
            
            %% Two-phase Vapor-Liquid Flash VLE calculation 
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %% ATTENTION: we are still not sure on the phases!%%
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %% We need to check Salt existence later
            if any(Nph == phaseflag({'V', 'L'}))                
                converged = (Nph == phaseflag({'V', 'L'}));
                update = active(converged);                
                fprintf('Compute V-L flash\n');
                eosVLmodel = model.eosVLmodel;                
                
                %% We initiate K_V
                [~, fvo, ~, redrho_V(converged,:), ~]  = getCompressibilityAndFugacity(model, redrho_V(converged,:), P(converged), T(converged), y(converged,:), 'Gas',10^-6);                         
                [~, flo, ~, redrho_L(converged,:), ~]  = getCompressibilityAndFugacity(model, nan(numel(redrho_L),1), P(converged), T(converged), x(converged,:),    'Liquid',10^-6);             
                K_V(converged, :) = (y(converged, :)./x(converged, :)).*(flo./fvo);             
                K_V(y(converged,:)==0) = 0;
                
                [x_update, y_update, nl1_update, nv_update,Nph_update, Z_L_update, Z_V_update, redrho_L_update, redrho_V_update, eos_update] = eosVLmodel.solveFlash(redrho_L(converged,:), redrho_V(converged,:), P(converged,:), T(converged,:), z(converged,:), K_V(converged,:));
                x0(update, :) = x_update; 
                y0(update, :) = y_update; 
                s0(update, :) = zeros(nnz(update), numC); 
                
                nl10(update, :) = nl1_update; 
                nv0(update, :)  = nv_update; 
                nl20(update, :) = 0; 
                
                Z_L0(update, :)  = Z_L_update;
                Z_V0(update, :)  = Z_V_update; 
                Z_HL0(update, :) = 1;
                
                redrho_L0(update, :)  = redrho_L_update;
                redrho_V0(update, :)  = redrho_V_update; 
                redrho_HL0(update, :) = 0;                
                
                Nph0(update) = Nph_update;
                
                %% We are still NOT sure and we  need to check  NaCl existence
                    fieldnames = {'z', 'x', 'y', 's', 'nl1', 'nv', 'nl2', 'Nph', 'redrho_L', 'redrho_V', 'redrho_HL', 'Z_L', 'Z_V', 'Z_HL', 'K_L', 'K_V'};
                    vars = collectVariables(model, [], fieldnames, z, x, y, s, nl1, nv, nl2, Nph, redrho_L, redrho_V, redrho_HL, Z_L, Z_V, Z_HL, K_L, K_V);
                    fieldnames0 = cellfun(@(str) (sprintf('%s0', str)), fieldnames, 'uniformoutput', false);
                    vars = collectVariables(model, vars, fieldnames0, z0, x0, y0, s0, nl10, nv0, nl20, Nph0, redrho_L0, redrho_V0, redrho_HL0, Z_L0, Z_V0, Z_HL0, K_L0, K_V0);
                    [z, x, y, s, nl1, nv, nl2, Nph, redrho_L, redrho_V, redrho_HL, Z_L, Z_V, Z_HL, K_L, K_V] = updateConvergedVariables(model, vars, converged, update);

            end

            %% We  check for Solid Salt existence    
            fprintf('Check for Solid Salt\n');
            if any(Nph == phaseflag({'V', 'L'})|Nph == phaseflag({'L'}))                
                converged = Nph == phaseflag({'V', 'L'})|Nph == phaseflag({'L'});
                update = active(converged);  
                %% We compute the Liquid (fS.AqueousSalt) and solid ( fS.SolidSalt) salt fugacities and check check solid Salt Existence
                %% The criteria for salt precence is that fS.SolidSalt >  fS.AqueousSalt 
                SaltInfo =  calculatePcsaftSaltFugacity(model, P(update), x(update,:),T(update));                            
                issalt(update,:)  = SaltInfo.isSalt;               
                %% We update Solid composition for cells containing solid Salt
                if any(issalt)
                    [K_L0(issalt,:), s0(issalt,:), Nph0(issalt), Z_HL0(issalt)] = model.updateSolidSaltComposition(P(issalt),T(issalt), x(issalt,:),s(issalt,:) ,Nph(issalt));
                    %% We are still NOT sure
                    fieldnames = {'z', 'x', 'y', 's', 'nl1', 'nv', 'nl2', 'Nph', 'redrho_L', 'redrho_V', 'redrho_HL', 'Z_L', 'Z_V', 'Z_HL', 'K_L', 'K_V'};
                    vars = collectVariables(model, [], fieldnames, z, x, y, s, nl1, nv, nl2, Nph, redrho_L, redrho_V, redrho_HL, Z_L, Z_V, Z_HL, K_L, K_V);
                    fieldnames0 = cellfun(@(str) (sprintf('%s0', str)), fieldnames, 'uniformoutput', false);
                    vars = collectVariables(model, vars, fieldnames0, z0, x0, y0, s0, nl10, nv0, nl20, Nph0, redrho_L0, redrho_V0, redrho_HL0, Z_L0, Z_V0, Z_HL0, K_L0, K_V0);
                    [z, x, y, s, nl1, nv, nl2, Nph, redrho_L, redrho_V, redrho_HL, Z_L, Z_V, Z_HL, K_L, K_V] = updateConvergedVariables(model, vars, converged, update);
                    
                    %% We need also  solid/Liquid fugacities for the Salt        
                    f_S  = SaltInfo.SolidSalt(issalt);                        
                    fs_W = SaltInfo.AqueousSalt(issalt);                    
                end
            end
            
            
            
            
            if any(Nph == phaseflag({'V', 'L'})) % Two-phases: Gas (V) and Liquid (L)

                converged = (Nph == phaseflag({'V', 'L'}));
                update = active(converged);
                

                Nph0(update,:) = model.parsePhaseFlag({'V', 'L'});  % Single-Phase Liquid/Sub-Cool Liq
                
                x0(update, :) = x(converged, :); 
                y0(update, :) = y(converged, :);  
                s0(update, :) = zeros(nnz(update), numC);  
                
                nl10(update) = nl1(converged); 
                nv0(update) = nv(converged); 
                nl20(update) = 0; 
                
                Z_L0(update) = Z_L(converged);
                Z_V0(update) = Z_V(converged);
                Z_HL0(update) = 1;
                
                redrho_L0(update) = redrho_L(converged);
                redrho_V0(update) = redrho_V(converged);
                redrho_HL0(update) = 0;                
                
                % Setup unconverged variables
                [P, T, Nph, z, x, y, s, nl1, nv , nl2, redrho_L, redrho_V, redrho_HL, Z_L, Z_V, Z_HL, active] = ...
                updateUnConvergedVariables(model, P, T, Nph, z, x, y, s, nl1, nv , nl2, redrho_L, redrho_V, redrho_HL, ...
                                            Z_L, Z_V, Z_HL, active, converged)

            end
            
            
            
            %% Here we are SURE that this is a Single-Liquid Phase:  we can proceed with LL stability
            if any(Nph == phaseflag({'L'}))  
                            
                converged = (Nph == phaseflag({'L'}));                
                update = active(converged);
                
                Nph0(update) =  phaseflag({'L'});                

                x0(update, :) = z(converged, :); 
                y0(update, :) = zeros(nnz(update), numC); 
                s0(update, :) = zeros(nnz(update), numC); 
                
                nl10(update) = 1; 
                nv0(update) = 0; 
                nl20(update) = 0; 
                
                Z_V0(update) = 1; 
                Z_HL0(update) = 1;
                redrho_V0(update) = 0;
                redrho_HL0(update) = 0;
                
                [~, ~, Z_L_update, redrho_L_update, ~] = getCompressibilityAndFugacity(model, redrho_L(converged), ...
                                                                  P(converged), T(converged), x(converged, :), ...
                                                                  'Liquid',[]);
                     
                redrho_L0(update) = redrho_L_update;
                Z_L0(update) = Z_L_update;                                 
                %% We can  setup unconverged variables                
                [P, T, Nph, z, x, y, s, nl1, nv , nl2, redrho_L, redrho_V, redrho_HL, Z_L, Z_V, Z_HL, active] = ...
                updateUnConvergedVariables(model, P, T, Nph, z, x, y, s, nl1, nv , nl2, redrho_L, redrho_V, redrho_HL, ...
                                            Z_L, Z_V, Z_HL, active, converged); 
            end
            
            
            
            if any(Nph == phaseflag({'V'}))  % Single-Gas Phase
                                
                converged = (Nph == phaseflag({'V'})); % (dimension de active)
                update = active(converged); % 
                
                
                Nph0(update) =  phaseflag({'V'});                
                
                nl10(update) = 0; 
                nl20(update) = 0; 
                nv0(update)  = 1; 
                
                y0(update, :) = z(converged, :); %
                x0(update, :) = zeros(nnz(update), numC); 
                s0(update, :) = zeros(nnz(update), numC); 
                

                Z_L0(update, :) = 1; 
                Z_HL0(update, :) = 1;
                redrho_L0(update, :) = 0;
                redrho_HL0(update, :) = 0;
                
                [~, ~, Z_V_update, redrho_V_update, ~] = getCompressibilityAndFugacity(model, redrho_V(converged), ...
                                                                  P(converged), T(converged), y(converged, :), 'Gas', 10^-6);
                    
                redrho_V0(update, :) = redrho_V_update;
                Z_V0(update, :) = Z_V_update;
                
               
                % Setup unconverged variables
                [P, T, Nph, z, x, y, s, nl1, nv , nl2, redrho_L, redrho_V, redrho_HL, Z_L, Z_V, Z_HL, active] = ...
                updateUnConvergedVariables(model, P, T, Nph, z, x, y, s, nl1, nv , nl2, redrho_L, redrho_V, redrho_HL, ...
                                            Z_L, Z_V, Z_HL, active, converged)
                
            end
            
            
            %% Two-phase Solid-Liquid Flash VLE calculation 
            if any(Nph == phaseflag({'HL', 'L'}))           
                
                converged = (Nph == phaseflag({'HL', 'L'}));
                update = active(converged);
                
                
                eos.converged(update,:) = true;
                                
                fprintf('Compute L-S flash\n');                
                eosLSmodel = model.eosLSmodel;
                
                [x_update, s_update, nl1_update, nl2_update, Nph_update, Z_L_update, Z_HL_update, redrho_L_update, redrho_HL_update, eos_update] = eosLSmodel.solveFlash(redrho_L(converged), redrho_HL(converged), P(converged), T(converged), z(converged,:), x(converged,:), s(converged,:));
                
                x0(update, :) = x_update; 
                y0(update, :) = zeros(nnz(update), numC); 
                s0(update, :) = s_update; 
                
                nl10(update, :) = nl1_update; 
                nv0(update, :) = 0; 
                nl20(update, :) = nl2_update; 
                
                Z_L0(update, :) = Z_L_update;
                Z_V0(update, :) = 1; 
                Z_HL0(update, :) = Z_HL_update;
                
                redrho_L0(update, :) = redrho_L_update;
                redrho_V0(update, :) = 0; 
                redrho_HL0(update, :) = redrho_HL_update;                
                
                Nph0(update) = Nph_update;
                
                % Here, we are SURE so can setup unconverged variables
                [P, T, Nph, z, x, y, s, nl1, nv , nl2, redrho_L, redrho_V, redrho_HL, Z_L, Z_V, Z_HL, active] = ...
                updateUnConvergedVariables(model, P, T, Nph, z, x, y, s, nl1, nv , nl2, redrho_L, redrho_V, redrho_HL, ...
                                            Z_L, Z_V, Z_HL, active, converged);
                if any(nl2 == 0)
                    update = (nl2 == 0);
                    s(update, :)     = 0; 
                    Z_HL(update)      = 1; 
                    redrho_HL(update) = 0;
                end
            
            end
            
            

            %% Three-phase Vapor-Liquid-Solid Flash VLSE calculation 
            if any(Nph == phaseflag({'V', 'L', 'HL'}))               
                
                converged = (Nph == phaseflag({'V', 'L', 'HL'}));
                fprintf('Compute V-L-S flash\n');
                eosVLSmodel = model.eosVLSmodel;            
               [x, y, s,  nl1, nv, nl2, Nph, Z_L, Z_V, Z_HL,   redrho_L, redrho_V, redrho_HL, ~] = eosVLSmodel.solveFlash( redrho_L, redrho_V, redrho_HL, P, T, z, x, y, s, nv,fs_W,f_S); 
                                                
               % when zeros solid phase
               if any(nl2 == 0)
                    update = (nl2 == 0);
                    s(update,:)        = 0; 
                    Nph(update)      = phaseflag({'V', 'L'}); 
                    Z_HL(update)      = 1; 
                    redrho_HL(update) = 0;
               end
                update = active(converged);
                
                %% We re-inject the solid salt as components 
                H2O = find(strcmp(eosVLSmodel.densitymodel.pcsaftmodel.names, 'H2O'));
                CO2 = find(strcmp(eosVLSmodel.densitymodel.pcsaftmodel.names, 'CO2'));
                NaCl = find(strcmp(eosVLSmodel.densitymodel.pcsaftmodel.names, 'Na')); % now its NaCl 
                x = [x(:, H2O : CO2) 0.5*x(:, NaCl) 0.5*x(:, NaCl)];
                y = [y zeros(numel(P),1)];                
                x0(update, :) = x;
                y0(update, :) = y;
                s0(update, :) = repmat([0 0 0.5 0.5],numel(P),1);
                
                nl10(update) = nl1; 
                nv0(update)  = nv; 
                nl20(update) = nl2; 
                
                Z_L0(update, :) = Z_L;
                Z_V0(update, :) = Z_V; 
                Z_HL0(update, :) = Z_HL;
                
                redrho_L0(update, :) = redrho_L;
                redrho_V0(update, :) = redrho_V; 
                redrho_HL0(update, :) = redrho_HL;     
                
                %% All cells should be done here!               
                eos.converged(update,:) = true;
                active = active(~converged);

                
            end
            
            
            % active cells should be empty!
            assert(isempty(active), 'problem: we missed come cases');
            
            % Update Saturation Values
            state = model.setProp(state, 's_V'  , nv0);
            state = model.setProp(state, 's_L'  , nl10);
            state = model.setProp(state, 's_HL' , nl20);
            % Update Molar Fraction Values            
            state = model.setProp(state, 'X_V' , y0);
            state = model.setProp(state, 'X_L' , x0);
            state = model.setProp(state, 'X_HL', s0);
            % Update Compressibility Factor Values
            state = model.setProp(state, 'Z_V' , Z_V0);
            state = model.setProp(state, 'Z_L' , Z_L0);
            state = model.setProp(state, 'Z_HL', Z_HL0);        
            % Update Reduced Density
            state = model.setProp(state, 'reducedDensity_V' , redrho_V0);
            state = model.setProp(state, 'reducedDensity_L' , redrho_L0);
            state = model.setProp(state, 'reducedDensity_HL', redrho_HL0);

            state.eos = eos;
            failure   = false;
            converged = true;
            % values_converged = values <= model.nonlinearTolerance;
            if model.verbose
                numC = model.getNumberOfComponents;
                compnames = arrayfun(@(x) sprintf('comp %d', x), (1 : numC), 'uniformoutput', false);
                printConvergenceReport(compnames, values, values_converged, iteration);
            end
            report = model.makeStepReport('Failure'  , failure, ...
                                          'Converged', converged);
        end

        
        function [state, report] = updateAfterConvergence(model, state0, state, dt, drivingForces)
            [state, report] = updateAfterConvergence@PhysicalModel(model, state0, state, dt, drivingForces);
        end
        

        function y = computeVapor(model, L, K, z)
            if isstruct(L)
                y = model.computeVapor(L.L, L.K, L.components);
                return
            end
            if iscell(z)
                y = z;
                sv = 0;
                for i = 1:numel(z)
                    bad = ~isfinite(K{i});
                    y{i} = K{i}.*z{i}./(L + (1-L).*K{i});
                    y{i}(bad) = 0;
                    sv = sv + value(y{i});
                end
                y = cellfun(@(k, zi) k.*zi./(L + (1-L).*k), K, z, 'UniformOutput', false);
                y = cellfun(@(x) x./sv, y, 'UniformOutput', false);
                assert(all(cellfun(@(x) all(isfinite(value(x))), y)));
            else
                y = K.*z./bsxfun(@plus, L, bsxfun(@times, 1-L, K));
                y(~isfinite(K)) = z(~isfinite(K));
                y = bsxfun(@rdivide, y, sum(y, 2));
                assert(all(isfinite(y(:))));
            end
        end
        
        function [x, sv] = computeLiquid(model, L, K, z)
            if isstruct(L)
                x = model.computeLiquid(L.L, L.K, L.components);
                return
            end
            if iscell(z)
                x = z;
                sv = 0;
                for i = 1:numel(z)
                    bad = ~isfinite(K{i});
                    x{i} = z{i}./(L + (1-L).*K{i});
                    x{i}(bad) = 0;
                    sv = sv + value(x{i});
                end
                x = cellfun(@(x) x./sv, x, 'UniformOutput', false);
                assert(all(cellfun(@(x) all(isfinite(value(x))), x)));
            else
                tmp = bsxfun(@times, 1-L, K);
                x = z./bsxfun(@plus, L, tmp);
                x(~isfinite(K)) = 0;
                sv = sum(x, 2);
                x = bsxfun(@rdivide, x, sv);
                assert(all(isfinite(x(:))));
            end
        end
        
        
        function [s, sums] = computeHeavyLiquid(model, S, V, Kl, Kv, z)
        % return vapor (y) and salt (s) compositions. 
            if isstruct(S)
                [s, sums] = model.computeHeavyLiquid(S.S, S.V, S.Kl, S.Kv, S.components); 
                return
            end
            
            if iscell(z)
                s = z; 
                sums = 0; 
                
                for i = 1:numel(z)
                    bad =~isfinite(Kv{i}) & ~isfinite(Kl{i}); 
                    s{i} = Kl{i}.*z{i}./(1 + V.*(Kv{i} - 1) + S.*(Kl{i} - 1)); 
                    s{i}(bad) = 0; 
                    sums = sums + double(s{i}); 
                end
                s = cellfun(@(kv, kl, zi) kl.*zi./(1 + S.*(kl - 1) + V.*(kv - 1)), Kv, Kl, z, 'UniformOutput', false); 
                s = cellfun(@(x) x./sums, s, 'UniformOutput', false); 
                assert(all(cellfun(@(x) all(isfinite(double(x))), y))); 
                
            else
                s = Kl.*z./(1 + (bsxfun(@times, V, (Kv - 1))) + (bsxfun(@times, (Kl - 1), S))); 
                s(~isfinite(Kl)) = z(~isfinite(Kl)); 
                s = bsxfun(@rdivide, s, sum(s, 2)); 
                sums = sum(s, 2); 
                assert(all(isfinite(s(:)))); 
            end
        end

        
        function Z_S = PcsaftSaltCompFactor(model, P, T)        
                Z_S = P./T./(2.16/58.44*1e6)./8.3144598;               
        end
        function f_S = saltFugacity(model, P, T)
            f_S = -3.998e+06 + 8975.*T - 5642.*(P./1e5) + 46.9 .*T.^2 + 28.74.*T.*(P./1e5);
        end
               
        function [K_L, X_HL, Nph, Z_HL] = updateSolidSaltComposition(model, P,T, X_L,X_HL ,Nph)

            % Number of components
            numC = model.densitymodel.pcsaftmodel.numC;
            % Shortcut
            phaseflag = @(flagstr) model.parsePhaseFlag(flagstr);
            % Compute the salt compressibility factor
            Z_HL = model.PcsaftSaltCompFactor(P, T);                       
            % We are sure that Salt exist so we update Nph, X_HL, and K_L
            Nph(Nph ==  phaseflag({'V', 'L'})) = phaseflag({'V', 'L', 'HL'});            
            Nph(Nph == phaseflag({'L'})) = phaseflag({'L', 'HL'});   
            
            ns = nnz(Nph == phaseflag({'V', 'L', 'HL'}));
            Na = find(strcmp(model.densitymodel.pcsaftmodel.names, 'Na'));
            Cl = find(strcmp(model.densitymodel.pcsaftmodel.names, 'Cl'));
            cp = zeros(1,numC);
            cp ([Na Cl]) = 0.5;
            X_HL(Nph == phaseflag({'V', 'L', 'HL'}), :) = repmat(cp,ns , 1);            
            
            K_L = X_HL./X_L;
        end
        
        function [xy, Nph, redrho_L_cnv, redrho_V_cnv] = initiateVaporStability(eos, p, T, z, redrho_L, redrho_V)
            
            %% Stability Analysis with QNNC method        
            opt = struct('tol_equil', 1e-10, 'tol_trivial', 1e-5);
            
            assert(all(p > 0 & isfinite(p)), 'Positive, finite pressures required for phase stability test.');
            
            tol_trivial = opt.tol_trivial;
            tol_equil   = opt.tol_equil;
            S_tol       = 10^-8;
            %% We start with single components
            H2O = find(strcmp(eos.densitymodel.pcsaftmodel.names, 'H2O'));
            CO2 = find(strcmp(eos.densitymodel.pcsaftmodel.names, 'CO2'));
            IsSingleCompH2O = (z(:, H2O) == 1) | (z(:, CO2) == 0); 
            IsSingleCompCO2 = (z(:, CO2) == 1) | (z(:, H2O) == 0);
            nG = numel(p);
            numC = eos.densitymodel.pcsaftmodel.numC;
            active = ~IsSingleCompH2O & ~IsSingleCompCO2;

            %% Set initial values
            zeta = ones(nG,1); 
            phiz = zeros (nG, numC); 
            [phiz(active, :), ~, ~, redrho_L, ~] = getCompressibilityAndFugacity(eos, redrho_L(active), ...
                                                                                           p(active), T(active), ...
                                                                                           z(active, :), 'Liquid',[]); ...
            h = log(z) + log(phiz);             
            %% Set the new xy  composition for the new phase
            Y = zeros(nG, numC); 
            Y(:, H2O) = 0.001; % water
            Y(:, CO2) = 0.999; % CO2
            xy = bsxfun(@rdivide, Y, sum(Y, 2));
            [phi_xy, ~, ~, redrho_V, ~] = getCompressibilityAndFugacity(eos, redrho_V, ...
                                                                                           p, T, ...
                                                                                           xy, 'Gas',[]); 
            r = log(Y(:, H2O : CO2)) + log(phi_xy(:, H2O : CO2)) - h(:, H2O : CO2); 
            lnY = log(Y(:, H2O : CO2)) - zeta.*r;
            Y(:, H2O : CO2) = exp(lnY); 
            Y_old = Y(:, H2O : CO2); 
            
            r_old = r; 
            dr    = r - r_old;
            dlnY  = -zeta .* r_old; 
            
            normdr    = sum(abs(dr).^2, 2).^(1/2); 
            normr_old = sum(abs(r_old).^2, 2).^(1/2); 
            normdlnY  = sum(abs(dlnY).^2, 2).^(1/2); 

            converge = ~active; 
            redrho_V_cnv(active,:) = redrho_V(active,:);
            redrho_L_cnv(active,:) = redrho_L(active,:);

            %% find Y
            for stepNo = 1 : 8000

                xy(active, :)     = bsxfun(@rdivide, Y(active, :), sum(Y(active, :), 2)); 

                [phi_xy(active, :), ~, ~, redrho_L, ~] = getCompressibilityAndFugacity(eos, redrho_L(active), ...
                                                                                           p(active), T(active), ...
                                                                                           xy(active, :), 'Liquid',[]);
                
                %% residuals                                                                       
                r(active, :)      = log(Y(active, 1:2)) + log(phi_xy(active, 1:2)) - h(active, 1:2); 
                dlnY(active, :)   = -zeta(active) .* r_old(active, :); 
                dr(active, :)     = r(active, :) - r_old(active, :); 
                
                %% norm calculations
                normdr(active)    = sum(abs(dr(active, :)).^2, 2).^(1/2); 
                normr_old(active) = sum(abs(r_old(active, :)).^2, 2).^(1/2); 
                normdlnY(active)  = sum(abs(dlnY(active, :)).^2, 2).^(1/2);
                %% updates
                zeta(active)   = zeta(active).*(normr_old(active).*normdlnY(active))./(normdr(active).*normdlnY(active)); %%% eq. 16
                lnY(active, :) = lnY(active, :) - zeta(active).*r(active, :); 
                Y(active, 1 : 2) = exp(lnY(active, :)); 

                %% Conv cheks
                Y_norm = sum((1 - Y_old./Y(:, H2O : CO2)).^2, 2); 
                r_old(active, :) = r(active, :); 
                Y_old(active, :) = Y(active, H2O : CO2); 
                
 
                converge  = (Y_norm(active) < tol_equil);                 
                redrho_V_cnv(active,:) = redrho_V(active,:);
                redrho_L_cnv(active,:) = redrho_L(active,:);
                active = active(~converge);
                if all(converge)
                    dispif(mrstVerbose() > 1, 'Stability done in %d iterations\n', stepNo);
                    break
                end
            end
            
            if ~all(converge)
                warning('Stability test did not converge');
            end

            S_V = sum(Y, 2);
            
            %% Check convergence to trivial solution
            xy = bsxfun(@rdivide, Y, sum(Y, 2));

            xy(IsSingleCompCO2,:) = 0;
            xy(IsSingleCompCO2,2) = 1;

            xy(IsSingleCompH2O,:) = 0;
            xy(IsSingleCompH2O,1) = 1;

            trivialtoz = ((sum(log(xy./z).^2, 2)./numC) < tol_trivial);
            V_stable = (S_V <= (1 + S_tol)) | trivialtoz;
            
            % shortcut
            pfun = @(flagstr) eos.parsePhaseFlag(flagstr);  
            % Flag of phases zero for single phase liquid/ subcool liq
            Nph = zeros(nG,1);
                    
            Nph(V_stable)  = pfun({'L'});  % Single Liquid Phase         
            Nph(~V_stable) = pfun({'V', 'L'});  % Liquid-Heavy Liquid         
            Nph(IsSingleCompCO2)  = pfun({'V'});  % Liquid-Heavy Liquid         

        end

        function vars = collectVariables(model, vars, fieldnames, varargin)

            assert(numel(fieldnames) == numel(varargin), 'wrong numbers of elements');
            
            for ind = 1 : numel(fieldnames)
                vars.(fieldnames{ind}) = varargin{ind};
            end
        
        end
        
        function [z, x, y, s, nl1, nv, nl2, Nph, redrho_L, redrho_V, redrho_HL, Z_L, Z_V, Z_HL, K_L, K_V] = ...
                updateConvergedVariables(model, vars, update, converged)
        
            z = vars.z;
            x = vars.x;
            y = vars.y;
            s = vars.s;
            nl1 = vars.nl1;
            nv = vars.nv;
            nl2 = vars.nl2;
            Nph = vars.Nph;
            redrho_L = vars.redrho_L;
            redrho_V = vars.redrho_V;
            redrho_HL = vars.redrho_HL;
            Z_L = vars.Z_L;
            Z_V = vars.Z_V;
            Z_HL = vars.Z_HL;
            K_L = vars.K_L;
            K_V = vars.K_V;
            
            z(converged,:) = vars.z0(update,:);
            x(converged,:) = vars.x0(update,:);
            y(converged,:) = vars.y0(update,:);
            s(converged,:) = vars.s0(update,:);
            
            nl1(converged) = vars.nl10(update);
            nv(converged)  = vars.nv0(update);
            nl2(converged) = vars.nl20(update);
            
            Nph(converged) = vars.Nph0(update);
            
            redrho_L(converged)  = vars.redrho_L0(update); 
            redrho_V(converged)  = vars.redrho_V0(update);
            redrho_HL(converged) = vars.redrho_HL0(update);
        
            Z_L(converged)   = vars.Z_L0(update);
            Z_V(converged)   = vars.Z_V0(update);
            Z_HL(converged)  = vars.Z_HL0(update);
            K_L(converged,:) = vars.K_L0(update,:);
            K_V(converged,:) = vars.K_V0(update,:);
            
        end        

        function [P, T, Nph, z, x, y, s, nl1, nv , nl2, redrho_L, redrho_V, redrho_HL, Z_L, Z_V, Z_HL, active] = ...
                updateUnConvergedVariables(model, P, T, Nph, z, x, y, s, nl1, nv , nl2, redrho_L, redrho_V, redrho_HL, ...
                                            Z_L, Z_V, Z_HL, active, converged)
            active = active(~converged); 

            P = P(~converged);
            T = T(~converged);
            Nph = Nph(~converged);

            z = z(~converged, :);
            x = x(~converged, :);
            y = y(~converged, :);
            s = s(~converged, :);

            nl1 = nl1(~converged);
            nv  = nv(~converged);
            nl2 = nl2(~converged);

            redrho_L = redrho_L(~converged); 
            redrho_V = redrho_V(~converged);
            redrho_HL = redrho_HL(~converged);

            Z_L = Z_L(~converged); 
            Z_V = Z_V(~converged);
            Z_HL = Z_HL(~converged);
            
        end
    end        
end




%{
Copyright 2009-2017 SINTEF ICT, Applied Mathematics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
