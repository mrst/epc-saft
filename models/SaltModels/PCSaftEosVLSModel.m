classdef PCSaftEosVLSModel < GenericEquationOfStateModel
%
%
% SYNOPSIS:
%   PCSaftEosVLSModel(densitymodel)
%
% DESCRIPTION:
%    A model encompassing routines to solve the Vapor-Liquid-Solid equilibrium using
%    the Sequential Substituion Iterations (SSI) and a Density Model built 
%    with  ePC-saft EoS
%
% PARAMETERS:
%   densitymodel - density model built with epc-saft EoS
%   fluid -        fluid properties input
%   rachfordricemodel- Rachford Rice Model
% RETURNS:
%   state - Updated state where saturations and compositions solve
%           three phase equilibrium equations.
%
% EXAMPLE:
%
% SEE ALSO: RachfordRiceModel, PCSaftEosVLModel
%

    properties
        densitymodel
        rachfordricemodel
    end
    
    
    methods
        
        function model = PCSaftEosVLSModel(densitymodel)
            fluid = densitymodel.pcsaftmodel;
            model = model@GenericEquationOfStateModel(fluid, 3);
            model.densitymodel = densitymodel;
            % Make sure that 'SALT' is the key for RachfordRiceModel
            model.rachfordricemodel = RachfordRiceModel(fluid,densitymodel,'SALT');        
        end                       
        
        function n = getReferenceKIndex(model)
        % the reference phase is the vapor phase
            n = 3;
        end
        
        function names = getPhaseNames(model)
            names = {'L', 'V', 'HL'};
        end


        function [state, report] = stepFunction(model, state, state0, dt, drivingForces, linsolve, nonlinsolve, iteration, varargin) %#ok
            
                                   
            T = model.getProp(state, 'T');
            P = model.getProp(state, 'pressure');
            z = model.getProp(state, 'components');
            
            nc   = numel(P);
            
            % Basic assertions
            assert(all(sum(z, 2) > 0.999), 'Molar fractions must sum up to unity.')
            assert(iteration > 0);
            assert(all(P >= 0));

            
            %% If we get here, we are using a proper 3-phase flash
            if iteration == 1
                
                Nph = model.parsePhaseFlag({'V', 'L', 'HL'}); % Three-phases Liquid-Vapor-Heavy Liquid
                state.Nph = Nph*ones(nc, 1);
                                
                % Count total iterations
                state.eos.itCount   = zeros(nc, 1);
                state.eos.itDens    = zeros(nc, 3);
                state.eos.converged = false(nc, 1);
            end
            
            z0    = model.getProp(state, 'components');
            x0    = model.getProp(state, 'X_L');
            y0    = model.getProp(state, 'X_V');
            s0    = model.getProp(state, 'X_HL');
            nv0   = model.getProp(state, 's_V');
            nl20  = model.getProp(state, 's_HL');
            nl10  = model.getProp(state, 's_L');
            
            ZL0  = model.getProp(state, 'Z_L');            
            ZV0  = model.getProp(state, 'Z_V');
            ZS0  = model.getProp(state, 'Z_HL');


            kl0   = model.getProp(state, 'K_L');
            kv0   = model.getProp(state, 'K_V');
            
            redrho_L0  = model.getProp(state, 'reducedDensity_L'); 
            redrho_V0  = model.getProp(state, 'reducedDensity_V'); 
            redrho_S0  = model.getProp(state, 'reducedDensity_HL'); 
            Nph0 = state.Nph;

            if iteration == 1                                
                % We need to treat the Salt as a component  
                 SaltInfo = state.SaltInfo;
                [z0,x0,y0,s0, kv0,kl0,nl20] = addSaltasComponent(model, z0,x0,y0,nl20, SaltInfo);
            end
            
            
            active = ~state.eos.converged;
            active = find(active);
            %% Only apply calculations for cells that have not converged yet

            z = z0(active, :);
            x = x0(active, :);
            y = y0(active, :);
            s = s0(active, :);

            kl  = kl0(active, :); 
            kv  = kv0(active, :);            

            nl1 = nl10(active);
            nl2 = nl20(active);
            nv  = nv0(active);
            
            ZL  = ZL0(active);
            ZV  = ZV0(active);
            ZS  = ZS0(active);
            Nph = Nph0(active);
            
            redrho_L  = redrho_L0(active);
            redrho_S  = redrho_S0(active);
            redrho_V  = redrho_V0(active);
            
            rrmodel = model.rachfordricemodel;
            fluidCase = rrmodel.fluidCase;
            tol = 10^-6; 
            [statee,equilvalsl, equilvalsv, itDens_V, itDens_L,itDens_S] = rrmodel.solveFlash3Phases(z, kl, kv, nl2, nv, P(active), T(active),Nph,ZL, ZV,ZS, redrho_L, redrho_V, redrho_S, iteration, fluidCase,  tol);
            
            z    = model.getProp(statee, 'components');
            x    = model.getProp(statee, 'X_L');
            y    = model.getProp(statee, 'X_V');
            s    = model.getProp(statee, 'X_HL');
            nv   = model.getProp(statee, 's_V');
            nl2  = model.getProp(statee, 's_HL');
            nl1  = model.getProp(statee, 's_L');
            
            ZL  = model.getProp(statee, 'Z_L');            
            ZV  = model.getProp(statee, 'Z_V');
            ZS  = model.getProp(statee, 'Z_HL');


            kl   = model.getProp(statee, 'K_L');
            kv   = model.getProp(statee, 'K_V');
            
            redrho_L  = model.getProp(statee, 'reducedDensity_L'); 
            redrho_V  = model.getProp(statee, 'reducedDensity_V'); 
            redrho_S  = model.getProp(statee, 'reducedDensity_HL');

            %% Convergence criteria            
            LVconvcriteria = (sum(equilvalsl.^2, 2));                              
            LSconvcriteria =  (sum(equilvalsv.^2, 2));           
            lvvconverged = (LVconvcriteria < 1e-12)&(LSconvcriteria < 1e-12); 

            %% Update                                     
            x0(active, :) = x;
            y0(active, :) = y;
            s0(active, :) = s;
            kl0(active, :) = kl; 
            kv0(active, :) = kv; 
            ZL0(active)   = ZL; 
            ZV0(active)   = ZV;                
            ZS0(active)   = ZS; 

            z0(active,:)   = z;                  

            
            redrho_L0(active) = redrho_L;                             
            redrho_V0(active) = redrho_V;
            redrho_S0(active) = redrho_S;                             

            nl10(active)  = nl1;
            nl20(active)  = nl2;            
            nv0(active)   = nv;
            


            convc = min(LVconvcriteria(~lvvconverged),LSconvcriteria(~lvvconverged));

            % Cells still active
            active = active(~lvvconverged);
            
            state = model.setProp(state, 'X_L' , x0);
            state = model.setProp(state, 'components' , z0);

            state = model.setProp(state, 'X_V' , y0);
            state = model.setProp(state, 'X_HL', s0);
            state = model.setProp(state, 'K_L' , kl0);
            state = model.setProp(state, 'K_V' , kv0);
            state = model.setProp(state, 's_L' , nl10);
            state = model.setProp(state, 's_V' , nv0);
            state = model.setProp(state, 's_HL', nl20);
            state = model.setProp(state, 'Z_L' , ZL0);
            state = model.setProp(state, 'Z_V' , ZV0);
            state = model.setProp(state, 'Z_HL', ZS0);

            state = model.setProp(state, 'reducedDensity_L', redrho_L0); 
            state = model.setProp(state, 'reducedDensity_V', redrho_V0); 
            state = model.setProp(state, 'reducedDensity_HL', redrho_S0);  
            
            state.convcriteria = convc;        
                        
            converged = true(nc, 1);
            converged(active) = false;
            state.eos.converged = converged;
            
            failure = false;
            failureMsg = '';

            %% Residuals is here the max of cell-residuals 
            %% Attention only  cell residuals are used for convergence check
            Residuals = max(LVconvcriteria+LSconvcriteria);
            report = model.makeStepReport('Failure'           , failure              , ...
                                          'FailureMsg'        , failureMsg           , ...
                                          'Converged'         , all(converged), ...
                                          'Residuals'         , Residuals, ... 
                                          'ResidualsConverged', converged );
            report.ActiveCells = sum(active);
            
        end

        function [x, y, s, nl1, nv, nl2, Nph, Z_L, Z_V, Z_S, redrho_L, redrho_V, redrho_S, eos] = solveFlash(model, redrho_L,redrho_V,  redrho_S, P, T, z, x, y, s,nv,AqueousSalt,SolidSalt)
            
            state.pressure = P;
            state.T = T;
            state.components = z;
            
            % Fugacity of  salt in liquid phase 
            state.SaltInfo.fs_W = AqueousSalt;
            
            % Fugacity of solid salt 
            state.SaltInfo.fS = SolidSalt;
            
            state = model.validateState(state);            
            % We insert (maybe!) a better initialization for the density calculation
            state = model.setProp(state, 'reducedDensity_L', redrho_L);
            state = model.setProp(state, 'reducedDensity_HL', redrho_S);
            state = model.setProp(state, 'reducedDensity_V', redrho_V);
            
            state = model.setProp(state, 'X_L', x);
            state = model.setProp(state, 'X_V', y);
            state = model.setProp(state, 'X_HL', s);
            
            state = model.setProp(state, 's_V', nv);

            solver = getDefaultFlashNonLinearSolver;
            
            [state, ~] = solver.solveTimestep(state, 1000*year, model);
            
            x   = model.getProp(state, 'X_L');
            y   = model.getProp(state, 'X_V');
            s   = model.getProp(state, 'X_HL');
            
            nl1 = model.getProp(state, 's_L');
            nv = model.getProp(state, 's_V');
            nl2 = model.getProp(state, 's_HL');
            
            Z_L = model.getProp(state, 'Z_L');
            Z_V = model.getProp(state, 'Z_V');
            Z_S = model.getProp(state, 'Z_HL'); 
            
            redrho_L = model.getProp(state, 'reducedDensity_L');
            redrho_V = model.getProp(state, 'reducedDensity_V');
            redrho_S = model.getProp(state, 'reducedDensity_HL');
            
            Nph = state.Nph;                        
            eos = state.eos;

                        
        end
        
        function [state, report] = updateAfterConvergence(model, state0, state, dt, drivingForces)
            [state, report] = updateAfterConvergence@PhysicalModel(model, state0, state, dt, drivingForces);
        end
        
    end

end


%{
  Copyright 2009-2021 SINTEF ICT, Applied Mathematics.

  This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

  MRST is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  MRST is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
