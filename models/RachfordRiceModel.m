classdef RachfordRiceModel < GenericEquationOfStateModel
%
%
% SYNOPSIS:
%   RachfordRiceModel(fluid,densitymodel,fluidCase)
%
% DESCRIPTION:
% Three-phases Rachford model to be used for
% (a) Vapor-Liquid-Heavy Liquid  model for Asphaltenic fluid
% (b) Vapor-Liquid-Solid  model for Salty fluid (Solid Salt)
%
% PARAMETERS:
%    fluid - fluid properties input  
%    densitymodel - density model 
%    fluidCase - Asphaltenic fluid or Salty fluid 
%
% RETURNS:
%   Liquid, Vapor and Solid or Heavy-Liquid distribution.
% EXAMPLE: 
%
% SEE ALSO: PCSaftEosVLSModel
%

properties
    %% DensityModel  
    densitymodel 
    %% FluidCase: Solid or Heavy-Liquid
    fluidCase
end
    
    methods
        
        function model = RachfordRiceModel(fluid,densitymodel,fluidCase)
            model = model@GenericEquationOfStateModel(fluid, 3);
            model.densitymodel = densitymodel;
            model.nonlinearTolerance = 1e-7;
            model.fluidCase = fluidCase;
        end                       
       
        function n = getReferenceKIndex(model)
        % the reference phase is the heavy-liquid/ solid phase
            n = 3;
        end
        
        function names = getPhaseNames(model)
            names = {'L', 'V', 'HL'};
        end
        
        function [vars, names, origin] = getPrimaryVariables(model, state)
            
            sL = model.getProp(state, 's_L');
            sV = model.getProp(state, 's_V');
            sHL = model.getProp(state, 's_HL');
            KL = model.getProp(state, 'K_L');
            KV = model.getProp(state, 'K_V');
            vars = {sL, sV, sHL, KV, KL};
            names = {'s_L', 's_V', 's_HL','K_L', 'K_V'};
            origin = cell(1, numel(vars));
            [origin{:}] = deal(class(model));
            
        end
        
        function [state, report] = stepFunction(model, state, state0, dt, drivingForces, linsolve, nonlinsolve, iteration, varargin) %#ok
            [state, report] = stepFunction@PhysicalModel(model, state, state0, dt, drivingForces, linsolve, nonlinsolve, iteration, varargin{:});
        end
        
        function state = validateState(model, state)
            
            nph = model.getNumberOfPhases;
            ncomp = model.getNumberOfComponents;
            
            assert(isfield(state, 'components'), 'missing components');
            ncell = size(state.components, 1);            
            
            Knames = model.getKNames;
            if ismember(model.fluidCase, 'SALT')
                %% Here we still treat salt as a component
                ncomp = model.getNumberOfComponents - 1;
                 for iph = 1 : nph
                     name = Knames{iph};
                     if iph ~= model.getReferenceKIndex                   
                         assert(isfield(state, name), 'missing K variable');
                         assert(all(size(state.(name)) == [ncell, ncomp]), 'size for K variable does not match');
                     end
                 end
            else               
                for iph = 1 : nph
                    name = Knames{iph};
                if iph ~= model.getReferenceKIndex
                    assert(isfield(state, name), 'missing K variable');
                    assert(all(size(state.(name)) == [ncell, ncomp]), 'size for K variable does not match');
                end
                end
            end

            if ~isfield(state, 's')
                state.s = repmat([0.4, 0.4, 0.2], ncell, 1);
            else
                assert(all(size(state.s) == [ncell, nph]), 'size for K variable does not match');
            end
            
        end
        
        
        function [eqs, names, types, state] = getModelEquations(model, state0, state, dt, drivingForces)
            
            numC = model.getNumberOfComponents;
            

            T = model.getProp(state, 'T');
            P = state.P;
            sL = model.getProp(state, 's_L');
            sV = model.getProp(state, 's_V');
            sHL = model.getProp(state, 's_HL');

            
            
            z = model.getProp(state, 'components');

            
            redrho_L  = state.redrho_L; 
            redrho_V  = state.redrho_V; 
            redrho_HL  = state.redrho_HL;
            
            KL = model.getProp(state, 'K_L');
            KV = model.getProp(state, 'K_V');

   
           
            % Compute liquid component fraction             
            [x, ~] = computeLiquid( value(sHL), value(sV), value(state.K_L), value(state.K_V), z);           
            % Compute Vapor component fraction
            y = computeVapor( value(sHL), value(sV), value(state.K_L), value(state.K_V), z); 
            % compute solid (Heavy Liquid) component fraction  
            s = computeHeavyLiquid(value(sHL), value(sV), value(state.K_L), value(state.K_V), z);
            
            
            [phi_V, ~, ~, redrho_V, itDens_V] = getCompressibilityAndFugacity(model, redrho_V,P, T, y, 'Gas');

            % Liquid fugacity coeficcient         
            [phi_L, ~, ~, redrho_L, itDens_L] = getCompressibilityAndFugacity(model, redrho_L,P, T, x, 'Liquid');
            
            % Heavy-Liquid fugacity coeficcient         
            [phi_HL, ~, ~, redrho_HL, itDens_HL] = getCompressibilityAndFugacity(model, redrho_HL, P, T, s, 'Solid'); 
            
            denom = cell(numC, 1);
            for i = 1 : numC
                denom{i} = 1 + (KL(:, i) - 1).*sL + (KV(:, i) - 1).*sV;
            end
            
            EL = 0;
            EV = 0;
            for i = 1 : numC
                EL = EL + (KL(:, i) - 1).*z(:, i)./denom{i};
                EV = EV + (KV(:, i) - 1).*z(:, i)./denom{i};                
            end
                        
            %% Equi_Fugacity

            
            EqKL = log(KL)+ log(phi_HL)-log(phi_L)   ; 
            EqKV = log(KV)+ log(phi_V)-log(phi_L)   ; 

            
            eqs{1} = EL;
            eqs{2} = EV;             
            eqs{3} = EqKL;
            eqs{4} = EqKV;
            
            names = {'liquid', 'vapor', 'Kliquid', 'Kvapor'};
            types = {'cells', 'cells', 'cells', 'cells'};
            
            %% update redrho
            state = model.setProp(state, 'reducedDensity', [redrho_L redrho_V redrho_HL]); 
            state.itDens_V = itDens_V;
            state.itDens_L = itDens_L;
            state.itDens_HL = itDens_HL;

            
            
        end
        
        function [state, report] = updateState(model, state, problem, dx, drivingForces)
            
           sL = model.getProp(state, 's_L');
           sV = model.getProp(state, 's_V');
           KL = model.getProp(state, 'K_L');
           KV = model.getProp(state, 'K_V');
           
           dsL = model.getIncrement(dx, problem, 's_L');
           dsV = model.getIncrement(dx, problem, 's_V');
           
           numC   = model.getNumberOfComponents();
           nph    = model.getNumberOfPhases();
           ncells = size(sL, 1);

           lambda = ones(ncells, 1);
           for i = 1 : numC
               enum  = 1 + (KL(:, i) - 1).*sL + (KV(:, i) - 1).*sV;
               denom = (KL(:, i) - 1).*dsL + (KV(:, i) - 1).*dsV;
               coef  = - enum./denom;
               lambda(coef > 0) = min(lambda(coef > 0), coef(coef > 0));
           end
           lambda(lambda < 1) = 0.9*lambda(lambda < 1);
           
           dsL = lambda.*dsL;
           dsV = lambda.*dsV;
           
           state = model.updateStateFromIncrement(state, dsL, problem, 's_L');
           state = model.updateStateFromIncrement(state, dsV, problem, 's_V');
           
           report = [];
       end
        

        function [state, equilvalsl, equilvalsv, itDens_V, itDens_L, itDens_HL] = solveFlash3Phases(model, z, KL, KV, sHL, sV ,P , T,Nph, Z_L, Z_V, Z_HL, redrho_L, redrho_V, redrho_HL, iteration, fluidCase, tol)
            
            state.components = z;
            state.K_L = KL;
            state.K_V = KV;
            state.T = T;
            state.s = [1-sV-sHL,sV,sHL];
            state.P = P;
            state.Nph = Nph;
            state.redrho_L  = redrho_L;
            state.redrho_V  = redrho_V;
            state.redrho_HL = redrho_HL;

            state.Z_L  = Z_L;
            state.Z_V  = Z_V;
            state.Z_HL = Z_HL;
           
            state.convcriteria = tol;                
            
            % main: substitution method for Rachford equations
                      
            state = model.validateState(state);                            
            tmp = state; 
            state = model.substitutionCompositionUpdate(tmp,fluidCase);
                            
            % convergence values
            equilvalsl = state.equilvalsl;
            equilvalsv = state.equilvalsv;
            % Density update
            itDens_V = state.itDens_V;
            itDens_L = state.itDens_L;
            itDens_HL = state.itDens_HL;            
            
        end

        function [state, report] = updateAfterConvergence(model, state0, state, dt, drivingForces)
            
            [state, report] = updateAfterConvergence@PhysicalModel(model, state0, state, dt, drivingForces);
            
            state = model.capProperty(state, 's_HL', 0, 1);
            state = model.capProperty(state, 's_V', 0, 1);
            
            sHL = model.getProp(state, 's_HL');
            sV = model.getProp(state, 's_V');
            
            state = model.setProp(state, 's_L', 1 - (sHL + sV));
            state = model.capProperty(state, 's_HL', 0, 1);

            
        end
        
        
        function state = substitutionCompositionUpdate(model, state,fluidcase)            
            P = state.P;
            T = state.T;
            z = state.components;
            K_L = state.K_L;
            K_V = state.K_V;
            s_V  = model.getProp(state, 's_V');
            s_HL = model.getProp(state, 's_HL');
            redrho_L = state.redrho_L;
            redrho_V = state.redrho_V;
            redrho_HL = state.redrho_HL;
            Nph0 = state.Nph;          
            numC = model.densitymodel.pcsaftmodel.numC;
            nc = numel(P);            
            phaseflag = @(flagstr) model.parsePhaseFlag(flagstr);
            isLL  = (Nph0 == phaseflag({'HL', 'L'})) | (s_V == 0); 
            isLG  = (Nph0 == phaseflag({'V', 'L'})) | (s_HL == 0); 
            isLLG = (Nph0 == phaseflag({'HL', 'L', 'V'})) & (s_V ~= 0) & (s_HL ~= 0);
            
            if any (isLLG)
                s_HL(isLLG) = solveRachfordRice3phaseStep(s_HL(isLLG), s_V(isLLG), K_L(isLLG, :), K_V(isLLG, :), z(isLLG, :));
                s_V(isLLG) = solveRachfordRice3phaseStep(s_V(isLLG), s_HL(isLLG), K_V(isLLG, :), K_L(isLLG, :), z(isLLG, :)); 

            end
            
            if any (isLG)
                s_V(isLG)  = solveRachfordRice3phaseStep(s_V(isLG), s_HL(isLG), K_V(isLG,:), K_L(isLG,:), z(isLG,:));
                s_HL(isLG) = 0;
            end
            
            if any (isLL)
                s_HL(isLL) = solveRachfordRice3phaseStep(s_HL(isLL), s_V(isLL), K_L(isLL, :), K_V(isLL, :), z(isLL, :));
                s_V(isLL)  = 0;
            end
            
            
            % Compute liquid component fraction            
            [x, sumx] = computeLiquid( s_HL, s_V, K_L, K_V, z);            
            % Compute Vapor component fraction            
            y = computeVapor( s_HL, s_V, K_L, K_V, z);            
            % compute solid (Heavy Liquid) component fraction            
            s = computeHeavyLiquid(s_HL, s_V, K_L, K_V, z);
                                       
            tol = state.convcriteria;
            switch  fluidcase
                case 'ASPH'
                    %% Vapor fugacity 
                    [~, f_V, Z_V, redrho_V, itDens_V] = getCompressibilityAndFugacity(model, redrho_V,P, T,  y, 'Gas',tol);
                    %% Liquid fugacity         
                    [~, f_L, Z_L, redrho_L, itDens_L] = getCompressibilityAndFugacity(model, redrho_L,P, T, x, 'Liquid',tol);            
                    %% Heavy-Liquid fugacity         
                    [~, f_HL, Z_HL, redrho_HL, itDens_HL] = getCompressibilityAndFugacity(model, redrho_HL, P, T, s, 'Solid',tol);            
                case 'SALT'  
                    
                     %% The fugacity of salt is not computed within the original pcsaft so we treat this fluid case separately.
                     %% see {calculatePcsaftSaltFugacity.m} for more details
                     Na = find(strcmp(model.densitymodel.pcsaftmodel.names, 'Na'));
                     Cl = find(strcmp(model.densitymodel.pcsaftmodel.names, 'Cl'));
                     NaCl = min(Na,Cl);
                     nonNaCl = setdiff((1:numC)',[Na;Cl]);
                     nnc = numel(nonNaCl);
                     %% We updated x composition
                     newx = zeros(nc,numC);
                     newx(:,nonNaCl) = x(:,nonNaCl);
                     newx(:,Na) = 0.5.*x(:,NaCl);
                     newx(:,Cl) = 0.5.*x(:,NaCl);
                     %% We updated x composition
                     newy = [y zeros(nc, numC-NaCl)];
                     %% Vapor fugacity 
                     % Attention the salt (NaCl) dissolves only in the Liquid phase 
                     [~, f_V, Z_V, redrho_V, itDens_V] = getCompressibilityAndFugacity(model, redrho_V,P, T,  newy, 'Gas',tol);                     
                     f_V = f_V(:, 1:NaCl);

                     %% Solid fugacity         
                     fS   = calculatePcsaftSaltFugacity(model,P,newx,T); 
                     %%  fugacity of Salt in Water
                     fs_W = fS.AqueousSalt;
                     %% Fugacity of solid Salt 
                     f_HL  = fS.SolidSalt;    
                     f_HL = [zeros(nc, nnc), f_HL]; 
                     %% Fugacity of non-salt components
                     phil = fS.phiw;
                     if iscell(newx)&&iscell(phil)                           
                         fns_W = phil(:, nonNaCl).*newx(:, nonNaCl).*P;
                     else                         
                         fns_W  =  [phil{nonNaCl}].*newx(:, nonNaCl).*P;
                     end                                          
                     %% Assembling Liquid fugacity
                     f_L  = [fns_W  fs_W];

                     %% Compressibilty factors
                     Z_L  = fS.Zw;                     
                     Z_HL = PcsaftSaltCompFactor(P, T,[]);                                                                                                         
            end
                    
                    
           
            %% Compute fugacity ratios
            f_rl = bsxfun(@times, 	sumx, f_L./f_HL);
            f_rl(z == 0) = 1;
            f_rl(s == 0) = 1;
            
            f_rv = bsxfun(@times, sumx, f_L./f_V);
            f_rv(z == 0) = 1;
            f_rv(y == 0) = 1;                                    
            
            %% Update K_V and K_L and convergence criteria
            equilvalsv = abs(f_rv - 1);            
            equilvalsv(isLL,:) = 0;
            K_V(~isLL, :)      = K_V(~isLL, :).*f_rv(~isLL, :); 
            K_V(~isfinite(K_V)) = 1;
            
            equilvalsl = abs(f_rl - 1);
            equilvalsl(isLG,:) = 0;
            K_L(~isLG, :)      = K_L(~isLG, :).*(f_rl(~isLG, :)); 
            K_L(~isfinite(K_L)) = 1;
            
            
            s_L = 1 - s_HL - s_V;

            % safe-guards for degenerate compositions
            Z_V(all(y==0,2))  = 1;
            Z_HL(all(s==0,2)) = 1;
            Z_L(all(x==0,2))  = 1;   
            
            state = model.setProp(state, 'X_L' , x);
            state = model.setProp(state, 'X_V' , y);
            state = model.setProp(state, 'X_HL', s);

            state = model.setProp(state, 'K_L' , K_L);
            state = model.setProp(state, 'K_V' , K_V);
            state = model.setProp(state, 's_L' , s_L);
            state = model.setProp(state, 's_V' , s_V);
            state = model.setProp(state, 's_HL', s_HL);          
            state = model.setProp(state, 'Z' , [Z_L Z_V Z_HL]);
            state = model.validateState(state);


           
            itDens_V(y==0)  = 0;
            itDens_HL(s==0) = 0;
            itDens_L(x==0)  = 0;

            

            state.equilvalsl = equilvalsl;
            state.equilvalsv = equilvalsv;
            
            state.itDens_V = itDens_V;
            state.itDens_L = itDens_L;
            state.itDens_HL = itDens_HL;
                       
            state = model.setProp(state, 'reducedDensity', [redrho_L redrho_V redrho_HL]); 
            
        end
        
        

        
    end

end

%{
  Copyright 2009-2021 SINTEF ICT, Applied Mathematics.

  This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

  MRST is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  MRST is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
