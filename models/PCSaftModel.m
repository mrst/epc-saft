classdef PCSaftModel < PhysicalModel
%
%
% SYNOPSIS:
%   PCSaftModel(varargin)
%
% DESCRIPTION:
%   get the epc-saft fluid parameters       
% PARAMETERS:
%   varargin - fluid input 
%
% RETURNS:
%   epc-saft parameters
%
% EXAMPLE:
%
% SEE ALSO: AsphaltenicCrudeOil, NaClH20Co2Fluid
%


        
    properties
        
        numC  % number of components
        names % component names
        
        % Data for the chain model
        segNum     % number of segment in a chain component (see ref1)
        sigma      % Temperature-independent segment diameter (see ref1)
        epsilon    % Depth of the potential (see ref1)
        amodconsts % Universal model constants (see eq 18 in ref1)
        bmodconsts % Universal model constants (see eq 19 in ref1)
        kij        % Binary interaction parameters (see ref 1)
        kb         % Boltzmann constant 
        
        % Data for the association model
        numAss    % number of association sites (see ref2)
        num_site_i  % Number of sites for each component i (see ref 2)
        sites     % Description of the sites. This is a structure with four
                  % fields:
                  % - sites.component : the component number of the site
                  % - sites.type      : the type of the site, either donnor (value = 1) or acceptor (value = 0).
                  % - sites.kappa     : the value of kappa for this site
                  % - sites.epsilon   : the value of epsilon for this site
        kappaAss_ij   % Volume of interaction between the sites i and j (see ref 2)
        epsilonAss_ij % Association energy of interaction between the sites i and j (see ref 2)
        
        % Data for the ionic model
        numions % Number of ions (see ref 3)
        CN      % charge number
        e       % in SI
        
        TC      % Critical temperature
        PC      % Critical pressure
        VC      % Critical volume;
        omega   % acentric factor
        
        % Values that are computed from data (only once, they are independent of composition)
        sigmaij
        epsilonij
        diams % segment diameters
        
        % PCSaft property function grouping
        PCSaftPropertyFunctions
        % Polar 
        dipMo;
        dipNum;
        
        %% Flag which indicates if the correspondic model is used
        useIonic       % Ionic model
        useAssociative % Associative model
        usePolar % Polar model
        
        % for EquationOfStateModel compatibility (see getReducedPT member function there)
        acentricFactors   
        Tcrit
        Pcrit
        
    end

    methods
        function model = PCSaftModel(varargin)
            
            model = model@PhysicalModel([]);    
            
            opt = struct('casename', 'Co2H2oNaCl'); 
            opt = merge_options(opt, varargin{:});
            
            casename = opt.casename;
            model = model.setParameters(casename);
            model.AutoDiffBackend = DiagonalAutoDiffBackend('useMex', true, 'rowmajor', false);
            model = model.setupStateFunctionGroupings();
            
        end
        
        function model = setParameters(model, casename)
            
        % We load the properties. 
              dipNum = 0;
              dipMo = 0;
          switch casename          
              case 'checkassoc'                                 
                  % fluid for benchmarking association interactions                
                  [numC, Properties, Binar, Cons, numAsm, numAss, Ass, Tsite, numions] = inputscheckassociation();
                  CN    = Properties(:, 11);                
                  TC    = Properties(:, 12);  %% Critical Temperature Rankin  
                  PC    = Properties(:, 13);  %% Critical pressure psi
                  omega = Properties(:, 14);  %%
                  VC    = [];                
              case 'checkion'
                  % fluid for benchmarking ionic interactions
                  [numC, Properties, Binar, Cons, numAsm, numAss, Ass, Tsite, numions] = inputsionic();               
                  CN  = Properties(:, 12);
                  bic = [];
                  VC  = [];                
              case 'OilC'
                 [numC, Properties,Binar,Cons, numAss, Ass, Tsite, numions,names] = AsphaltenicCrudeOil();                
                  CN    = Properties(:, 12);                
                  TC    = Properties(:, 13);  %% Critical Temperature Rankin
                  PC    = Properties(:, 14);  %% Critical pressure psi
                  omega = Properties(:, 15);  %%
                  VC    = [];
              case 'OilC_v2'
                 [numC, Properties,Binar,Cons, numAss, Ass, Tsite, numions,names] = AsphaltenicCrudeOilV2();                
                 CN    = Properties(:, 12);                
                 TC    = Properties(:, 13);  %% Critical Temperature Rankin
                 PC    = Properties(:, 14);  %% Critical pressure psi
                 omega = Properties(:, 15);  %%
                 VC    = [];
              case 'Co2H2oNaCl'                
                  [numC,Properties,Binar,Cons, numAss, Ass, Tsite, numions, names] = NaClH20Co2Fluid();                
                 CN    = Properties(:, 12);                
                 TC    = Properties(:, 13);  %% Critical Temperature Rankin
                 PC    = Properties(:, 14);  %% Critical pressure psi
                 omega = Properties(:, 15);  %%
                 VC    = [];    
              case 'H2oNaCl'
                [numC, Properties, Binar, Cons, numAsm, numAss, Ass, Tsite, numions] = inputH2O_NaCl();                
                CN    = Properties(:, 12);                
                TC    = Properties(:, 13);  %% Critical Temperature Rankin
                PC    = Properties(:, 14);  %% Critical pressure psi
                omega = Properties(:, 15);  %%
                VC    = [];                
              case 'H2OCO2'                 
                [numC, Properties,Binar,Cons, numAss, Ass, Tsite, numions,names] = WaterCO2fluid();                
                CN    = Properties(:, 12);                
                TC    = Properties(:, 13);  %% Critical Temperature Rankin
                PC    = Properties(:, 14);  %% Critical pressure psi
                omega = Properties(:, 15);  %%
                VC    = []; 
              case 'H2OH2'                 
                [numC, Properties,Binar,Cons, numAss, Ass, Tsite, numions,dipMo, dipNum, names] = WaterH2Fluid();                
                CN    = Properties(:, 12);                
                TC    = Properties(:, 13);  %% Critical Temperature Rankin
                PC    = Properties(:, 14);  %% Critical pressure psi
                omega = Properties(:, 15);  %%
                VC    = []; 
              otherwise
                error('casename not recognized');
                
            end
            
            model.numAss = numAss;
            model.numC   = numC;
            
            if isempty(names)
                model.names = arrayfun(@(i) sprintf('comp_%d', i), (1 : numC)', 'uniformoutput', false);
            else
                model.names = names;
            end
            
            model.segNum    = Properties(:, 3);
            sigma           = Properties(:, 4);
            epsilon         = Properties(:, 5);
            kappaAss        = Properties(:, 6);
            epsilonAss      = Properties(:, 7);
            num_site_i      = Properties(:, 8);
            num_donnors_i   = Properties(:, 9);
            num_acceptors_i = Properties(:, 10);
        
            model.useAssociative = (sum(numAss) > 0);
            model.useIonic = (sum(CN) > 0);
            model.usePolar = (sum(dipMo) > 0);

            
            sites.components = rldecode((1 : numC)', num_site_i);
            sites.kappa      = rldecode(kappaAss, num_site_i);
            sites.epsilon    = rldecode(epsilonAss, num_site_i);
            sites.type       = zeros(numel(sites.components), 1);
            comp_ind = [1; cumsum(num_site_i(1 : end - 1)) + 1];
            donnor_ind = mcolon(comp_ind, comp_ind + num_donnors_i - 1)';
            sites.type(donnor_ind) = 1;
            model.sites = sites;
            model.num_site_i = num_site_i;
            
           % We use mixing rules from reference 5, eqs 2 and 3
            kappa_ij   = zeros(numAss, numAss);
            epsilon_ij = zeros(numAss, numAss);
            for i = 1 : numAss
                sigma1   = sigma(sites.components(i));
                kappa1   = sites.kappa(i);
                epsilon1 = sites.epsilon(i);
                type1    = sites.type(i);
                for j = 1 : numAss
                    sigma2   = sigma(sites.components(j));
                    kappa2   = sites.kappa(j);
                    epsilon2 = sites.epsilon(j);
                    type2    = sites.type(j);
                    if type1 ~= type2
                        kappa_ij(i, j) = sqrt(kappa1*kappa2)*(((sqrt(sigma1*sigma2))/((sigma1+sigma2)/2))^3);
                        epsilon_ij(i, j) = (epsilon1 + epsilon2)/2;
                    end
                end
            end
   

            model.kappaAss_ij = kappa_ij;
            model.epsilonAss_ij = epsilon_ij;
            
            %model.kij = Binar;
            
            model.amodconsts = Cons(:, 1 : 3)';
            model.bmodconsts = Cons(:, 4 : 6)';
            
            sigmaij   = zeros(numC, numC); 
            epsilonij = zeros(numC, numC); 
            for i = 1 : numC
                for j = 1 : numC
                    sigmaij(i, j) = 0.5*(sigma(i) + sigma(j)); % see A.14 of ref1
                    if (CN(i) == 0) || (CN(j) == 0) % The ions do not have dispersion with each other.
                        epsilonij(i, j) = sqrt(epsilon(i)*epsilon(j))*(1 - Binar(i, j)); % see A.15 of ref1     	
                    end
                end
            end
            
            model.sigma     = sigma;
            model.epsilon   = epsilon;
            model.sigmaij   = sigmaij;
            model.epsilonij = epsilonij;
            model.CN        = CN;
            
            model.TC        = TC';
            model.PC        = PC';
            model.omega     = omega';
            
            % for EquationOfStateModel compatibility (see getReducedPT member function there)
            model.Tcrit = TC';
            model.Pcrit = PC';
            model.acentricFactors = omega'; 
            model.VC = VC;

            %% other constants
            model.kb = 1.3806504e-23; %BOLTZMAN Constant J/K;
            model.e  = 1.60217662e-19; % in SI
            % polar data
            model.dipMo = dipMo;
            model.dipNum = dipNum;
        end
        

        function [fn, index] = getVariableField(model, name, varargin)
        % Map variables to state field.
            
            switch(lower(name))
              case {'t', 'temperature'}
                fn = 'T';
                index = ':';
              case {'x', 'components'}
                % Overall mole fraction
                fn = 'components';
                index = ':'; 
              case {'reduceddensity'}
                % Reduced density
                fn = 'reducedDensity';
                index = ':';
              otherwise
                % This will throw an error for us
                [fn, index] = getVariableField@PhysicalModel(model, name, varargin{:});
            end
        end
        
        function model = setupStateFunctionGroupings(model, varargin)
            if isempty(model.PCSaftPropertyFunctions)
                dispif(model.verbose, 'Setting up PCSaftPropertyFunctions...');
                model.PCSaftPropertyFunctions = PCSaftPropertyFunctions(model); %#ok
                dispif(model.verbose, ' Ok.\n');
            end
        end
        
        function model = removeStateFunctionGroupings(model)
            model.PCSaftPropertyFunctions = [];
        end
        
                      
        function numC = getNumberOfComponents(model)
            numC = model.numC;
        end
        
        
        function containers = getStateFunctionGroupings(model)
            containers = getStateFunctionGroupings@PhysicalModel(model);
            extra = {model.PCSaftPropertyFunctions};
            extra = extra(~cellfun(@isempty, extra));
            containers = [containers, extra];
        end
        
        function indlin = getInd(model, i, k)
            numC   = model.numC;
            indlin = (i - 1)*numC + k;
        end
         
    end

end

%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}

