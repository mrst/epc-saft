classdef GenericEquationOfStateModel < PhysicalModel
%
%
% SYNOPSIS:
%   GenericEquationOfStateModel(fluid, nph)
%
% DESCRIPTION:
%
% PARAMETERS:
%   fluid - 
%   nph   - 
%
% RETURNS:
%   class instance
%
% EXAMPLE:
%
% SEE ALSO:
%


% Generic equation of state model. 
        
    properties
        CompositionalMixture % CompositionalMixture
        selectGibbsMinimum = true; % Use minimum Gibbs energy to select Z
        minimumComposition = 1e-8; % Minimum composition value (for numerical stability)
        minimumSaturation  = 1e-8; % Minimum total EOS phase saturation 
        useNewton = false;
        numberOfPhases
    end

    methods
        
        function model = GenericEquationOfStateModel(fluid, nph)
            model = model@PhysicalModel([]);
            model.CompositionalMixture = fluid;
            model.nonlinearTolerance = 1e-4;
            model.numberOfPhases = nph;
        end

        function n = getNumberOfPhases(model)
            n = model.numberOfPhases;
        end
        
        function n = getReferenceKIndex(model)
        % The default reference phase for the computation of the coefficient K is the first phase
            n = 1;
        end
        
        function names = getPhaseNames(model)
        % default names are phase index
            nph = model.getNumberOfPhases();
            names = cellfun(@(i) sprintf('%d', i), (1 : nph)', 'uniformoutput', false);            
        end
        
        function names = getSaturationsNames(model)
            phasenames = model.getPhaseNames();
            names = cellfun(@(str) sprintf('s_%s', str), phasenames, 'uniformoutput', false);
        end
        
        function names = getKNames(model)
        % default name assignment (numbered)
            phasenames = model.getPhaseNames();
            names = cellfun(@(str) sprintf('K_%s', str), phasenames, 'uniformoutput', false);
        end
        
        function names = getComponentKNames(model)
        % default name assignment (numbered)
            Knames    = model.getKNames();
            nph       = model.getNumberOfPhases;
            compnames = model.getComponentNames;
            ncomp     = model.getNumberOfComponents;
               
            names = cell(nph*ncomp, 1);
            for  iph = 1 : nph
                Kname = Knames{iph};
                for  icomp = 1 : ncomp
                    compname = compnames{icomp};
                    iall = model.getPhaseCompLinearIndex(iph, icomp);
                    names{iall} = sprintf('%s_%s', Kname, compname);
                end
            end
            
        end
        
        function names = getZNames(model)
        % default name assignment (numbered)
            phasenames = model.getPhaseNames();
            names = cellfun(@(str) sprintf('Z_%s', str), phasenames, 'uniformoutput', false);
        end
        
        function names = getRhoNames(model)
        % default name assignment (numbered)
            phasenames = model.getPhaseNames();
            names = cellfun(@(str) sprintf('reducedDensity_%s', str), phasenames, 'uniformoutput', false);
        end
        
        function n = getNumberOfComponents(model)
            n = model.CompositionalMixture.getNumberOfComponents();
        end

        function names = getComponentNames(model)
            names = model.CompositionalMixture.names;
        end
        
        function names = getPhaseMolarFractionNames(model)
        % Names for the vector of compositions for each phase
        % Default assignment
            phasenames = model.getPhaseNames;
            nph        = model.getNumberOfPhases;
            names = cell(nph, 1);
            for  iph = 1 : nph
                phasename = phasenames{iph};
                names{iph} = sprintf('X_%s', phasename);
            end
        end
        
        function [iph, icomp] = getPhaseCompIndex(model, iall)
            nph = model.getNumberOfPhases;
            ncomp = model.getNumberOfComponents;
            iph = floor((iall - 1)/ncomp) + 1;
            icomp = iall - (iph - 1)*ncomp;             
        end
        
        function iall = getPhaseCompLinearIndex(model, iph, icomp)
            ncomp = model.getNumberOfComponents;
            iall = ncomp*(iph - 1) + icomp;
        end
        
        
        function names = getAllPhaseMolarFractionNames(model)
        % Names for all the compositions (one per component-phase pair)
            phasecompnames = model.getPhaseMolarFractionNames();
            nph            = model.getNumberOfPhases;
            compnames      = model.getComponentNames;
            ncomp          = model.getNumberOfComponents;
            
            names = cell(nph*ncomp, 1);
            for  iph = 1 : nph
                phasecompname = phasecompnames{iph};
                for  icomp = 1 : ncomp
                    compname = compnames{icomp};
                    iall = model.getPhaseCompLinearIndex(iph, icomp);
                    names{iall} = sprintf('%s_%s', phasecompname, compname);
                end
            end
        end
        
        function [state, report] = stepFunction(model, state, state0, dt, drivingForces, linsolve, nonlinsolve, iteration, varargin) %#ok
        % implement one step for the flash solver (Substitution or Newton) or RachforRice
        % no generic implementation is given in this base class
        end

        function state = validateState(model, state)
            n_cell = size(state.pressure, 1);
            nph = model.getNumberOfPhases;
            ncomp = model.getNumberOfComponents;
            
            % Check assignment of saturations
            if ~isfield(state, 's') 
                % Add empty that we do not know. The flash routine will then perform the stability test for all cells.
                state.s = nan(n_cell, nph);
            else
                assert(all(size(state.s) == [n_cell, nph]), 'size for saturation variable does not match');
            end
            
            % Check assignment of K constants
            Knames = model.getKNames;
            for iph = 1 : nph
                name = Knames{iph};
                if ~isfield(state, name)
                    if iph == model.getReferenceKIndex
                        state.(name) = ones(n_cell, ncomp);
                    else
                        state.(name) = zeros(n_cell, ncomp);
                    end
                else
                    assert(all(size(state.(name)) == [n_cell, ncomp]), 'size for K variable does not match');
                end
            end

            % Check assignment phase molar fractions
            phasemolarfractionnames = model.getPhaseMolarFractionNames;
            for iph = 1 : nph
                name = phasemolarfractionnames{iph};
                if ~isfield(state, name)
                    state.(name) = state.components;
                else
                    assert(all(size(state.(name)) == [n_cell, ncomp]), 'size for molar phase composition variable does not match');
                end
            end
            
            % Check assignment compressibility factor
            if ~isfield(state, 'Z')
                state.Z = ones(n_cell, nph);
            else
                assert(all(size(state.Z) == [n_cell, nph]), 'size for molar phase composition variable does not match');
            end
            
            
            % Check assignment compressibility factor
            if ~isfield(state, 'reducedDensity')
                state.reducedDensity = nan(n_cell, nph);
            else
                assert(all(size(state.reducedDensity) == [n_cell, nph]), 'size for molar phase composition variable does not match');
            end
                 
            % Check assignment of temperature
            if ~isfield(state, 'T') 
                % Add empty that we do not know. The flash routine will then perform the stability test for all cells.
                state.s = nan(n_cell, nph);
            else
                assert(all(size(state.T) == [n_cell, 1]), 'size for temperature variable does not match');
            end

        end
        
        function frac = getMoleFraction(model, massfraction)
            % Convert mass fraction to molar fraction
        end
        
        function frac = getMassFraction(model, molfraction)
            % Convert molar fraction to mass fraction
        end

        function [state, report] = updateAfterConvergence(model, state0, state, dt, drivingForces)
            
            error('not updated yet')
            
            [state, report] = updateAfterConvergence@PhysicalModel(model, state0, state, dt, drivingForces);
            changed = false;
            state = model.setFlag(state);
            if ~isfield(state, 'x')
                state.x = model.computeLiquid(state);
                changed = true;
            end
            if ~isfield(state, 'y')
                state.y = model.computeVapor(state);
                changed = true;
            end
            if changed
                singlePhase = state.L == 1 | state.L == 0;
                z_1ph = state.components(singlePhase, :);
                state.x(singlePhase, :) = z_1ph;
                state.y(singlePhase, :) = z_1ph;
            end
        end
        
        function [Pr, Tr] = getReducedPT(model, P, T, useCell)
            if nargin < 4
                useCell = true;
            end
            Tc = model.CompositionalMixture.Tcrit;
            Pc = model.CompositionalMixture.Pcrit;
            if useCell
                n = model.CompositionalMixture.getNumberOfComponents();
                Tr = cell(1, n);
                Pr = cell(1, n);
                
                Ti = 1./Tc;
                Pi = 1./Pc;
                for i = 1:n
                    Tr{i} = T.*Ti(i);
                    Pr{i} = P.*Pi(i);
                end
            else
                Tr = bsxfun(@rdivide, T, Tc);
                Pr = bsxfun(@rdivide, P, Pc);
            end
        end

        
        function state = setFlag(model, state)
        % set flag per phase
        end
        
        function flag = getFlag(model, state)
        % get flag per phase 
        end

        function flagval = parsePhaseFlag(model, flagstr)
            nph = model.getNumberOfPhases;
            phasenames = model.getPhaseNames;
            flagval = 0;
            for i = 1 : nph
                phasename = phasenames{i};
                if ismember(phasename, flagstr)
                    flagval = flagval + 2^(i - 1);
                end
            end
        end
        
        function [fn, index] = getVariableField(model, name, varargin)
            
            varfound = false;
            failed = false;
            
            while ~varfound

                if ismember(name, {'p', 'pressure'})
                    varfound = true;
                    fn = 'pressure';
                    index = ':';
                    break
                end
                
                
                if ismember(name, {'T', 'temperature'})
                    varfound = true;
                    fn = 'T';
                    index = ':';
                    break
                end
                
                if ismember(name, {'components', 'z'})
                    varfound = true;
                    fn = 'components';
                    index = ':';
                    break
                end
                
                names = model.getComponentNames();
                ind = strcmpi(name, names);
                if any(ind)
                    varfound = true;
                    fn = 'components';
                    index = find(ind);
                    break
                end

                names = getSaturationsNames(model);
                ind = strcmpi(name, names);
                if any(ind)
                    varfound = true;
                    fn = 's';
                    index = find(ind);
                    break
                end
                
                names = getKNames(model);
                if ismember(name, names)
                    varfound = true;
                    fn = name;
                    index = ':';
                    break
                end
                
                names = getComponentKNames(model);
                ismember(name, names);
                if any(ind)
                    varfound = true;
                    index = find(ind);
                    [phaseind, compind] = model.getPhaseCompIndex(index);
                    Knames = model.getKNames();
                    fn = Knames{phaseind};
                    index = compind;
                    break
                end
                                
                if strcmpi(name, 'Z')
                    varfound = true;
                    fn = 'Z';
                    index = ':';
                    break
                end
                
                
                names = getZNames(model);
                ind = strcmpi(name, names);
                if any(ind)
                    varfound = true;
                    fn = 'Z';
                    index = find(ind);
                    break
                end
                
                
                
                if strcmpi(name, 'reducedDensity')                
                    varfound = true;
                    fn = 'reducedDensity';
                    index = ':';
                    break
                end
                
                names = getRhoNames(model);
                ind = strcmpi(name, names);
                if any(ind)
                    varfound = true;
                    fn = 'reducedDensity';
                    index = find(ind);
                    break
                end

                names = getPhaseMolarFractionNames(model);
                if ismember(name, names)
                    varfound = true;
                    fn = name;
                    index = ':';
                    break
                end
                
                names = getAllPhaseMolarFractionNames(model);
                ind = strcmpi(name, names);
                if any(ind)
                    varfound = true;
                    index = find(ind);
                    [phaseind, compind] = model.getPhaseCompIndex(index);
                    phasemolarfractionnames = model.getPhaseMolarFractionNames();
                    fn = phasemolarfractionnames{phaseind};
                    index = compind;
                    break
                end
                
                varfound = true; % to exit the loop
                failed   = true;    
                
            end
            
            if failed
                [fn, index] = deal([]);
            end
            
        end
        
        
    end

end

%{
Copyright 2009-2020 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}

    

