classdef DerAssocFracDrhoModel < AssocFracModel
% "{Chapman, Walter G., et al. "New reference equation of state for associating liquids.
%  Industrial & engineering chemistry research 29.8 (1990): 1709-1721.}"
%
% SYNOPSIS:
%   DerAssocFracDrhoModel(assocFracModel, varargin)
%
% DESCRIPTION:
%   model to get the derivative wrt to rho of the  Association term of mixtures; 
%   see more details in  Eq. 22 in the above reference  
% PARAMETERS:
%   assocFracModel - Association term of mixtures 
%   varargin       - optional
%
% RETURNS:
%   class instance
%
% EXAMPLE:
%
% SEE ALSO:
%   AssocFracModel
%

    properties
        derAssocNames
    end
    
    methods
        
        function model = DerAssocFracDrhoModel(assocFracModel, varargin)
            
            dens   = assocFracModel.dens;
            delta  = assocFracModel.delta;
            ddelta_rho = assocFracModel.ddelta_rho;
            ddelta_xi = assocFracModel.ddelta_xi;

            x = assocFracModel.x;
            pcsaftpropmodel = assocFracModel.pcsaftpropmodel;
            
            model = model@AssocFracModel(x, dens, delta, ddelta_rho, ddelta_xi, pcsaftpropmodel);
            
            numAss = model.pcsaftpropmodel.numAss;
            index = (1 : numAss)';
            model.derAssocNames = arrayfun(@(x) sprintf('derAssocFrac_%d', x), ...
                                          index, 'uniformoutput', false);
                                              
           model.AutoDiffBackend = DiagonalAutoDiffBackend('useMex', true, 'rowmajor', false);

           model.nonLinearMaxIterations = 10;
            
        end

        function [vars, names, origin] = getPrimaryVariables(model, state)
        % Get primary variables from state
            names = model.derAssocNames;
            
            nvars = numel(names);
            vars = cell(1, nvars);
            [vars{:}] = model.getProps(state, names{:});
            
            origin = cell(1, nvars);
            [origin{:}] = deal(class(model));
        end

        
        function [eqs, names, types, state] = getModelEquations(model, state0, state, dt, drivingForces)

            Xa = model.getProp(state, 'assocFrac');
            % We convert to cells
            sz = size(Xa);
            Xa = mat2cell(Xa, sz(1), ones(sz(2), 1));
            
            eqs = equationsDerAssocFracDrho(state, Xa, model, 'scalarinput', true);            
            
            pVars = model.derAssocNames;
            
            names = cellfun(@(str) ['eqs_', str], pVars, 'uniformoutput', ...
                            false)';
            types = repmat({'cells'}, 1, numel(pVars));
            
            
        end
                
        function [state, failure, report] = solveDerAssocFrac(model, state)
            
            solver = NonLinearSolver();

            solver.maxIterations = model.nonLinearMaxIterations;
            
            pcmodel = model.pcsaftpropmodel;
            numAss  = pcmodel.numAss;
            dens    = model.dens;
            if isa(dens, 'ADI')
                ncells = size(dens.value, 1);
            else
                ncells  = size(dens, 1);
            end
            
            inputstate = model.setProp(state, 'derAssocFrac', zeros(ncells, numAss));

            inputstate0   = inputstate;
            dt            = 0; % dummy timestep
            drivingForces = []; % drivingForces;

            [state, failure, report] = solveMinistep(solver, model, inputstate, ...
                                                     inputstate0, dt, ...
                                                     drivingForces);
            
            converged = report.Converged;
            if ~converged
                nlreport = report.NonlinearReport{end};
                failure  = nlreport.Failure;
                if failure
                    msg = ['Model step resulted in failure state. Reason: ', ...
                           nlreport.FailureMsg]; %#ok<AGROW>
                    error(msg);
                end
            end
            
        end
        
        function [fn, index] = getVariableField(model, name, varargin)

            [fn, index] = getVariableField@AssocFracModel(model, name);
            
            if ~isempty(index)
                return
            else
                varfound = false;
                failed   = false;
            end
            
            while ~varfound

                if strcmpi(name, 'derAssocFrac')
                    varfound = true;
                    fn = 'derAssocFrac';
                    index = ':';
                    break
                end

                ind = strcmpi(name, model.derAssocNames);
                if any(ind)
                    varfound = true;
                    fn = 'derAssocFrac';
                    index = find(ind);
                    break
                end

                varfound = true; % to exit the loop
                failed   = true;
            end
            
            if failed
                [fn, index] = deal([]);
            end
            
        end        
    end
end

%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
