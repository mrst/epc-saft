function [phi, f, Z, redrho, itDens] = getCompressibilityAndFugacity(model, redrho, P, T, comp, fluidtype, tol)

%
%
% SYNOPSIS:
%   function [phi, f, Z, redrho, itDens] = getCompressibilityAndFugacity(model, redrho, P, T, comp, fluidtype, tol)
%
% DESCRIPTION:
%
% PARAMETERS:
%   model     - a density model built with epc-saft EoS
%   redrho    - initialization
%   P         - pressure
%   T         - temperature
%   comp      - composition
%   fluidtype - liquid or vapor or solid
%   tol       - tolerance for the Nonlinear solver
%
% RETURNS:
%   phi    - fugacity coefficient
%   f      - fugacity
%   Z      - compressibility factor
%   redrho - computed reduced density
%   itDens - iterations number for reporting
%
% EXAMPLE:
%
% SEE ALSO:
%   CompressibilityFactor 

    
% Do not handle AD for the moment                                    
    
    pcsaftmodel = model.CompositionalMixture; 
    % Temporarly state
    state.components = comp;
    state.pressure = P;
    state.T = T;
    state.reducedDensity = redrho;            
    % Choose FluidType
    state.FluidType = fluidtype;
    % Choose tolerance
   % if ~isempty(tol)               
    %    model.densitymodel.nonlinearTolerance = tol;
   % end  
    pcstate = model.densitymodel.getProp(state, 'initializePcsaftState');
    Z      = pcsaftmodel.getProp(pcstate, 'CompressibilityFactor');
    f      = pcsaftmodel.getProp(pcstate, 'Fugacity');
    phi    = pcsaftmodel.getProp(pcstate, 'FugacityCoefficient');            
    redrho = pcsaftmodel.getProp(pcstate, 'reducedDensity');
    
    
    % Cumulated Nonlinear iterations
    itDens = pcstate.itDens;
    
    f   = [f{:}];
    phi = [phi{:}]; 
    
    
    % saf-guard against vanishing fugacity
    zz = (comp==0);
    phi(zz) = 1.0e-16;            
    f(zz)   = 1.0e-16;
    
end

%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}