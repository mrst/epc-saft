function [y, sumy] = computeVapor(HL, V, Kl, Kv, z)

%
%
% SYNOPSIS:
%   function [y, sumy] = computeVapor(HL, V, Kl, Kv, z)
%
% DESCRIPTION:
%
% PARAMETERS:
%   HL - 
%   V  - 
%   Kl - 
%   Kv - 
%   z  - 
%
% RETURNS:
%   y    - 
%   sumy - 
%
% EXAMPLE:
%
% SEE ALSO:
%

% return vapor (y) and salt (s) compositions. 
    if isstruct(HL)
        [y, sumy] = model.computeVapor(HL.HL, HL.V, HL.Kl, HL.Kv, HL.components); 
        return
    end
    
    if iscell(z)
        y = z; 
        sumy = 0; 
        
        for i = 1:numel(z)
            bad =~isfinite(Kv{i}) & ~isfinite(Kl{i}); 
            y{i} = Kv{i}.*z{i}./(1 + V.*(Kv{i} - 1) + HL.*(Kl{i} - 1)); 
            y{i}(bad) = 0; 
            sumy = sumy + double(y{i}); 
        end
        
        y = cellfun(@(kv, kl, zi) kv.*zi./(1 + HL.*(kl - 1) + V.*(kv - 1)), Kv, Kl, z, 'UniformOutput', false); 
        y = cellfun(@(x) x./sumy, y, 'UniformOutput', false); 
        assert(all(cellfun(@(x) all(isfinite(double(x))), y))); 
        
    else
        y = Kv.*z./(1 + (bsxfun(@times, V, (Kv - 1))) + (bsxfun(@times, (Kl - 1), HL))); 
        y(~isfinite(Kv)) = z(~isfinite(Kv)); 
        y = bsxfun(@rdivide, y, sum(y, 2)); 
        sumy = sum(y, 2); 
        assert(all(isfinite(y(:)))); 
    end
end