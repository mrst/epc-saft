function stateC = convertToCell(state)

%
%
% SYNOPSIS:
%   function stateC = convertToCell(state)
%
% DESCRIPTION:
%
% PARAMETERS:
%   state - 
%
% RETURNS:
%   stateC - 
%
% EXAMPLE:
%
% SEE ALSO:
%

    if iscell(state)
        stateC = state;
        return
    elseif isstruct(state)
        fds = fieldnames(state);
        for i = 1 : numel(fds)
            fd = fds{i};
            sz = size(state.(fd));
            if sz(2) > 1
                stateC.(fd) = mat2cell(state.(fd), sz(1), ones(sz(2), 1));
            else
                stateC.(fd) = state.(fd);
            end
        end
        return
    elseif isnumeric(state)
        sz = size(state);
        if sz(2) > 1
            stateC = mat2cell(state, sz(1), ones(sz(2), 1));
        else
            stateC = state;
        end
        return
    else
        error('input for convertToCell not recognized');
    end
    
end
