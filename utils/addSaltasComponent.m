function [zsalt, X0salt, Y0salt, S0salt, Kv0salt, Kl0salt, nl20] = addSaltasComponent(model, z,x0,y0,nl20, SaltInfo)

%
%
% SYNOPSIS:
%   function [zsalt,X0salt,Y0salt,S0salt, Kv0salt,Kl0salt,nl20] = addSaltasComponent(model, z,x0,y0,nl20, SaltInfo)
%
% DESCRIPTION: treat the salt (NaCl) as a component 
%
% PARAMETERS:
%   model    - VLS epcsaft model
%   z        - Composition for   [H2O CO2 Na Cl]
%   x0       - Liquid composition
%   y0       - Vapor composition
%   nl20     - Solid saturation
%   SaltInfo - Infor for the salt
%
% RETURNS:
%   zsalt   - New composition for   [H2O CO2 NaCl]
%   X0salt  - New liquid-composition
%   Y0salt  - New vapor-composition
%   S0salt  - New solid-omposition
%   Kv0salt - New K_V for the salt
%   Kl0salt - New K_L for the salt
%   nl20    - New solid saturation
%
% EXAMPLE:
%
% SEE ALSO: calculatePcsaftSaltFugacity
%

    %% We need to treat salt as a component
    Na = find(strcmp(model.densitymodel.pcsaftmodel.names, 'Na'));
    Cl = find(strcmp(model.densitymodel.pcsaftmodel.names, 'Cl'));
    NaCl = min(Na,Cl);
    numC = model.densitymodel.pcsaftmodel.numC;
    nc = numel(nl20);
    % Fugacity of  salt in liquid phase 
    fs_W     = SaltInfo.fs_W;
    % Fugacity of solid salt 
    f_S      = SaltInfo.fS;
    %% Composition for   [H2O CO2 NaCl]
    zsalt        = z(:, 1 : NaCl); 
    zsalt(:, NaCl)  = z(:, Na) + z(:, Cl); 
    X0salt       = x0(:, 1 : NaCl); 
    X0salt(:, NaCl) = x0(:, Na) + x0(:, Cl); 
    Y0salt       = y0(:, 1 : NaCl); 
    Y0salt(:, NaCl) = y0(:, Na) + y0(:, Cl); 
    vec = zeros(1,NaCl);
    vec(1,NaCl) = 1;
    S0salt = repmat(vec, nc, 1);               
    Kv0salt = Y0salt./X0salt;
    Kl0salt = S0salt./X0salt.*fs_W./f_S;                
    nl20(nl20 == 0|isnan(nl20)) = 0.0001;              
end