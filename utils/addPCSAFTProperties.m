function model = addPCSAFTProperties(model, varargin)

%
%
% SYNOPSIS:
%   function model = addPCSAFTProperties(model, varargin)
%
% DESCRIPTION:
%
% PARAMETERS:
%   model    - 
%   varargin - 
%
% RETURNS:
%   model - 
%
% EXAMPLE:
%
% SEE ALSO:
%

    switch casename
      case 'OnlyAssoc'
        [numC, Properties, Binar, Cons, numAsm, numAss, Ass, Tsite, numions] ...
            = inputscheckassociation();
        CN = Properties(:, 11);
      case 'WithIon'
        [numC, Properties, Binar, Cons, numAsm, numAss, Ass, Tsite, numions] ...
            = inputscheckion();
        CN = Properties(:, 12);
        
      otherwise
        error('casename not recognized');
        
    end
    
    model.numAss = numAss;
    model.numC   = numC;
    fns = {'m','sigma','epsilon'};
    for i=3:numel(fns)
        model.(fns{i-2})=Properties(:,i);
    end

    kappaAss   = Properties(:, 6);
    epsilonAss = Properties(:, 7);
    num_site_i      = Properties(:, 8);
    num_donnors_i   = Properties(:, 9);
    num_acceptors_i = Properties(:, 10);
    
    sites.components = rldecode((1 : numC)', num_site_i);
    sites.kappa      = rldecode(kappaAss, num_site_i);
    sites.epsilon    = rldecode(epsilonAss, num_site_i);
    sites.type       = zeros(numel(sites.components), 1);
    comp_ind = [1; cumsum(num_site_i(1 : end - 1)) + 1];
    donnor_ind = mcolon(comp_ind, comp_ind + num_donnors_i - 1)';
    sites.type(donnor_ind) = 1;
    model.sites = sites;
    model.num_site_i = num_site_i;
    
    % We use mixing rules from reference 5, eqs 2 and 3
    kappa_ij   = zeros(numAss, numAss);
    epsilon_ij = zeros(numAss, numAss);
    for i = 1 : numAss
        sigma1   = sigma(sites.components(i));
        kappa1   = sites.kappa(i);
        epsilon1 = sites.epsilon(i);
        type1    = sites.type(i);
        for j = 1 : numAss
            sigma2   = sigma(sites.components(j));
            kappa2   = sites.kappa(j);
            epsilon2 = sites.epsilon(j);
            type2    = sites.type(j);
            if type1 ~= type2
                kappa_ij(i, j)   = sqrt(kappa1*kappa2)*(((sqrt(sigma1*sigma2))/((sigma1+sigma2)/2))^3);
                epsilon_ij(i, j) = (epsilon1 + epsilon2)/2;
            end
        end
    end
    model.kappaAss_ij   = kappa_ij;
    model.epsilonAss_ij = epsilon_ij;
    
    model.kij = Binar;
    
    model.amodconsts = Cons(:, 1 : 3)';
    model.bmodconsts = Cons(:, 4 : 6)';
    

    sigmaij   = zeros(numC, numC); 
    epsilonij = zeros(numC, numC); 
    for i = 1 : numC
        for j = 1 : numC
            sigmaij(i, j) = 0.5*(sigma(i) + sigma(j)); % see A.14 of ref1
            epsilonij(i, j) = sqrt(epsilon(i)*epsilon(j)) * (1 - Binar(i, j)); % see A.15 of ref1     	
        end
    end
    model.sigmaij   = sigmaij;
    model.epsilonij = epsilonij;
    model.CN        = CN;
    model = model.setDiameters();
    model = model.setIonModelCsts();
    
    %% other constants
    model.kb = 1.3806504e-23; %BOLTZMAN Constant J/K;
    model.e  = 1.60217662e-19; % in SI  
end