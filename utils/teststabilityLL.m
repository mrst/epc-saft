function [s, Y, Nph, redrho_L, redrho_S, iteration, itDens_L, itDens_S] = teststabilityLL(model, redrho_L, redrho_S, P, T, comp, x, y, nv, nl1, Nph)
    
    %% Liquid-Liquid Stability Analysis with QNNC Method 
    % ref. Nghiem an Li 1984, COMPUTATION OF MULTIPIPHASE EQUILIBRIUM PHENOMENA WITH AN EQUATION OF STATE        
    numC  = model.getNumberOfComponents;    
    N     = numel(P);    
    isL = (Nph == model.parsePhaseFlag({'L'}));
    z(isL, :) = comp(isL, :);     
    z(~isL,:) = x(~isL,:);
    zeta = 1; ones(N,1);      
    %% We inject the previous reduced density to initialize
    %% density calculation
    [phil, ~, ~, redrho_L, itDens_L] = getCompressibilityAndFugacity(model, redrho_L, P, T, z, 'Liquid',[]);    
    Hres = log(z) + log(phil); 
    %% The initial Guess procedure 
    Light = [];
    Heav  = numC; % Asphaltene
    [Y, s]  = InitiatePhaseComposition(model, P, T ,x , y, z, Hres, Light, Heav);    
    
    %% Preparing the QNNC iter.
    [phiy, ~, ~, redrho_S, itDens_S] = getCompressibilityAndFugacity(model, redrho_S, P, T, s, 'Solid',[]);
    r     = log(Y) + log(phiy) - Hres; 
    lnY   = log(Y) - zeta.*r; %%% Eq.15
    Y     = exp(lnY); 
    Y_old = Y; 
    r_old = r; 
    %% Params for Reduction procedure
    Ycnv = Y;
    redrho_S_cnv = redrho_S;    
    active = true(size(Y,1),1);     
    itDens_S = itDens_S.*ones(size(Y,1),1);
    itDens_L = itDens_L.*ones(size(Y,1),1);
    %% main 
    iter  = 1;
    error = 1;
    iteration = ones(size(Y,1),1);     
    PrintConv = false;
    tol =10^-4;
    while any(error > 10^-12)
	        
        if PrintConv 
            fprintf('iter %d\n', iter);
        end
        s = Y(active,:)./sum(Y(active,:),2); 
        [phiy,~,~, redrho_S,iterDens_S]  = getCompressibilityAndFugacity(model, redrho_S(active,:), P(active,:), T(active,:), s(active,:), 'Liquid',[]); 
        r = log(Y(active,:)) + log(phiy) - Hres(active,:); 		
        dlnY = -zeta.* r_old(active,:); 
        dr = r - r_old(active,:); 
        normdr = norm(dr); 
        normr_old = norm(r_old); 
        normdlnY = norm(dlnY); 
        zeta = zeta.*(normr_old.*normdlnY)./(normdr.*normdlnY); %%% eq. 16
        lnY = lnY(active,:) - zeta.*r; 
        r_old = r; 
        Y = exp (lnY); 	
        error = sum((1 - Y_old(active,:)./Y).^2,2); 
        tol = max(mean(sqrt(error)),10^-2);
        conv =  error < 10^-12;
        Ycnv(active,:) = Y(active,:);
        iteration(active,:) = iter;
        itDens_S(active,:) =  itDens_S(active,:) + iterDens_S;
        redrho_S_cnv(active,:) = redrho_S(active,:);
        active = active(~conv);
        Y_old = Y; 
        iter = iter + 1;
    end
    
    % Get Y and redrho_S at convergence
    Y = Ycnv;
    redrho_S = redrho_S_cnv;
    s = Y./sum(Y,2);

    %% Convergence analysis
    [s, Nph] = CheckSatbilityAnalysis(model,x,y, s, z, Y, Nph, nl1, nv);    
    
end



function [Y, s] = InitiatePhaseComposition(model, P,T,x,y,z, h, Light, Heav)
   
%%  We select an initial guess that results in higher percent of Asphalt 
%%  from five options: 
pcsaftmodel = model.CompositionalMixture;   
numC  = model.getNumberOfComponents;     
TC    = pcsaftmodel.TC;
PC    = pcsaftmodel.PC;
omega = pcsaftmodel.omega; 
N     = numel(P); 
Yguess = cell(5,1); 
Kges   = (PC./P.*exp(5.37.*(1 + omega).*(1 - TC./T))); % Wilson's equation
%% Option 1: Lightest and Heaviest components as pure phases
Yguess{1} = zeros(N,numC);
Yguess{1}(:, [Light, Heav])  = 0.999; 
nLH = length([Light, Heav]);
rest = setdiff((1:numC), [Light,Heav]);
Yguess{1}(:, rest) = 0.001/(numC - nLH); 

%% Option 2: With Wilson's correlation
Yguess{2} = Kges.*z;     
%% Option 3: zero composition 
Yguess{3} = zeros(N,numC);
%% Option 4: Arithmetic mean from existing composition
Yguess{4} = 0.5.*(x + y);     
%% Option 5: An ideal gaz mixture  
Yguess{5} = exp(h);    
%% We select the way (guess) that results in higher percent of Asphalt 
YG = zeros(5,N);
for i = 1:5     
    YG(i,:) = Yguess{i}(:, numC);     
end
[~, I] = max(YG);   
Y = Yguess{I};

% Trial composition 
s     = Y./sum(Y,2); 
end

function [s, Nph] = CheckSatbilityAnalysis(model,x,y, s, z,Y, Nph, nl1, nv)

numC  = model.getNumberOfComponents;    
%% Check 1: Convergence to overall trivial composition  
f1 = sum(log(s./z).^2,2)/numC;     
%% Check 2: Convergence to Liquid composition
Liquid = (nl1 ~= 0);
f2(Liquid) = sum(log(s(Liquid,:)./x(Liquid,:)).^2,2)/numC;  
f2(~Liquid) =  20;     
%% Check 3: Convergence to Vapour composition  
Vapor = (nv ~= 0);     
f3(Vapor) = sum(log(s(Vapor,:)./y(Vapor,:)).^2,2)/numC; 
f3(~Vapor) =  20;     
%% Check 4: Stability check
f4 = sum(Y,2) - 1; 
% shortcut
pfun = @(flagstr) model.parsePhaseFlag(flagstr);    
%% main phase check
 if any(f1 >= 10^-5 & f2' >= 10^-5 &  f3' >= 10^-5 & f4 >= 10^-8) 
     update = f1 >= 10^-5 & f2' >= 10^-5 &  f3' >= 10^-5 & f4 >= 10^-8;    
     if any(Nph(update) == pfun({'L'}))    
         converged = (Nph == pfun({'L'}));    % Liquid        
         Nph(converged&update) = pfun({'L', 'HL'});  % Liquid-Heavy Liquid         
     end        
     if any(Nph(update) == pfun({'V', 'L'}))
         converged = (Nph == pfun({'V', 'L'}));      % Gas-Liquid        
         Nph(converged&update) = pfun({'V', 'L', 'HL'}); % Gas-Liquid-Liquid-Heavy Liquid        
     end    
     s(~update,:) = zeros(nnz(~update), numC);    
 end

end

%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}