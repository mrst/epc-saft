function eqs = equationsDerAssocFracDxi(state, Xa, model, varargin)

%
%
% SYNOPSIS:
%   function eqs = equationsDerAssocFracDxi(state, Xa, model, varargin)
%
% DESCRIPTION:
%
% PARAMETERS:
%   state    - state for the derivative (with respect to composition) 
%              model from assocFracmodel
%   Xa       - association variable Xa
%   model    - Derivative with respect to composition of the association model
%   varargin - 
%
% RETURNS:
%   eqs - 
%
% EXAMPLE:
%
% SEE ALSO:
%


    opt = struct('scalarinput', true);
    opt = merge_options(opt, varargin{:});
    
    dens      = model.dens;
    delta     = model.delta;
    ddelta_xi = model.ddelta_xi;
    x         = model.x;
    
    if opt.scalarinput
        dens      = convertToDouble(dens);
        delta     = convertToDouble(delta);
        ddelta_xi = convertToDouble(ddelta_xi);
        x         = convertToDouble(x);
    end
    
    pcmodel = model.pcsaftpropmodel;
    numAss  = pcmodel.numAss;
    numC    = pcmodel.numC;
    sites   = pcmodel.sites;
    
    dXa = model.getProp(state, 'derAssocFrac');
    
    if~iscell(dXa)
        dXa = num2cell(dXa);
    end
    
    eqs = cell(1, numAss*numC);
    
    for k = 1 :numC

        for i = 1 : numAss
            
            indlin = pcmodel.getInd(i, k);            
            s1 = 0*dXa{indlin};
            s2 = 0*dXa{indlin};
            s3 = 0*dXa{indlin};
            
            for j = 1 : numAss
                indlin = pcmodel.getInd(j, k);
                xj = sites.components(j);
                s1 = s1 + dens.*x{xj}.*delta{i, j}.*dXa{indlin};
                s2 = s2 + dens.*x{xj}.*ddelta_xi{i, j, k}.*Xa{j};
                if (xj == k)
                    s3 = s3 + dens.*Xa{j}.*delta{i, j};
                end
            end
    
            indlin = pcmodel.getInd(i, k);
            eqs{indlin} = dXa{indlin} + (Xa{i}.^2).*(s1 + s2 + s3);
            
        end
    end
    
end

%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}

