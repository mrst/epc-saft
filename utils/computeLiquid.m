function [x, sumx] = computeLiquid(HL, V, Kl, Kv, z)

%
%
% SYNOPSIS:
%   function [x, sumx] = computeLiquid(HL, V, Kl, Kv, z)
%
% DESCRIPTION:
%
% PARAMETERS:
%   HL - 
%   V  - 
%   Kl - 
%   Kv - 
%   z  - 
%
% RETURNS:
%   x    - 
%   sumx - 
%
% EXAMPLE:
%
% SEE ALSO:
%

    
    if isstruct(HL)
        x = model.computeLiquid(HL.HL, HL.V, HL.Kl, HL.Kv, HL.components);
        return
    end

    if iscell(z)
        x = z;
        sumx = 0;
        for i = 1:numel(z)
            bad = ~isfinite(Kv{i})& ~isfinite(Kl{i});
            x{i} = z{i}./(1+ V.*(Kv{i}-1)+ HL.*(Kl{i}-1));
            x{i}(bad) = 0;
            sumx = sumx + double(x{i});
        end
        x = cellfun(@(x) x./sumx, x, 'UniformOutput', false);
        assert(all(cellfun(@(x) all(isfinite(double(x))), x)));
    else
        x = z./(1 + (bsxfun(@times,V , (Kv-1)))+(bsxfun(@times,(Kl-1),HL)));
        x(~isfinite(Kv)) = 0;
        x(~isfinite(Kl)) = 0;
        sumx = sum(x, 2);
        x = bsxfun(@rdivide, x, sumx);
        if ~all(isfinite(x(:)))
            z
        end
        assert(all(isfinite(x(:))));
    end
    
    
end