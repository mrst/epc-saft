classdef AssocFracModel < PhysicalModel
% "{Chapman, Walter G., et al. "New reference equation of state for associating liquids.
%  Industrial & engineering chemistry research 29.8 (1990): 1709-1721.}"
%
% SYNOPSIS:
%   AssocFracModel(x, dens, delta, ddelta_rho, ddelta_xi, pcsaftpropmodel, varargin)
%
% DESCRIPTION:
%  Model to solve  Association term of mixtures (Eq. 22of the reference)
% PARAMETERS:
%   x               - composition
%   dens            - density
%   delta           - association strength
%   ddelta_rho      - derivative of association strength with respect to density
%   ddelta_xi       - derivative of association strength with respect to density_i
%   pcsaftpropmodel - epc-saft model
%   varargin        - 
%
% RETURNS:
%   class instance
%
% EXAMPLE:
%
% SEE ALSO:
%  AssociativeHelmoltzEnergy

    properties
        x               
        dens           
        delta           
        ddelta_rho          
        ddelta_xi    
        pcsaftpropmodel 
        assocNames
        nonLinearMaxIterations
    
    end
    
    methods
        function model = AssocFracModel(x, dens, delta, ddelta_rho, ddelta_xi, pcsaftpropmodel, varargin)
            model = model@PhysicalModel([]);
            model.dens   = dens;
            model.delta  = delta;
            model.ddelta_rho = ddelta_rho;
            model.ddelta_xi = ddelta_xi;

            model.x = x;
            
            model.pcsaftpropmodel = pcsaftpropmodel;
            
            numAss = pcsaftpropmodel.numAss;
            index = (1 : numAss)';
            model.assocNames = arrayfun(@(x) sprintf('assocFrac_%d', x), index, 'uniformoutput', false);
                                   
            model.AutoDiffBackend = DiagonalAutoDiffBackend('useMex', true, 'rowmajor', false);

            model.nonLinearMaxIterations = 50;
        end

        function [vars, names, origin] = getPrimaryVariables(model, state)
        % Get primary variables from state
            names = model.assocNames;
            
            nvars = numel(names);
            vars = cell(1, nvars);
            [vars{:}] = model.getProps(state, names{:});
            
            origin = cell(1, nvars);
            [origin{:}] = deal(class(model));
        end

        
        function [eqs, names, types, state] = getModelEquations(model, state0, state, dt, drivingForces)

            
            eqs = equationsAssocFrac(state, model, 'scalarinput', true);
            
            pVars = model.assocNames;
            
            names = cellfun(@(str) ['eqs_', str], pVars, 'uniformoutput', ...
                            false)';
            types = repmat({'cells'}, 1, numel(pVars));
            
            
        end
        
        function [state, failure, report] = solveAssocFrac(model)
            
            solver = NonLinearSolver();
            solver.maxIterations = model.nonLinearMaxIterations;
            
            pcmodel = model.pcsaftpropmodel;
            numAss  = pcmodel.numAss;
            dens    = model.dens;
             if isa(dens, 'ADI')
                ncells = size(dens.value, 1);
            else
                ncells  = size(dens, 1);
            end
            
            inputstate.assocFrac = zeros(ncells, numAss);

            inputstate0   = inputstate;
            dt            = 0; 
            drivingForces = []; 

            [state, failure, report] = solveMinistep(solver, model, inputstate, ...
                                                     inputstate0, dt, ...
                                                     drivingForces);
            
            converged = report.Converged;
            if ~converged
                nlreport = report.NonlinearReport{end};
                failure  = nlreport.Failure;
                if failure
                    msg = ['Model step resulted in failure state. Reason: ', ...
                           nlreport.FailureMsg]; %#ok<AGROW>
                    error(msg);
                end
            end
            
        end

        function [fn, index] = getVariableField(model, name, varargin)
            varfound = false;
            failed   = false;
            
            while ~varfound
                if strcmpi(name, 'assocFrac')
                    varfound = true;
                    fn = 'assocFrac';
                    index = ':';
                    break
                end

                ind = strcmpi(name, model.assocNames);
                if any(ind)
                    varfound = true;
                    fn = 'assocFrac';
                    index = find(ind);
                    break
                end                
                varfound = true;
                failed   = true;
            end
            
            if failed
                [fn, index] = deal([]);
            end
            
        end        
    end
end


%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}