function eqs = equationsDerAssocFrac(state, Xa, model, varargin)

%
%
% SYNOPSIS:
%   function eqs = equationsDerAssocFrac(state, Xa, model, varargin)
%
% DESCRIPTION:
%
% PARAMETERS:
%   state    - 
%   Xa       - 
%   model    - 
%   varargin - 
%
% RETURNS:
%   eqs - 
%
% EXAMPLE:
%
% SEE ALSO:
%


    opt = struct('scalarinput', true);
    opt = merge_options(opt, varargin{:});
    
    dens   = model.dens;
    delta  = model.delta;
    ddelta_rho = model.ddelta_rho;
    x = model.x;
    
    if opt.scalarinput
        dens   = convertToDouble(dens);
        delta  = convertToDouble(delta);
        ddelta_rho = convertToDouble(ddelta_rho);
        x = convertToDouble(x);
    end
    
    pcmodel = model.pcsaftpropmodel;
    numAss = pcmodel.numAss;
    sites  = pcmodel.sites;
    
    dXa = model.getProp(state, 'derAssocFrac');
    
    eqs = cell(1, numAss);
    for i = 1 : numAss
        s1 = 0*dXa{i};
        s2 = 0*dXa{i};
        s3 = 0*dXa{i};
        for j = 1 : numAss
            xj = sites.components(j);
            s1 = s1 + dens.*x{xj}.*delta{i, j}.*dXa{j};
            s2 = s2 + x{xj}.*delta{i, j}.*Xa{j};
            s3 = s3 + dens.*x{xj}.*ddelta_rho{i, j}.*Xa{j};
        end
        eqs{i} = dXa{i} + (Xa{i}.^2).*(s1 + s2 + s3);
    end
    
end
