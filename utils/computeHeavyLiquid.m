function [s, sums] = computeHeavyLiquid(HL, V, Kl, Kv, z)

%
%
% SYNOPSIS:
%   function [s, sums] = computeHeavyLiquid(HL, V, Kl, Kv, z)
%
% DESCRIPTION:
%
% PARAMETERS:
%   HL - 
%   V  - 
%   Kl - 
%   Kv - 
%   z  - 
%
% RETURNS:
%   s    - 
%   sums - 
%
% EXAMPLE:
%
% SEE ALSO:
%

% return vapor (y) and salt (s) compositions. 
    if isstruct(HL)
        [s, sums] = model.computeHeavyLiquid(HL.HL, HL.V, HL.Kl, HL.Kv, HL.components); 
        return
    end
    
    if iscell(z)
        s = z; 
        sums = 0; 
        
        for i = 1:numel(z)
            bad =~isfinite(Kv{i}) & ~isfinite(Kl{i}); 
            s{i} = Kl{i}.*z{i}./(1 + V.*(Kv{i} - 1) + HL.*(Kl{i} - 1)); 
            s{i}(bad) = 0; 
            sums = sums + double(s{i}); 
        end
        s = cellfun(@(kv, kl, zi) kl.*zi./(1 + HL.*(kl - 1) + V.*(kv - 1)), Kv, Kl, z, 'UniformOutput', false); 
        s = cellfun(@(x) x./sums, s, 'UniformOutput', false); 
        assert(all(cellfun(@(x) all(isfinite(double(x))), y))); 
        
    else
        s = Kl.*z./(1 + (bsxfun(@times, V, (Kv - 1))) + (bsxfun(@times, (Kl - 1), HL))); 
        s(~isfinite(Kl)) = z(~isfinite(Kl)); 
        s = bsxfun(@rdivide, s, sum(s, 2)); 
        sums = sum(s, 2); 
        assert(all(isfinite(s(:)))); 
    end
end