function eqs = equationsDerAssocFracDrho(state, Xa, model, varargin)

%
%
% SYNOPSIS:
%   function eqs = equationsDerAssocFracDrho(state, Xa, model, varargin)
%
% DESCRIPTION:
%
% PARAMETERS:
%   state    - state from the derivative (with respect to density)
%              model of assocFracmodel  
%   Xa       - association variable Xa
%   model    - Derivative with respect to density of the association model
%   varargin - 
%
% RETURNS:
%   eqs - equations for the derivative of the association equations
%
% EXAMPLE:
%
% SEE ALSO: assocFracModel
%


    opt = struct('scalarinput', true);
    opt = merge_options(opt, varargin{:});
    
    dens   = model.dens;
    delta  = model.delta;
    ddelta_rho = model.ddelta_rho;
    x = model.x;
    
    if opt.scalarinput
        dens   = convertToDouble(dens);
        delta  = convertToDouble(delta);
        ddelta_rho = convertToDouble(ddelta_rho);
        x = convertToDouble(x);
    end
    
    pcmodel = model.pcsaftpropmodel;
    numAss = pcmodel.numAss;
    sites  = pcmodel.sites;
    
    dXa = model.getProp(state, 'derAssocFrac');
    if~iscell(dXa)
        dXa = num2cell(dXa);
    end
    eqs = cell(1, numAss);
    for i = 1 : numAss
        s1 = 0*dXa{i};
        s2 = 0*dXa{i};
        s3 = 0*dXa{i};
        for j = 1 : numAss
            xj = sites.components(j);
            s1 = s1 + dens.*x{xj}.*delta{i, j}.*dXa{j};
            s2 = s2 + x{xj}.*delta{i, j}.*Xa{j};
            s3 = s3 + dens.*x{xj}.*ddelta_rho{i, j}.*Xa{j};
        end
        eqs{i} = dXa{i} + (Xa{i}.^2).*(s1 + s2 + s3);
                                
                        
        if isa(   eqs{i},'ADI') && ~all(isfinite([   eqs{i}.val' sum(   eqs{i}.jac{:})]))                     
            warning('Linear system rhs must have finite entries.');
        elseif ~isa(   eqs{i},'ADI')&& ~all(isfinite(   eqs{i}))            
           warning('Linear system rhs must have finite entries.');            
        end
    end
    
end


%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}

