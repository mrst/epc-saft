function [state,report] = genericStandaloneFlash(state, eosmodel, varargin)

%
%
% SYNOPSIS:
%   function [state,report] = genericStandaloneFlash(state, eosmodel, varargin)
%
% DESCRIPTION:
%
% PARAMETERS:
%   state    - 
%   eosmodel - 
%   varargin - 
%
% RETURNS:
%   state  - 
%   report - 
%
% EXAMPLE:
%
% SEE ALSO:
%


    solver = getDefaultFlashNonLinearSolver(varargin{:});
    
    z = eosmodel.getProp(state, 'components');
    z = max(z, eosmodel.minimumComposition);
    z = bsxfun(@rdivide, z, sum(z, 2));
     
    state = eosmodel.setProp(state, 'components', z);
    
    state = eosmodel.validateState(state);
    [state, report] = solver.solveTimestep(state, 1, eosmodel);
    
end


