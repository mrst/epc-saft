function var  = convertToDouble(var)

%
%
% SYNOPSIS:
%   function var  = convertToDouble(var)
%
% DESCRIPTION:
%
% PARAMETERS:
%   var - 
%
% RETURNS:
%   var - 
%
% EXAMPLE:
%
% SEE ALSO:
%

    if iscell(var)
        var = cellfun(@(x) convertToDouble(x), var, 'uniformoutput', false);
        return
    elseif isa(var, 'ADI')
        var = var.val;
        return
    end
end
        
    