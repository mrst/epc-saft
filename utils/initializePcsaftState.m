classdef initializePcsaftState < StateFunction
%
%
% SYNOPSIS:
%   initializePcsaftState(model, varargin)
%
% DESCRIPTION:
%   Compute the reduced density for a given pressure, composition and temperature
% PARAMETERS:
%   model - density model  built with epc-saft EoS
%   varagin - state with a given pressure, temperature and composition and
%             reduced density (should be initialized)
% RETURNS:
%   state - a new  state with updated reduce  density using nonlinear solves 
%           of the density model 
%
% EXAMPLE:
%
% SEE ALSO: 
%   DensityModel
%


    
    properties
    
    end
    
    methods
        function gp = initializePcsaftState(model, varargin)
            gp@StateFunction(model, varargin{:});
            gp = gp.dependsOn({'T', 'components', 'pressure'}, 'state');
            
            gp.label = 'Initiate ReducedDensity';
        end

        function state = evaluateOnDomain(prop, model, state)
           
            comp = state.components;
            T = state.T;
            P = state.pressure;
            redrho = state.reducedDensity;

            % Initialize or re-use the previous density to re-start 
            % Reduced Density calculation   

            fluidtype    = state.FluidType;
            [~, redrho] = prepareReducedDensity(model, state, redrho,  fluidtype);
            
            pcsaftmodel  = model.pcsaftmodel;
            
            [~, state, report] = model.computeDensity(comp, T, P, redrho);
            state = pcsaftmodel.initStateFunctionContainers(state);
            state.itDens= report.Iterations;
        end
    end
end





function [state, redrho] = prepareReducedDensity(model, state, redrho,  fluidtype)
            % Prepare initial redduced density
            P = state.pressure; 
            
            switch fluidtype                                                  
                case 'Liquid'   
                    if any(isempty(redrho))||any(isnan(redrho))||(sum(redrho)==0)
                        Ind = (isnan(redrho))|(redrho==0);
                        redrho(Ind(:)) = 0.5;                   
                    end                        
                case 'Gas'                     
                    if any(isempty(redrho))||any(isnan(redrho))||(sum(redrho)==0)
                        Ind = (isnan(redrho))|(redrho==0);
                        redrho(Ind(:)) = 10^-10;                                                     
                        % correct initialization for critical pressure                          
                        redrho(P>7.5e6,:) = 0.5; % PS: in the litterature it's 0.4!  
                    end 
                case 'Solid'                                                             
                    if any(isempty(redrho))||any(isnan(redrho))||(sum(redrho)==0)
                        Ind = (isnan(redrho))|(redrho==0);
                        redrho(Ind(:)) = 0.5;
                    end    
            end
            
            
            %% Basic assertions          
            assert(all(isfinite( redrho)), 'Initial densitity must have finite values.');              
end
       

%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}

    

