function L1 = solveRachfordRice3phaseStep(L1, L2, K1, K2, z, varargin)

%
%
% SYNOPSIS:
%   function L1 = solveRachfordRice3phaseStep(L1, L2, K1, K2, z, varargin)
%
% DESCRIPTION:
%
% PARAMETERS:
%   L1       - 
%   L2       - 
%   K1       - 
%   K2       - 
%   z        - 
%   varargin - 
%
% RETURNS:
%   L1 - 
%
% EXAMPLE:
%
% SEE ALSO:
%


% Solve Rachford Rice equations to find liquid and vapor
% distribution.
    opt = struct('tolerance'    , 1e-12, ...
                 'min_z'        , 1e-10,...
                 'maxIterations', 500);
    opt = merge_options(opt, varargin{:});

    ncomp = size(z, 2);
    tmp1 = warning('query','MATLAB:nearlySingularMatrix');
    tmp2 = warning('query','MATLAB:singularMatrix');
    warning('off','MATLAB:nearlySingularMatrix')
    warning('off','MATLAB:singularMatrix')

    K1(~isfinite(K1)) = 1e6;

    singularities1 = 1./(1 - K1);
    singularities1 = min(max(singularities1, 0), 1);

    L_min1 = min(singularities1, [], 2);
    L_max1 = max(singularities1, [], 2);

    if isempty(L1)
        L1 = (L_min1 + L_max1)/2;
    end


    n_L = numel(L1);
    L_final = L1;
    active = true(n_L, 1);
    for it = 1:opt.maxIterations
        % fprintf('Iteration %d: %d active\n', it, nnz(active));
        L0 = L1;
        L1 = initVariablesADI (L1);
        L2_loc = L2(active);
        K_local1 = K1(active, :);
        K_local2 = K2(active, :);
        z_local = z(active, :);
        eq1 = 0;
        for i = 1:ncomp
            K1i = K1(active, i);
            K2i = K2(active, i);
            zi = z(active, i);
            v1 = ((K1i - 1).*zi)./(1 + abs(L1).*(K1i - 1)+ abs(L2_loc).*(K2i - 1));
            present = zi > opt.min_z;
            eq1 = eq1 + v1.*present;
        end
        
        r = value(eq1);
        if isa(eq1, 'ADI')
            J = -eq1.jac{1};
            dL = mldivide(J, r);
        else
            dL = 0*r;
        end
        
        vNorm = abs(r);
        % Converged values do not need to be updated
        convResidual = vNorm < opt.tolerance;
        dL = ~convResidual.*dL;
        %dL = sign(dL).*min(abs(dL), 0.2); %%%ASK???
        L1 = value(L1) + dL;
        %L = abs(value(L) + dL);
        dLNorm = abs(L1 - L0);
        
        atBoundary = (dL > 0 & L0 == L_max1) | (dL < 0 & L0 == L_min1);
        convResidual(atBoundary) = true;
        conv = dLNorm < opt.tolerance | convResidual;
        
        
        bad1 = (L1 < L_min1 | L1 > L_max1);
        if any(bad1)
            % Perform bisection
            L1(bad1) = bisection_3phase_L1(L_max1(bad1), L_min1(bad1), L2(bad1), K_local1(bad1, :), K_local2(bad1, :), z_local(bad1, :), opt);
            conv(bad1) = true;
        end
        L1 = max(L1, L_min1);
        L1 = min(L1, L_max1);
        
        if it == opt.maxIterations
            disp('reached max iterations in Racheford rice')
            conv = conv | true;
        end
        update = false(n_L, 1);
        update(active) = conv;
        
        L_final(update) = L1(conv);
        active(update) = false;
        
        L1 = L1(~conv);
        L_max1 = L_max1(~conv);
        L_min1 = L_min1(~conv);
        
        if all(conv)
            break
        end
        
    end

    L1 = L_final;


    warning(tmp1.state,'MATLAB:nearlySingularMatrix')
    warning(tmp2.state,'MATLAB:singularMatrix')
end

function L1 = bisection_3phase_L1(L_max10, L_min10, L2, K1, K2, z, opt)

    fn1 = @(L1) sum(((K1 - 1).*z)./(1 + abs(L1).*(K1 - 1)+ abs(L2).*(K2 - 1)).*(z>opt.min_z), 2);
    %fn2 = @(L2) sum(((K2 - 1).*z)./(1 + L1.*(K1 - 1)+ L2.*(K2 - 1)).*(z>opt.min_z), 2);

    left10 = fn1(L_min10);
    right10 = fn1(L_max10);
    [left1, right1, L_max1, L_min1] = deal(left10, right10, L_max10, L_min10);
    %left20 = fn2(L_min20);
    %right20 = fn2(L_max20);
    %[left2, right2, L_max2, L_min2] = deal(left20, right20, L_max20, L_min20);

    it = 1;
    tol = opt.tolerance;
    while it < 10000
        L1 = (L_max1 + L_min1)/2;
        %L2 = (L_max2 + L_min2)/2;
        mid1 = fn1(L1);
        
        toLeft1 = mid1.*left1 > 0;
        
        left1(toLeft1) = mid1(toLeft1);
        L_min1(toLeft1) = L1(toLeft1);
        right1(~toLeft1) = mid1(~toLeft1);
        L_max1(~toLeft1) = L1(~toLeft1);
        
        
        %mid2 = fn2(L2);
        
        %toLeft2 = mid2.*left2 > 0;
        
        %left2(toLeft2) = mid2(toLeft2);
        %L_min2(toLeft2) = L2(toLeft2);
        %right2(~toLeft2) = mid2(~toLeft2);
        %L_max2(~toLeft2) = L2(~toLeft2);
        
        if (norm(mid1, inf) < tol || norm(L_min1 - L_max1, inf) < tol)
            break
        end
        it = it + 1;
    end
    % Treat case where endpoints are outside of the domain
    bad1 = sign(left1) == sign(right1);
    lrg1 = abs(left10(bad1)) < abs(right10(bad1));
    L1(bad1) = lrg1.*L_min10(bad1) + ~lrg1.*L_max10(bad1);
    %bad2 = sign(left2) == sign(right2);
    %lrg2 = abs(left20(bad2)) < abs(right20(bad2));
    %L2(bad2) = lrg2.*L_min20(bad2) + ~lrg2.*L_max20(bad2);
    
end