function [eqs, names, types] = equationsAssocFrac(state, model, varargin)
% see. Walter G Chapman et al. “New reference equation of state for 
% associating liquids”. In: Industrial & engineering chemistry research 29.8 (1990),
% pages 1709–1721
%
% SYNOPSIS:
%   function [eqs, names, types] = equationsAssocFrac(state, model, varargin)
%
% DESCRIPTION:
%
% PARAMETERS:
%   state    - composition (x)
%   model    - pcsaft model (pcsaftpropmodel)
%   varargin - density (rho)
%
% RETURNS:
%   eqs   - get equations for the Association term for mixtures.
%   names - "HelmholtzEnergyAssociation"
%   types - "none"
%
% EXAMPLE:
%
% SEE ALSO:
% AssocFracModel
% AssociativeHelmoltzEnergy

    opt = struct('scalarinput', false);
    opt = merge_options(opt, varargin{:});
    
    if opt.scalarinput
        dens  = convertToDouble(model.dens);
        delta = convertToDouble(model.delta);
        x = convertToDouble(model.x);
    else
        dens  = model.dens;
        delta = model.delta;        
        x = model.x;
    end
    
    pcmodel = model.pcsaftpropmodel;
    numAss = pcmodel.numAss;
    sites  = pcmodel.sites;
    
    Xa = model.getProp(state, 'assocFrac');
    
    if ~iscell(Xa)
        Xa = num2cell(Xa,1);
    end
        
    
    eqs = cell(1, numAss);
    for i = 1 : numAss
        s = 0*Xa{i} + 1;
        for j = 1 : numAss
            xj = sites.components(j);
            s = s + dens.*x{xj}.*Xa{j}.*delta{i, j};
        end
        eqs{i} = Xa{i} - s.^-1;       
        % safe-guard  against infinite entries
        if isa(eqs{i},'ADI') && ~all(isfinite([eqs{i}.val' sum(eqs{i}.jac{:})]))
            warning('Linear system rhs must have finite entries.');    
        elseif ~isa(eqs{i},'ADI')&& ~all(isfinite(eqs{i}))            
           warning('Linear system rhs must have finite entries.');            
        end
    end
    
    names = "HelmholtzEnergyAssociation";
    types = "none";
end



%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
